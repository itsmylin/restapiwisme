<?php header("X-Frame-Options: DENY");?>
<!DOCTYPE html>
<html lang="en">

<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale = 1.0, user-scalable = 0" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="app-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="apple-touch-fullscreen" content="yes" />
        <link rel='shortcut icon' href='template/images/favicon.ico' type='x-icon'>
        <title>影片統計 | healing_fruits</title>
        <meta name="description" content="What you see what you get Enjoy to Interactive with living objects">

        <link class="icon" href="../icon/logo.png" rel="apple-touch-icon-precomposed" />
        <link class="icon" href="../icon/logo.png" rel="SHORTCUT ICON" />
        
        <link rel="stylesheet" href="css/all_css.css">
        <link rel="stylesheet" href="css/global.css">
        
        <?php include( "js/all_js.php"); ?>
        <script src="js/jquery.pagination.js"></script>
        <script src="js/arod/management_account.jquery.pagination.js"></script>
        <script>
                $.global_date_statistics_column = "vip"; //table:date_statistics > col:vip
        </script>
        <!--script src="js/search.js"></script-->
        <script src="js/batch.js"></script>

        <!-- include HIGHCHARTS-->
<!--        <script src="js/highstock.js"></script>
        <script src="js/mgm_highchart_jack.js"></script>
        <script src="js/mgm_hichart_click_jack.js"></script>-->
        <!-- for arod edit function (點擊折線圖的點會呼叫的fun)-->

        <!-- datePicker -->
        <script src="js/jquery-ui.js"></script>

        <!-- jquery.dataTables -->
        <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
        <script type="text/javascript" language="javascript" src="js/dataTablesPlugin.js"></script>
        
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>

        <script>
            
                $(document).ready(function() {
                });

                function init() {
                        
                        fn_read_video_statistics();
                        
                        var data = {
                                token: getCookie("scs_cookie")
                        };
                        var success_back = function(data) {
                                data = JSON.parse(data);
                                console.log(data);
                                var main_data, sub_data;
                                if (data.success) {
                                        //主分類-月點閱數統計
                        
                                        var tmp_data = data.data
                        
                                        $('#container_highcharts').highcharts({
                                            title: {
                                                text: '',
                                                x: -20 //center
                                            },
                                            subtitle: {
                                                text: '主分類',
                                                x: -20
                                            },
                                            xAxis: {
                                                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                                                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                                            },
                                            yAxis: {
                                                title: {
                                                    text: '月點閱數'
                                                },
                                                plotLines: [{
                                                    value: 0,
                                                    width: 1,
                                                    color: '#808080'
                                                }]
                                            },
                                            tooltip: {
                                                valueSuffix: '°C'
                                            },
                                            legend: {
                                                layout: 'vertical',
                                                align: 'right',
                                                verticalAlign: 'middle',
                                                borderWidth: 0
                                            },
                                            series: tmp_data
                                        });   
                                } else {
                                        show_remind( data.msg , "error");
                                }
                        };
                        var error_back = function(data) {
                                console.log(data);
                        };
                        $.Ajax("POST", "php/json_mgm_video_statistics.php?func=fn_read_video_highchart_line", data, "", success_back, error_back);
                }
                
                
                //會員居住城市統計、會員性別統計、會員年齡統計、會員職業統計
                function fn_read_video_statistics() {
                        var data = {
                                token: getCookie("scs_cookie")
                        };
                        var success_back = function(data) {
                                data = JSON.parse(data);
                                console.log(data);
                                var main_data, sub_data;
                                if (data.success) {
                                        //主分類點閱統計
                                        $.each( data.main_data , function( index , value ){
                                                main_data += '<tr class="child-middle">' +
                                                                    '<td>'+ value.cate_name +'</td>' +
                                                                    '<td>'+ value.m_click +'</td>' +
                                                                    '<td>'+ value.w_click +'</td>' +
                                                                    '<td>'+ value.d_click +'</td>' +
                                                            '</tr>';
                                        });
                                        $("#main_data").html(main_data);
                                        
                                        //子分類點閱統計 
                                        $.each( data.sub_data , function( index , value ){
                                                sub_data += '<tr class="child-middle">' +
                                                                    '<td>'+ value.cate_name +'</td>' +
                                                                    '<td>'+ value.m_click +'</td>' +
                                                                    '<td>'+ value.w_click +'</td>' +
                                                                    '<td>'+ value.d_click +'</td>' +
                                                            '</tr>';
                                        });
                                        $("#sub_data").html(sub_data);
                                                        
                                } else {
                                        show_remind( data.msg , "error");
                                }
                        };
                        var error_back = function(data) {
                                console.log(data);
                        };
                        $.Ajax("POST", "php/json_mgm_video_statistics.php?func=fn_read_video_statistics", data, "", success_back, error_back);
                }

        </script>
</head>

<body>
        <div id="all">

                <?php include( "html/loading.php"); ?>
                <?php include( "html/header.php"); ?>

                <div class="content">
                        <?php include( "html/sidebar_setting.php"); ?>

                        <div class="main-content container">                        
                                <div class="path">
                                        <a href="#">控制後台</a> > <a href="#">影片統計</a>
                                </div>

                                <div class="list">
                                        <div style="width: 49%; display: inline-block; margin-right: 1%; vertical-align: top;">
                                                <h2>影片統計</h2>
                                                <div id="container_highcharts" style="height: 300px; width: 100%; border: 1px solid;">圖表在這裡</div>
                                        </div>
                                        <div style="width:49%;display: inline-block;">
                                                <h2>主分類點閱統計</h2>
                                                <table class="display select dataTable" id="dynamic-table" style="width: 100%;">
                                                        <thead>
                                                                <tr>
                                                                        <th>類別/期間</th>
                                                                        <th>月點閱數</th>
                                                                        <th>週點閱數</th>
                                                                        <th>日點閱數</th>
                                                                </tr>
                                                        </thead>
                                                        <tbody id="main_data">
                                                                <tr class="odd child-middle">
                                                                        <td>樂活</td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                </tr>
                                                                <tr class="even child-middle">
                                                                        <td>工作</td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                </tr>
                                                                <tr class="odd child-middle">
                                                                        <td>健康</td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                </tr>
                                                                <tr class="even child-middle">
                                                                        <td>家庭</td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                </tr>
                                                        </tbody>
                                                </table>
                                        </div>
                                </div>

                                <div class="list">
                                                <h2>子分類點閱統計</h2>
                                                <table class="display select dataTable" id="dynamic-table" style="width: 100%;">
                                                        <thead>
                                                                <tr>
                                                                        <th>類別/期間</th>
                                                                        <th>月點閱數</th>
                                                                        <th>週點閱數</th>
                                                                        <th>日點閱數</th>
                                                                </tr>
                                                        </thead>
                                                        <tbody id="sub_data">
                                                                <tr class="odd child-middle">
                                                                        <td>信念</td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                </tr>
                                                                <tr class="even child-middle">
                                                                        <td>吉凶</td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                </tr>
                                                                <tr class="odd child-middle">
                                                                        <td>個性</td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                </tr>
                                                                <tr class="even child-middle">
                                                                        <td>人際</td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                </tr>
                                                                <tr class="odd child-middle">
                                                                        <td>溝通</td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                </tr>
                                                                <tr class="even  child-middle">
                                                                        <td>時間</td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                </tr>
                                                                <tr class="odd child-middle">
                                                                        <td>財富</td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                </tr>
                                                                <tr class="even child-middle">
                                                                        <td>職場</td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                </tr>
                                                                <tr class="odd child-middle">
                                                                        <td>紓壓</td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                </tr>
                                                                <tr class="even  child-middle">
                                                                        <td>生死</td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                </tr>
                                                                <tr class="odd child-middle">
                                                                        <td>綠活</td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                </tr>
                                                                <tr class="even  child-middle">
                                                                        <td>親子</td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                </tr>
                                                                <tr class="odd child-middle">
                                                                        <td>愛情</td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                </tr>
                                                                <tr class="even child-middle">
                                                                        <td>婆媳</td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                </tr>
                                                        </tbody>
                                                </table>
                                </div>
                        </div>

                </div>

                <?php include( "html/footer.php"); ?>

        </div>
    
        <script>
            
        function unconnected_callback() {
                loading_ajax_hide();
                show_remind( "請先登入" , "error"  );
        };
        function connected_callback( member ) {
                if( member.a_admin !== "true" ){
                        loading_ajax_hide();
                        show_remind( "不是管理者，三秒後轉跳到首頁。" , "error" );
                        setTimeout( function(){ location.href = "../index.php" }, 3000);
                }
                else{
                        init();
                }
        };
        
        </script>
</body>
</html>
