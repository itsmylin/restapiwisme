<?php header("X-Frame-Options: DENY");?>
<!DOCTYPE html>
<html lang="en">

<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale = 1.0, user-scalable = 0" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="app-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="apple-touch-fullscreen" content="yes" />
        <link rel='shortcut icon' href='template/images/favicon.ico' type='x-icon'>
        <title>分類設定 | healing_fruits</title>
        <meta name="description" content="What you see what you get Enjoy to Interactive with living objects">

        <link class="icon" href="../icon/logo.png" rel="apple-touch-icon-precomposed" />
        <link class="icon" href="../icon/logo.png" rel="SHORTCUT ICON" />
        
        <link rel="stylesheet" href="css/all_css.css">
        <link rel="stylesheet" href="css/global.css">
        
        <?php include( "js/all_js.php"); ?>
        <script src="js/jquery.pagination.js"></script>
        <script src="js/arod/management_account.jquery.pagination.js"></script>
        <script>
                $.global_date_statistics_column = "vip"; //table:date_statistics > col:vip
        </script>
        <!--script src="js/search.js"></script-->
        <script src="js/batch.js"></script>

        <!-- include HIGHCHARTS-->
        <script src="js/highstock.js"></script>
        <!--<script src="js/mgm_highchart_jack.js"></script>-->
        <!--<script src="js/mgm_hichart_click_jack.js"></script>-->
        <!-- for arod edit function (點擊折線圖的點會呼叫的fun)-->

        <!-- datePicker -->
        <script src="js/jquery-ui.js"></script>

        <!-- jquery.dataTables -->
        <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
        <script type="text/javascript" language="javascript" src="js/dataTablesPlugin.js"></script>
        
        <!--category-->                   
	<link rel="stylesheet" href="css/nestable.css">
    	<script src="js/jquery.nestable.js"></script>
        <style>
            .fa{
                cursor:pointer;
            }
        </style>

        <link rel="stylesheet" href="http://www.oort.com.tw/funbook19/template_tt/template/assets/css/font-awesome.css">
</head>

<body>
        <div id="all">
            
                <?php include( "html/loading.php"); ?>
                <?php include( "html/header.php"); ?>

                <div class="content">
                        <?php include( "html/sidebar_setting.php"); ?>

                        <div class="main-content container">
                                <div class="path">
                                        <a href="#">控制後台</a> > <a href="#">分類管理</a>
                                </div>

                                <!-- AL 20160703-->
                                <div id="container"> 
                                        <div class="filter">
                                                <form method="get" action="">
                                                        <input type="button" class="button" value="新增" id="category_add">
                                                </form>
                                        </div>

                                        <div class="dd" id="channel_list">
                                                <ol class="dd-list">

                                                </ol>
                                                <!--button class="btn btn-sm  btn-primary panel-float-right" type="button" style="border-radius: 3px; right: 15px;" id="category_save_order">
                                                            儲存排序 style="display:none;"
                                                </button-->
                                        </div>

                                        <div id="form_info" style="display: none;">
                                                <div class="list">
                                                        <nav class="set">
                                                                <h2 class="set-title">詳細設定</h2>
                                                                <form>
                                                                        <nav>
                                                                                <p>名稱<input type="text" placeholder="" id="form_name"></p>
                                                                                <p style="display: none;">顏色<input type="text" placeholder="" id="form_color"></p>
                                                                                <input type="checkbox" id="form_display" style="height: 15px">顯示
                                                                                <p>文章數 : <c id="page_num"></c></p>
                                                                                <p>總點擊數 : <c id="total_click"></c></p>

                                                                                <input type="button" id="category_del" class="button" value="刪除">
                                                                                <input type="button" id="category_save" class="button" value="儲存">
                                                                        </nav>
                                                                </form>

                                                                <div class="form-group" style="display:none;">
                                                                        <label style="text-align: left; font-size: 15px;" for="form-field-1-1" class="col-xs-12 control-label no-padding-right">分類首頁文章</label>
                                                                        <div class="col-xs-12">
                                                                            <div style="padding:0px;" class="col-xs-4 col-sm-3 col-md-2">
                                                                                    <input type="text" id="form_page" placeholder="" class="form-control">
                                                                            </div>
                                                                            <div style="padding:0px;" class="col-xs-4 col-sm-3 col-md-2">
                                                                                    <input type="text" id="form_page" placeholder="" class="form-control">
                                                                            </div>
                                                                            <div style="padding:0px;" class="col-xs-4 col-sm-3 col-md-2">
                                                                                    <input type="text" id="form_page" placeholder="" class="form-control">
                                                                            </div>
                                                                            <div style="padding:0px;" class="col-xs-4 col-sm-3 col-md-2">
                                                                                    <input type="text" id="form_page" placeholder="" class="form-control">
                                                                            </div>
                                                                            <div style="padding:0px;" class="col-xs-4 col-sm-3 col-md-2">
                                                                                    <input type="text" id="form_page" placeholder="" class="form-control">
                                                                            </div>
                                                                            <div style="padding:0px;" class="col-xs-4 col-sm-3 col-md-2">
                                                                                    <input type="text" id="form_page" placeholder="" class="form-control">
                                                                            </div>
                                                                            <div style="padding:0px;" class="col-xs-4 col-sm-3 col-md-2">
                                                                                    <input type="text" id="form_page" placeholder="" class="form-control">
                                                                            </div>
                                                                            <div style="padding:0px;" class="col-xs-4 col-sm-3 col-md-2">
                                                                                    <input type="text" id="form_page" placeholder="" class="form-control">
                                                                            </div>
                                                                            <div style="padding:0px;" class="col-xs-4 col-sm-3 col-md-2">
                                                                                    <input type="text" id="form_page" placeholder="" class="form-control">
                                                                            </div>
                                                                            <div style="padding:0px;" class="col-xs-4 col-sm-3 col-md-2">
                                                                                    <input type="text" id="form_page" placeholder="" class="form-control">
                                                                            </div>
                                                                            <div style="padding:0px;" class="col-xs-4 col-sm-3 col-md-2">
                                                                                    <input type="text" id="form_page" placeholder="" class="form-control">
                                                                            </div>
                                                                            <div style="padding:0px;" class="col-xs-4 col-sm-3 col-md-2">
                                                                                    <input type="text" id="form_page" placeholder="" class="form-control">
                                                                            </div>
                                                                        </div>
                                                                </div>
                                                        </nav>
                                                </div>
                                        </div>
                                </div>
                        </div>

                </div>

                <?php include( "html/footer.php"); ?>

        </div>
    
        <!--刪除分類-->
        <div class="modal fade" id="myModalDeleteCategory" aria-hidden="false">
                <div class="modal-dialog">
                        <div id="dialog-msg">
                                <dl>
                                        <div class="list">
                                                <h2>刪除分類</h2>
                                                <p class="words">是否刪除分類：<strong id="myModalDeleteCategory_Name"></strong></p>
                                                <p>注意：刪除就無法還原</p>
                                                <form method="get">
                                                        <nav>
                                                                <p>所屬文章轉移至主分類：<select style="margin: 0 10px 0 0;" type="year" id="transfer_category"></select></p>
                                                                <p>所屬文章轉移至次分類：<select style="margin: 0 10px 0 0;" type="year" id="transfer_sub_category"></select></p>
                                                                <a type="button" class="button" id="myModalDeleteCategory_Yes" data-dismiss="modal">確定</a>
                                                                <a type="button" class="button delet" data-dismiss="modal">取消</a>
                                                        </nav>
                                                </form>
                                        </div>
                                </dl>
                        </div>
                </div>
        </div>

        <!-- Edit Channel-->
        <div style="display: none;" class="modal fade" id="myModalEditChannel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                        <div class="modal-content">
                                <div class="modal-dialog" style="width: 350px;">
                                        <div class="modal-content">

                                                <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">
                                                                <span aria-hidden="true">
                                                                        <span class="glyphicon glyphicon-remove"></span>
                                                                </span>
                                                                <span class="sr-only">Close</span>
                                                        </button>
                                                        <div id="myModalEditChannel_title">
                                                            <h3 state="add" class="modal-title">新增分類</h3>
                                                            <h3 state="modify" class="modal-title">修改分類</h3>
                                                        </div>
                                                </div>

                                                <div class="modal-body">

                                                        <!--div class="widget-main">
                                                            <div class="form-group">
                                                                    <div class="col-xs-12">
                                                                            <label class="ace-file-input ace-file-multiple">
                                                                                <input id="myModalEditChannel_uploadIcon" type="file">
                                                                                <span class="ace-file-container" data-title="Drop files here or click to choose">
                                                                                    <span id="CheckChannel_drop_img_o" data-title="No File ..." class="ace-file-name">
                                                                                        <i class=" ace-icon ace-icon fa fa-cloud-upload"></i>
                                                                                    </span>
                                                                                    <div style="height: 150px; display: none;">
                                                                                        <img id="CheckChannel_drop_img_c" style="display: block; text-align: center; position: absolute; margin: auto; left: 0px; right: 0px; width: 150px;" src="">
                                                                                    </div>
                                                                                </span>
                                                                                <a href="#" class="remove">
                                                                                    <i class=" ace-icon fa fa-times"></i>
                                                                                </a>
                                                                            </label>
                                                                    </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div-->

                                                        <form role="form" class="form-horizontal">
                                                                    <div style="margin: 0" class="form-group">
                                                                                <label style="font-size: 12pt; font-weight: normal; float: left; text-align: left;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 control-label">
                                                                                    <span style="float: left; margin-top: 3px;">分類名稱 ： </span>
                                                                                    <input data-input="title" style="width: auto; float: left;" class="form-control" type="text">
                                                                                </label>

                                                                                <!--label style="font-size: 12pt; font-weight: normal; float: left; text-align: left;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 control-label">
                                                                                    <span style="float: left; margin-top: 3px;">頻道主編 ： </span>
                                                                                        <select data-select="user">
                                                                                            <optgroup label="主編">
                                                                                            </optgroup>
                                                                                        </select>
                                                                                </label-->

                                                                                <!--label style="font-size: 12pt; font-weight: normal; float: left; text-align: left;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 control-label">
                                                                                    <input type="checkbox" name="display" style="float: left; margin-right: 5px; margin-top: 2px;">
                                                                                    <div>是否顯示</div>
                                                                                </label-->
                                                                    </div>
                                                        </form>
                                                </div>

                                                <div class="modal-footer">
                                                    <button id="myModalEditChannel_Yes" style="border-radius: 5px" class="btn btn-primary" data-dismiss="modal">確 認</button>
                                                    <button style="border-radius: 5px" class="btn btn-primary" data-dismiss="modal">取 消</button>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
        
        <style>
                    
            /* <div style="background-image:url('img/global/folder.png');"></div> */
            .pagebg {
                background-position: 50% 0%;
                background-size: cover;
                height: 94%;
                margin-left: 0%;
                margin-top: 3%;
                /*position: absolute;*/
                width: 94%;
            }
            /*AL 20160703*/
            .set nav {
                width: 100%;
            }
            #form_info .list {
                display: inline-block; 
                margin-left: 10px; 
                width: 90%;
            }
            #form_info {
                float: left;
                width: 50%;
            }
            @media(max-width:800px){
                #channel_list .dd {
                    width: 40%;
                }
                #form_info {
                    width: 35%;
                }
            }
            @media(max-width:320px){
                #channel_list .dd {
                    width: 100%;
                }
                #form_info {
                    width: 100%;
                }
            }
        </style>
        
        <script type="text/javascript">
        
        loading_ajax_show();
        $("document").ready(function() {
                
        });
        
        function init(){
                
                show_list();
                $( "#channel_list .dd-list" ).delegate( ".dd3-content", "click", function() {
                        
                        var li = $( this ).parent();
                        var data = $( this ).data("data");
                        $( "#form_info" ).attr( "parent" , li.attr( "parent" ) ).attr( "info_id" , data.id ).show();
                        $( "#form_name" ).val( data.cate_name );
                        $( "#form_color" ).val( data.cate_color );
                        //vip專用板塊
                        if( data.cate_display === "vip" ){
                            $( "#form_display" ).parent().hide();
                        }
                        else{
                            $( "#form_display" ).parent().show();
                            if( $( "#form_display" ).is( ":checked" ) !== ( data.cate_display === "block" ) )
                                $( "#form_display" ).click();
                        }
                        
                        $( "[id=form_page]" ).val( "" );
                        $.each( data.cate_page , function( index , value ){
                                $( "[id=form_page]" ).eq( index ).val( value );
                        });
                        
                        $( "#page_num" ).html( data.page_num );
                        $( "#total_click" ).html( data.total_click );
                        
                });
                $( "#channel_list .dd-list" ).delegate( ".fa-plus-square", "click", function() {
                        
                        var cate_id = $( this ).parent().attr( "data-id" );
                        var data = {
                                token: getCookie("scs_cookie") ,
                                cate_id: cate_id
                        };
                        var success_back = function( data ) {

                                data = JSON.parse( data );
                                console.log(data);
                                loading_ajax_hide();
                                if( data.success ) {
                                        show_list();
                                }
                                else {
                                        show_remind( data.msg , "error" );
                                }

                        }
                        var error_back = function( data ) {
                                console.log(data);
                        }
                        $.Ajax( "POST" , "../php/category.php?func=add_sub" , data , "" , success_back , error_back);  
                        
                });
                $( "#channel_list .dd-list" ).delegate( ".fa-arrow-down", "click", function() {
                        
                        var pos = $( this ).parent( "li" );
                        var data = pos.children('.dd3-content').data("data");
                        if( pos.next()[0] ) {

                                var next_pos = pos.next();
                                var html = pos.html();
                                var attr = pos[0].attributes;
                                var attr_html = "";
                                $.each( attr , function( index , value ){
                                        attr_html += " " + value.name + "=" + "'" + value.value + "'";
                                });
                                pos.remove();
                                next_pos.after( "<li" + attr_html + ">" + html + "</li>" );
                                console.log( next_pos );
                                console.log( next_pos.next() );
                                console.log( data );
                                next_pos.next().children('.dd3-content').data("data",data);
                                $( "#channel_list" ).trigger( "change" );
                        }
                        
                });
                $( "#channel_list .dd-list" ).delegate( ".fa-arrow-up", "click", function() {
                        
                        var pos = $( this ).parent( "li" );
                        var data = pos.children('.dd3-content').data("data");
                        if( pos.prev()[0] ) {

                                var prev_pos = pos.prev();
                                var html = pos.html();
                                var attr = pos[0].attributes;
                                var attr_html = "";
                                $.each( attr , function( index , value ){
                                        attr_html += " " + value.name + "=" + "'" + value.value + "'";
                                });
                                pos.remove();
                                prev_pos.before( "<li" + attr_html + ">" + html + "</li>" );
                                console.log( prev_pos );
                                console.log( prev_pos.prev() );
                                console.log( data );
                                prev_pos.prev().children('.dd3-content').data("data",data);
                                $( "#channel_list" ).trigger( "change" );

                        }
                        
                });

        }
        
        function show_list() {
            
                $( "#channel_list ol" ).html("");
                var data = {
                            token:      getCookie("scs_cookie")
                };
                var success_back = function( data ) {

                        data = JSON.parse( data );
                        console.log(data);
                        loading_ajax_hide();
                        if( data.success ) {
                            
                            $.hide_sub_category = "";
                            $.sel_html = "";
                            $.sel_sub_json = {};
                            
                            $.each( data.data , function( index , value ){
                                    buildItem( $('#channel_list > ol') , value , true , false );
                            });
                            
                            $( "#transfer_category" ).html( $.sel_html );
                            
                            $( "#transfer_category" ).unbind('change').bind('change', function(){
                                    console.log( $( this ).val() );
                                    $( "#transfer_sub_category" ).html( $.sel_sub_json[ $( this ).val() ] );
                                    if( $.hide_sub_category ){
                                        $( "#transfer_sub_category" ).children( "[value='"+$.hide_sub_category+"']" ).addClass("hide");
                                        $( "#transfer_sub_category" ).val( $( "#transfer_sub_category" ).children(":not('.hide'):eq(0)").val() );
                                    }
                            }).trigger('change');
                            
                            $( "#channel_list" ).nestable();
                            $( "#channel_list" ).unbind('change').bind('change', function(){
                                    
                                    loading_ajax_show();
                                    var data = {
                                                token:      getCookie("scs_cookie") ,
                                                tree : JSON.stringify( $( "#channel_list" ).nestable('serialize') ) ,
                                                json_tree : $( "#channel_list" ).nestable('serialize')
                                    };
                                    var success_back = function( data ) {

                                            data = JSON.parse( data );
                                            console.log(data);
                                            loading_ajax_hide();
                                            if( data.success ) {
                                                    //show_remind( "儲存成功" );
                                            }
                                            else {
                                                    show_remind( data.msg , "error" );
                                            }

                                    }
                                    var error_back = function( data ) {
                                            console.log(data);
                                    }
                                    $.Ajax( "POST" , "../php/category.php?func=save_order" , data , "" , success_back , error_back);
                                    
                            }).trigger('change');

                            
                        }
                        else {
                            show_remind( data.msg , "error" );
                        }

                }
                var error_back = function( data ) {
                        console.log(data);
                }
                $.Ajax( "POST" , "../php/category.php?func=detail_list" , data , "" , success_back , error_back);
        }
        
        function buildItem( pos , item , parent , my_parent ) {
                
                var bk_color,bk_image;
                if( item.cate_display === "block" ){
                        bk_color = "#428bca";
                        bk_image = "none";
                }
                else if( item.cate_display === "none" ){
                        bk_color = "#ddd";
                        bk_image = "none";
                }
                else if( item.cate_display === "vip" ){
                        bk_color = "#b43800";
                        bk_image = "none";
                }
                var tmp = '<li class="dd-item dd3-item" data-id="' + item.id + '" parent="'+my_parent+'">' +
                        '<i style="float: right; padding:8px 5px;color:gray;" class="fa fa-arrow-down"></i>' +
                        '<i style="float: right;padding:8px 5px;color:gray;" class="fa fa-arrow-up"></i>';
                if( parent ){
                        tmp += '<i style="float: right; padding:8px 5px;color:gray;" class="fa fa-plus-square"></i>';
                        $.sel_html += '<option value="' + item.id + '">' + item.cate_name + '</option>';
                        $.sel_sub_json[item.cate_id] = '';
                }
                else{
                        $.sel_sub_json[my_parent] += '<option value="' + item.id + '">' + item.cate_name + '</option>';
                }
                tmp += '<div class="dd-handle dd3-handle" style="background-color:' + bk_color + '; background-image:' + bk_image + ';"></div><div class="dd3-content">' + item.cate_name + ': ' + item.id + '</div>' +
                      '</li>';
                pos.append( tmp );
                $( "#channel_list .dd-list [data-id=" + item.id + "] .dd3-content" ).data( "data" , item );
                
                //$.sel_html += '<option value="' + item.id + '">' + item.cate_name + '</option>';

                if (item.children) {
                    
                    pos.find( "li:last" ).append( "<ol class='dd-list'></ol>" );
                    pos = pos.find( "ol:last" );
                    $.each(item.children, function (index, sub) {
                        buildItem( pos , sub , false , item.cate_id );
                    });
                    
                }
                else{
                    $.sel_sub_json[item.cate_id] = '<option value="0">無次分類</option>';
                }
                
        }
        
        $( "#form_display" ).unbind( "change" ).bind( "change" , function(){
                
                var info_id = $( "#form_info" ).attr( "info_id" );
                var bk_color = $( this ).is( ":checked" ) ? "#428bca" : "#ddd";
                var bk_image = $( this ).is( ":checked" ) ? "none" : "none";
                $( "#channel_list [data-id=" + info_id + "] > .dd3-handle" ).css( "background-color" , bk_color ).css( "background-image" , bk_image );

        });
        $( "#category_add" ).unbind( "click" ).bind( "click" , function(){
                
                var data = {
                            token:      getCookie("scs_cookie")
                };
                var success_back = function( data ) {

                        data = JSON.parse( data );
                        console.log(data);
                        loading_ajax_hide();
                        if( data.success ) {
                                show_list();
                        }
                        else {
                                show_remind( data.msg , "error" );
                        }

                }
                var error_back = function( data ) {
                        console.log(data);
                }
                $.Ajax( "POST" , "../php/category.php?func=add" , data , "" , success_back , error_back);
                
        });
        
        $( "#category_save" ).unbind( "click" ).bind( "click" , function(){
                
                var bool = true;
                var msg = "";
                var info_id = $( "#form_info" ).attr( "info_id" );
                var display = $( "#form_display" ).is( ":checked" ) ? "block" : "none";
                var page = [];
                $.each( $( "[id=form_page]" ) , function( index , value ){
                    
                    if( $( value ).val() !== "" )
                        page[page.length] = $( value ).val();
                    
                });
                page = JSON.stringify( page );
                
                if( $( "#form_name" ).val() ){
                    $( "#form_name" ).parent().removeClass( "has-error" );
                }
                else{
                    bool = false;
                    $( "#form_name" ).parent().addClass( "has-error" );
                    msg += msg === "" ? "請" : "、";
                    msg += "請輸入名稱";
                }
                
//                if( $( "#form_color" ).val() ){
//                    $( "#form_color" ).parent().removeClass( "has-error" );
//                }
//                else{
//                    bool = false;
//                    $( "#form_color" ).parent().addClass( "has-error" );
//                    msg += msg === "" ? "請" : "、";
//                    msg += "請輸入顏色";
//                }
                
                if( bool ) {
                    
                    var data = {
                                token:      getCookie("scs_cookie") ,
                                cate_id : info_id ,
                                cate_name : $( "#form_name" ).val() , 
                                cate_color : $( "#form_color" ).val() , 
                                cate_display : display , 
                                cate_page : page
                    };
                    var success_back = function( data ) {

                            data = JSON.parse( data );
                            console.log(data);
                            loading_ajax_hide();
                            if( data.success ) {
                                    show_remind( "儲存成功" );
                                    show_list();
                            }
                            else {
                                    show_remind( data.msg , "error" );
                            }

                    }
                    var error_back = function( data ) {
                            console.log(data);
                    }

                    $.Ajax( "POST" , "../php/category.php?func=save_single_info" , data , "" , success_back , error_back);
                    
                }
                else {
                    show_remind( msg , "error" );
                }
                
        });
        $( "#category_del" ).unbind( "click" ).bind( "click" , function(){
                
                var tmp = "";
                var info_id = $( "#form_info" ).attr( "info_id" );
                var parent = $( "#form_info" ).attr( "parent" );
                
                $( "#transfer_category" ).children().removeClass("hide");
                $( "#transfer_sub_category" ).children().removeClass("hide");
                
                if( parent === "false" ){
                    //主類別刪除
                    tmp += "主類別-" + $( "#form_name" ).val();
                    $.each( $( "#channel_list [data-id=" + info_id + "] > ol .dd3-content" ).get() , function( k , v ){
                            tmp += "、次類別-" + $( v ).html();
                    });
                    $( "#myModalDeleteCategory_Name" ).html( tmp );
                    //準備刪除的隱藏
                    $( "#transfer_category" ).children( "[value=" + info_id + "]" ).addClass("hide");
                    $( "#transfer_category" ).val( $( "#transfer_category" ).children(":not('.hide'):eq(0)").val() ).trigger("change");
                    
                    $.hide_sub_category = "";
                }
                else{
                    //次類別刪除
                    $( "#myModalDeleteCategory_Name" ).html( "次類別-" + $( "#form_name" ).val() );
                    //準備刪除的隱藏
                    $.hide_sub_category = info_id;
                    $( "#transfer_sub_category" ).children( "[value="+info_id+"]" ).addClass("hide");
                }
                
                
//                if( $( "[data-id='" + info_id + "']" ).children("ol")[0] ){
//                    $.each( $( "[data-id='" + info_id + "']" ).children("ol").find( "[data-id]" ) , function( index , value ){
//                        $( "#transfer_category" ).children( "[value=" + $( value ).attr( "data-id" ) + "]" ).hide();
//                    });
//                }
                
                $( "#myModalDeleteCategory" ).modal( "show" );

        });

        $( "#myModalDeleteCategory_Yes" ).unbind( "click" ).bind( "click" , function(){
                
                if( !$( "#transfer_sub_category" ).val() ){
                        show_remind( "填入正確的次分類" , "error" );
                        return false;
                }
                
                var info_id = $( "#form_info" ).attr( "info_id" );
                var parent = $( "#form_info" ).attr( "parent" );
                var data = {
                            token:      getCookie("scs_cookie") ,
                            cate_id : info_id ,
                            transfer : $( "#transfer_category" ).val() ,
                            transfer_sub : $( "#transfer_sub_category" ).val() ,
                            parent : parent
                };
                var success_back = function( data ) {

                        data = JSON.parse( data );
                        console.log(data);
                        loading_ajax_hide();
                        if( data.success ) {
                                show_list();
                                $( "#form_info" ).removeAttr( "info_id" ).hide();
                                //$( "#channel_list [data-id=" + info_id + "]" ).remove();
                                //$( "#transfer_category [value=" + info_id + "]" ).remove();
                        }
                        else {
                                show_remind( data.msg , "error" );
                        }

                }
                var error_back = function( data ) {
                        console.log(data);
                }

                $.Ajax( "POST" , "../php/category.php?func=delete" , data , "" , success_back , error_back);
                
        });
        
        /*$('#myModalDeleteCategory')
        .on('show.bs.modal', function (e) {
        })
        .on('hidden.bs.modal', function (e) {
                $("#myModalDeleteCategory").attr("cate_id" , "");
        });*/
        
            
        function unconnected_callback() {
                loading_ajax_hide();
                show_remind( "請先登入" , "error"  );
        };
        function connected_callback( member ) {
                if( member.a_admin !== "true" ){
                        loading_ajax_hide();
                        show_remind( "不是管理者，三秒後轉跳到首頁。" , "error" );
                        setTimeout( function(){ location.href = "../index.php" }, 3000);
                }
                else{
                        init();
                }
        };
        
        </script>
</body>
</html>
