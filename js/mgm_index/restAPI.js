/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function($) {
    console.log("test");
    var createRandom = String(parseInt(Math.random()*100000000000000));
    var createRandomAdmin = String(parseInt(Math.random()*100000000000000));
    var editRandomName = String(parseInt(Math.random()*100000000000000));
    var editRandomEmail = String(parseInt(Math.random()*100000000000000));
    
$.RESTfulAPI = {
    conf: {
        users : [{
            type: "POST",
            url: "/users/login",
            test:[{
                buttonTitle: "使用帳號密碼測試登入",
                desc: "使用帳號密碼測試登入",
                data: {
                    "userinfo_Email": "53639359617359@gmail.com"
                }
            }]
        },{
            type: "GET",
            url: "/users",
            test:[{
                buttonTitle: "使用TOKEN取得資料",
                desc: "使用TOKEN測試登入",
                data: {  }
            }]
        },{
            type: "POST",
            url: "/users",
            test:[{
                buttonTitle: "使用者註冊會員",
                desc: "使用者註冊會員",
                data: { "userinfo_UserName": createRandom,
                        "userinfo_Password": btoa(md5(createRandom)),
                        "userinfo_Email": createRandom+"@gmail.com"
                }
            }]
        },{
            type: "POST",
            url: "/users/createUserByAdmin",
            test:[{
                buttonTitle: "管理者新增會員",
                desc: "管理者新增會員",
                data: { "userinfo_UserName": createRandomAdmin,
                        "userinfo_Password": createRandomAdmin,
                        "userinfo_Email": createRandomAdmin+"@gmail.com",
                        "employee_ID": "1",
                        "userinfo_InExUser": ["Operator","Accounting"],
                        "userinfo_Remarks": 'Remarks'
                }
            }]
        },{
            type: "GET",
            url: "/users/getPermissions",
            test:[{
                buttonTitle: "取得使用者權限",
                desc: "取得使用者權限",
                data: {  }
                /*
//                    'Authorization': "Token 34e8bd4b51f6cf0b5068c0e4c24ac637"//Administrator
//                    'Authorization': "Token c7b6db9df4c16480a11ab9447e7a31d8"//Dispatcher,Sales
//                    'Authorization': "Token c85df655cc8ea4ad8756c944bac571f8"//Operator
                    'Authorization': "Token e3c9a1cddef7a723730e33d2d419af7e"//Accounting
//                    'Authorization': "Token ec38dd04eba4349acdb5ae6fe649c962"//Sales
//                    'Authorization': "Token be7e5a771b8282aad39d1ac184072315"//None*/
            }]
        },{
            type: "GET",
            url: "/users/checkUserState",
            test:[{
                buttonTitle: "管理者取得某一使用者狀態",
                desc: "管理者取得某一使用者狀態",
                data: { "userinfo_ID": "3"
                }
            }]
        },{
            type: "POST",
            url: "/users/editMyName",
            test:[{
                buttonTitle: "使用者修改名字",
                desc: "使用者修改名字",
                data: { "userinfo_UserName": "AdministratorUserName"//editRandomName
                }
            }]
        },{
            type: "POST",
            url: "/users/editMyEmail",
            test:[{
                buttonTitle: "使用者修改Email",
                desc: "使用者修改Email",
                data: { "userinfo_Email": editRandomEmail+"@gmail.com" ,
                    "userinfo_Password": btoa(md5("520520op"))
                }
            }]
        },{
            type: "POST",
            url: "/users/editMyPassword",
            test:[{
                buttonTitle: "使用者修改密碼",
                desc: "使用者修改密碼",
                data: { "userinfo_old_Password": btoa(md5("520520op")) ,
                    "userinfo_new_Password": btoa(md5("520520op"))
                }
            }]
        },{
            type: "POST",
            url: "/users/editByUserID",
            test:[{
                buttonTitle: "管理者修改使用者資料",
                desc: "管理者修改使用者資料",
                data: { "userinfo_ID": "18" ,
                    "userinfo_UserName": "ChenBoHan" ,
                    "userinfo_Password": btoa(md5("123321123")) ,
                    "userinfo_Email": "bightp85065@gmail.com" ,
                    "employee_ID": "1" ,
                    "userinfo_InExUser": ["Sales"] ,
                    "userinfo_Remarks": "這是備註"
                }
            }]
        },{
            type: "POST",
            url: "/users/forgotPassword",
            test:[{
                buttonTitle: "使用者忘記密碼",
                desc: "使用者忘記密碼",
                data: { "userinfo_Email": "bightp85065@gmail.com"
                }
            }]
        },{
            type: "POST",
            url: "/users/emailAuthentication",
            test:[{
                buttonTitle: "使用者Email驗證的TOKEN驗證",
                desc: "使用者Email驗證的TOKEN驗證(注意：此測試僅能測試一次，送出後Token失效)",
                data: { "token": "RJOPHQV3GTXUVXN9F53V"
                }
            }]
        },{
            type: "POST",
            url: "/users/checkForgotToken",
            test:[{
                buttonTitle: "使用者驗證忘記密碼的TOKEN",
                desc: "使用者驗證忘記密碼的TOKEN(注意：此測試當送出'使用者驗證忘記密碼的TOKEN'功能，送出後Token失效)",
                data: { "token": "PHHIG4HXSHRHHV5BF0AI"
                }
            }]
        },{
            type: "POST",
            url: "/users/editPasswordByForgotToken",
            test:[{
                buttonTitle: "使用者驗證忘記密碼的TOKEN",
                desc: "使用者驗證忘記密碼的TOKEN(注意：此測試送出後Token失效)",
                data: { "token": "PHHIG4HXSHRHHV5BF0AI" ,
                    "userinfo_Password": btoa(md5("520520op"))
                }
            }]
        },{
            type: "POST",
            url: "/users/switchStatu",
            test:[{
                buttonTitle: "管理者修改使用者狀態",
                desc: "管理者修改使用者狀態",
                data: { "token": "0B7OFVN296MJEHJ8UX7S" ,
                    "userinfo_ID": "19"
                }
            }]
        },{
            type: "POST",
            url: "/users/userLogout",
            test:[{
                buttonTitle: "使用者登出",
                desc: "使用者登出",
                data: {  }
            }]
        },{
            type: "POST",
            url: "/users/reSendEmailForAuth",
            test:[{
                buttonTitle: "使用者要求重新寄送認證信",
                desc: "使用者要求重新寄送認證信",
                data: { }
            }]
        }],
        userProfile: [{
            type: "GET",
            url: "/userProfile/get",
            test:[{
                buttonTitle: "GET",
                desc: "依照userinfo_oTableColumn值取得相對應userinfo_id的資料<br><br>userinfo_oTableColumn sample:<div style='padding-left: 15px;'><li>userinfo_appv.userinfo_appv</li><li>userinfo_bike_mode.userinfo_bike_mode</li><li>userinfo_birth.userinfo_birth</li><li>userinfo_cfv.userinfo_cfv</li><li>userinfo_console_id.userinfo_console_id</li><li>userinfo_date_time.userinfo_date_time</li><li>userinfo_device_id.userinfo_device_id</li><li>userinfo_email.userinfo_email</li><li>userinfo_format.userinfo_format</li><li>userinfo_indoor.userinfo_indoor</li><li>userinfo_tire_size.userinfo_tire_size</li><li>userinfo_training_mode.userinfo_training_mode</li><li>userinfo_unit.userinfo_unit</li><li>userinfo_weight.userinfo_weight</li></div>",
                data: {
                    userinfo_id: '145'
                    , userinfo_oTableColumn: 'userinfo_device_id.userinfo_device_id'
                }
            }]
        },{
            type: "POST",
            url: "/userProfile/set",
            test:[{
                buttonTitle: "SET",
                desc: "依照userinfo_oTableColumn值新增/修改相對應userinfo_id的資料<br><br>userinfo_oTableColumn sample:<div style='padding-left: 15px;'><li>userinfo_appv.userinfo_appv</li><li>userinfo_bike_mode.userinfo_bike_mode</li><li>userinfo_birth.userinfo_birth</li><li>userinfo_cfv.userinfo_cfv</li><li>userinfo_console_id.userinfo_console_id</li><li>userinfo_date_time.userinfo_date_time</li><li>userinfo_device_id.userinfo_device_id</li><li>userinfo_email.userinfo_email</li><li>userinfo_format.userinfo_format</li><li>userinfo_indoor.userinfo_indoor</li><li>userinfo_tire_size.userinfo_tire_size</li><li>userinfo_training_mode.userinfo_training_mode</li><li>userinfo_unit.userinfo_unit</li><li>userinfo_weight.userinfo_weight</li></div>",
                data: {
                    userinfo_id: '145'
                    , userinfo_oTableColumn: 'userinfo_device_id.userinfo_device_id'
                    , userinfo_value: '3'
                }
            }]
        }],
        training: [{
            type: "POST",
            url: "/training/upload",
            uploadFile: true,
            test:[{
                buttonTitle: "UPLOAD",
                desc: "上傳.csv or .afr檔，並將此檔案資料新增至wisdat.console",
                data: {
                    trainingCloudId: 'auto'
                }
            }]
        },{
            type: "GET",
            url: "/training/7",
            test:[{
                buttonTitle: "GET",
                desc: "根據url最後一個值抓取此key的training資料",
                data: {}
            }]
        },{
            type: "GET",
            url: "/training/getList",
            test:[{
                buttonTitle: "GET List",
                desc: "回傳paging範圍內的training資料('1,2':每頁兩筆資料顯示第一頁的資料, 'all':顯示全部)",
                data: {
                    pagingInfo: "1,2"
                },
                data2: {
                    pagingInfo: "all"  
                }
            }]
        }],
        trainingTest:[{
            type: "GET",
            url: "/trainingTest",
            test:[{
                buttonTitle: "trainingTest",
                desc: "",
                data: {}
            }]
        }],
        wismelinode2Frontend:[{
            type: "POST",
            url: "http://139.162.112.225/WIS_SPORTINFO",
            test:[{
                buttonTitle: "取得使用者運動資訊(年)",
                desc: "key:固定服務驗證碼c3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為year<br>class:型態(cal、step、distance)分別卡路里;步數;距離<br>mac:手環MAC<br>year:指定年份",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"year",
                    "class":"cal",
                    "mac":"22:22:22:22:22",
                    "year":2016
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_SPORTINFO",
            test:[{
                buttonTitle: "取得使用者運動資訊(月)",
                desc: "key:固定服務驗證碼c3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為month<br>class:型態(cal、step、distance)分別卡路里;步數;距離<br>mac:手環MAC<br>year:指定年份<br>month:指定月份",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"month",
                    "class":"cal",
                    "mac":"22:22:22:22:22",
                    "year":2016,
                    "month":2
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_SPORTINFO",
            test:[{
                buttonTitle: "取得使用者運動資訊(週)",
                desc: "key:固定服務驗證碼c3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為week<br>class:型態(cal、step、distance)分別卡路里;步數;距離<br>mac:手環MAC<br>stdate:開始日期yyyy-mm-dd<br>eddate:結束日期yyyy-mm-dd",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"week",
                    "class":"cal",
                    "mac":"22:22:22:22:22",
                    "stdate":"2016-01-31",
                    "eddate ":"2016-02-06"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_SPORTINFO",
            test:[{
                buttonTitle: "取得使用者運動資訊(日)",
                desc: "key:固定服務驗證碼c3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為day<br>class:型態(cal、step、distance)分別卡路里;步數;距離<br>mac:手環MAC<br>date:指定日期yyyy-mm-dd",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"day",
                    "class":"cal",
                    "mac":"22:22:22:22:22",
                    "date":"2016-02-02"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_SPORTINFO",
            test:[{
                buttonTitle: "取得使用者睡眠資訊(年)",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為year<br>mac:手環MAC<br>year:指定年份",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"year",
                    "mac":"22:22:22:22:22",
                    "year":2016
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_SLEEPINFO",
            test:[{
                buttonTitle: "取得使用者睡眠資訊(月)",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為month<br>mac:手環MAC<br>year:指定年份",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"month",
                    "mac":"22:22:22:22:22",
                    "year":2016,
                    "month":2
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_SLEEPINFO",
            test:[{
                buttonTitle: "取得使用者睡眠資訊(週)",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為week<br>mac:手環MAC<br>stdate:開始日期yyyy-mm-dd<br>eddate:結束日期yyyy-mm-dd",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"week",
                    "mac":"22:22:22:22:22",
                    "stdate":"2016-01-31",
                    "eddate":"2016-02-06"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_SLEEPINFO",
            test:[{
                buttonTitle: "取得使用者睡眠資訊(日)",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為day<br>mac:手環MAC<br>day:指定日期yyyy-mm-dd",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"day",
                    "mac":"22:22:22:22:22",
                    "date":"2016-02-02"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_GETACTIVITYBYDATE",
            test:[{
                buttonTitle: "取得使用者活動量資訊(年)",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為range<br>mac:手環MAC<br>year:年份",
                data: {
                    "key": "3fac51abf9b621e5327ec96a1b59dd3e",
                    "account": "aaaa@gmail.com",
                    "type": "year",
                    "class": "activity",
                    "mac": "22:22:22:22:22",
                    "year": 2016
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_GETACTIVITYBYDATE",
            test:[{
                buttonTitle: "取得使用者活動量資訊(區間)",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為range<br>mac:手環MAC<br>stdate:開始日期yyyy-mm-dd<br>eddate:結束日期yyyy-mm-dd",
                data: {
                    "key": "3fac51abf9b621e5327ec96a1b59dd3e",
                    "account": "aaaa@gmail.com",
                    "type": "range",
                    "class": "activity",
                    "mac": "22:22:22:22:22",
                    "stdate": "2017-03-14",
                    "eddate": "2017-03-14"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_GETACTIVITYBYDATE",
            test:[{
                buttonTitle: "取得使用者活動量資訊(月)",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為month<br>mac:手環MAC<br>year:指定年份",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"month",
                    "mac":"22:22:22:22:22",
                    "year":2016,
                    "month":2
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_GETACTIVITYBYDATE",
            test:[{
                buttonTitle: "取得使用者活動量資訊(週)",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為week<br>mac:手環MAC<br>stdate:開始日期yyyy-mm-dd<br>eddate:結束日期yyyy-mm-dd",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"week",
                    "mac":"22:22:22:22:22",
                    "stdate":"2016-01-31",
                    "eddate":"2016-02-06"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_GETACTIVITYBYDATE",
            test:[{
                buttonTitle: "取得使用者活動量資訊(日)",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為day<br>mac:手環MAC<br>day:指定日期yyyy-mm-dd",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"day",
                    "mac":"22:22:22:22:22",
                    "date":"2016-02-02"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_WEIGHTINFO",
            test:[{
                buttonTitle: "取得使用者體重資訊",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:欲查詢之帳號(預設為email)",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_LOGIN",
            test:[{
                buttonTitle: "會員帳號登入(no ccap)",
                desc: "key:固定服務驗證碼27fcc3c31b374c0b98c3a586424fd8be<br>account:帳號(email)<br>pwd: MD5加密過的密碼",
                data: {
                    "acc": "aaaa@gmail.com",
                    "pwd": "098f6bcd4621d373cade4e832627b4f6",
                    "key": "27fcc3c31b374c0b98c3a586424fd8be"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_LOGOUT",
            test:[{
                buttonTitle: "會員帳號登出",
                desc: "",
                data: {}
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_GETGOALBYDATE",
            test:[{
                buttonTitle: "取得用戶每日目標值",
                desc: "account:預查詢之帳號<br>mac:手環MAC<br>date:預查詢之日期yyyy-mm-dd",
                data: {
                    "account": "aaaa@gmail.com",
                    "mac": "22:22:22:22:22",
                    "date": "2016-11-14"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_GETRINGMAC",
            test:[{
                buttonTitle: "取得用戶手環資料",
                desc: "key:固定服務驗證碼8fcad9c6b114a190b58a7852bdd5f67b<br>account:欲查詢之帳號(預設為email)",
                data: {
                    "key": "8fcad9c6b114a190b58a7852bdd5f67b",
                    "account": "aaaa@gmail.com"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_GETRINGTIME",
            test:[{
                buttonTitle: "取得手環最後同步時間",
                desc: "key:固定服務驗證碼6abdf4ade610973d36ead20ebe8ed972<br>account:欲查詢之帳號(預設為email)<br>mac:手環MAC",
                data: {
                    "key": "6abdf4ade610973d36ead20ebe8ed972",
                    "account": "aaaa@gmail.com",
                    "mac": "22:22:22:22:22"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_USERINFODOWNLOAD",
            test:[{
                buttonTitle: "取得使用者基本資料",
                desc: "key:固定服務驗證碼cd81864fabfa2fa4358e85de7d0008c0<br>account:欲查詢之帳號(預設為email)<>",
                data: {
                    "key": "cd81864fabfa2fa4358e85de7d0008c0",
                    "account": "aaaa@gmail.com"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_UP_USER_HEADIMG",
            test:[{
                buttonTitle: "使用者上傳大頭貼",
                desc: "key:固定服務驗證碼ce7f54633d7f55ad191e25e12dae1d4e<br>account:帳號<br>imgData:經編碼為base64字串後的圖片",
                data: {
                    "key": "ce7f54633d7f55ad191e25e12dae1d4e",
                    "account": "aaaa@gmail.com",
                    "imgData": "data:image/png;base64,iVBORw0KGgoAAAANS…."
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_GETUSERBLOCKSETTING",
            test:[{
                buttonTitle: "取得用戶區塊顯示設定",
                desc: "key:固定服務驗證碼fb186bad245098045eede52ea81973f9<br>account:帳號(email)",
                data: {
                    "key": "fb186bad245098045eede52ea81973f9",
                    "account": "aaaa@gmail.com"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_UPDATEUSERBLOCKSETTING",
            test:[{
                buttonTitle: "取得用戶區塊顯示設定",
                desc: "key:固定服務驗證碼2ae72b616d555ea77067908d0721982e<br>account:帳號(email)<br/>exp_block_setting:區塊內容",
                data: {
                    "key": "2ae72b616d555ea77067908d0721982e",
                    "account": "aaaa@gmail.com",
                    "exp_block_setting": ["calories", "step", "distance", "weight", "sleep", "activity", "food"]
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_GETFOODCLASS",
            test:[{
                buttonTitle: "取得食物分類",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>lang:語系(tw繁體中文cn簡體中文us英文)",
                data: {
                    "key": "3fac51abf9b621e5327ec96a1b59dd3e",
                    "lang": "tw"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_GETFOODCLASSITEM",
            test:[{
                buttonTitle: "取得食物分類項目",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>foodid:食物分類ID",
                data: {
                    "key": "3fac51abf9b621e5327ec96a1b59dd3e",
                    "foodid": "F000003"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_UPLOADFOODPLAN",
            test:[{
                buttonTitle: "上傳用戶飲食計劃",
                desc: "key:固定服務驗證碼8822f49812835b3a74539ae2ae52673f<br>account:欲查詢之帳號(預設為email)<br>格式參照breakfast",
                data: {
                    "key": "8822f49812835b3a74539ae2ae52673f",
                    "account": "aaaa@gmail.com",
                    "data": {
                        "plandatetime": "2017-03-14",
                        "breakfast": [{
                            "food_id": "A001",
                            "food": "米飯",
                            "units": 1,
                            "kcal": 183
                        }],
                        "lunch": [],
                        "dinner": [],
                        "other": [],
                        "totalkcal": 183
                    }
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_DOWNLOADFOODPLAN",
            test:[{
                buttonTitle: "取得用戶飲食計劃",
                desc: "key:固定服務驗證碼e7bfaeb34e84f8c123de9460464602a9<br>account:欲查詢之帳號(預設為email)<br>plandatetime:欲查詢日期",
                data: {
                    "key": "e7bfaeb34e84f8c123de9460464602a9",
                    "account": "aaaa@gmail.com",
                    "plandatetime": "2017-03-14"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_GETFOODTOTALKCAL",
            test:[{
                buttonTitle: "取得用戶飲食計劃熱量值",
                desc: "key:固定服務驗證碼5764d940ca640a9d44905f5301ffc24f<br>account:欲查詢之帳號(預設為email)<br>type:只有week可以選擇<br>month:指定月份<br>year:指定年份<br>stdate:開始日期<br>eddate:結束日期",
                data: {
                    "key": "5764d940ca640a9d44905f5301ffc24f",
                    "account": "aaaa@gmail.com",
                    "type": "week",
                    "year": 2017,
                    "month": 5,
                    "date": "2017-05-25",
                    "stdate": "2017-05-25",
                    "eddate": "2017-05-31"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_GETFOODTOTALKCAL",
            test:[{
                buttonTitle: "取得用戶飲食計劃熱量值",
                desc: "key:固定服務驗證碼5764d940ca640a9d44905f5301ffc24f<br>account:欲查詢之帳號(預設為email)<br>type:只有week可以選擇<br>month:指定月份<br>year:指定年份<br>stdate:開始日期<br>eddate:結束日期",
                data: {
                    "key": "5764d940ca640a9d44905f5301ffc24f",
                    "account": "aaaa@gmail.com",
                    "type": "week",
                    "year": 2017,
                    "month": 5,
                    "date": "2017-05-25",
                    "stdate": "2017-05-25",
                    "eddate": "2017-05-31"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_GETFOODTOTALKCAL",
            test:[{
                buttonTitle: "取得用戶飲食計劃熱量值",
                desc: "key:固定服務驗證碼5764d940ca640a9d44905f5301ffc24f<br>account:欲查詢之帳號(預設為email)<br>type:只有week可以選擇<br>month:指定月份<br>year:指定年份<br>stdate:開始日期<br>eddate:結束日期",
                data: {
                    "key": "5764d940ca640a9d44905f5301ffc24f",
                    "account": "aaaa@gmail.com",
                    "type": "week",
                    "year": 2017,
                    "month": 5,
                    "date": "2017-05-25",
                    "stdate": "2017-05-25",
                    "eddate": "2017-05-31"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_DELFOODPLAN",
            test:[{
                buttonTitle: "用戶刪除飲食計畫",
                desc: "key:固定服務驗證碼4d68e7fa9845a8959bed0af23ab05414<br>account:欲查詢之帳號(預設為email)<br>plandatetime:日期",
                data: {
                    "key": "4d68e7fa9845a8959bed0af23ab05414",
                    "account": "aaaa@gmail.com",
                    "plandatetime": "2017-05-31"
                }
            }] 
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_CLRFOODAVGKCAL",
            test:[{
                buttonTitle: "用戶清除飲食計畫卡路里平均值",
                desc: "key:固定服務驗證碼48a7c11ed1ccb8766e92d879c106a741<br>account:欲查詢之帳號(預設為email)<br>plandatetime:日期",
                data: {
                    "key": "48a7c11ed1ccb8766e92d879c106a741",
                    "account": " aaaa@gmail.com",
                    "plandatetime": "2017-06-01"
                }
            }] 
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_RESETACCOUNT",
            test:[{
                buttonTitle: "用戶重設密碼",
                desc: "key:固定服務驗證碼9e697e63b257e4a630dd4d4409c05a45<br>account:欲查詢之帳號(預設為email)<br>plandatetime:日期",
                data: {
                    "key": "9e697e63b257e4a630dd4d4409c05a45",
                    "account": " aaaa@gmail.com"
                }
            }] 
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_VERIFICATIONRSTACCOUNT",
            test:[{
                buttonTitle: "用戶忘記帳號token驗證",
                desc: "a:帳號<br>v:驗證參數<br>t:驗證參數<br>key:固定服務驗證碼2d6ae242b526bbd0245b1bb8be98ea93",
                data: {
                    "a": "aaaa@gmail.com",
                    "t": "1496297114298",
                    "v": "87910bc8051235463e57ad306a5deeb2",
                    "key": "2d6ae242b526bbd0245b1bb8be98ea93"
                }
            }] 
        },{
            type: "POST",
            url: "http://139.162.112.225/WIS_NEWPWDRECOVERY",
            test:[{
                buttonTitle: "用戶送出新的密碼",
                desc: "a:帳號<br>v:驗證參數<br>t:驗證參數<br>key:固定服務驗證碼1822e527f87be633ba8822e0709007be<br>passkey:驗證驗證信連結取得之驗證key<br>password:MD5新密碼",
                data: {
                    "a": "aaaa@gmail.com",
                    "passkey": "65d956005af18c6a5159f7f67341ef4a",
                    "password": "52467e98ed69ad0e5c3cf1f27212e8ad",
                    "v": "87910bc8051235463e57ad306a5deeb2",
                    "key": "1822e527f87be633ba8822e0709007be"
                }
            }] 
        }],    
        wismelinode_data:[{
            type: "POST",
            url: "http://139.162.112.225/api/auth",
            test:[{
                buttonTitle: "取得Server token",
                desc: "格式為account,password",
                data: {
                    "account" : "aaaa@gmail.com",
                    "password": "098f6bcd4621d373cade4e832627b4f6"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/api/data/sport",
            test:[{
                buttonTitle: "取得使用者運動資訊(年)",
                desc: "key:固定服務驗證碼c3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為year<br>class:型態(cal、step、distance)分別卡路里;步數;距離<br>mac:手環MAC<br>year:指定年份",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"year",
                    "class":"cal",
                    "mac":"22:22:22:22:22",
                    "year":2016
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/api/data/sport",
            test:[{
                buttonTitle: "取得使用者運動資訊(月)",
                desc: "key:固定服務驗證碼c3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為month<br>class:型態(cal、step、distance)分別卡路里;步數;距離<br>mac:手環MAC<br>year:指定年份<br>month:指定月份",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"month",
                    "class":"cal",
                    "mac":"22:22:22:22:22",
                    "year":2016,
                    "month":2
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/api/data/sport",
            test:[{
                buttonTitle: "取得使用者運動資訊(週)",
                desc: "key:固定服務驗證碼c3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為week<br>class:型態(cal、step、distance)分別卡路里;步數;距離<br>mac:手環MAC<br>stdate:開始日期yyyy-mm-dd<br>eddate:結束日期yyyy-mm-dd",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"week",
                    "class":"cal",
                    "mac":"22:22:22:22:22",
                    "stdate":"2016-01-31",
                    "eddate ":"2016-02-06"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/api/data/sport",
            test:[{
                buttonTitle: "取得使用者運動資訊(日)",
                desc: "key:固定服務驗證碼c3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為day<br>class:型態(cal、step、distance)分別卡路里;步數;距離<br>mac:手環MAC<br>date:指定日期yyyy-mm-dd",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"day",
                    "class":"cal",
                    "mac":"22:22:22:22:22",
                    "date":"2016-02-02"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/api/data/sleep",
            test:[{
                buttonTitle: "取得使用者睡眠資訊(年)",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為year<br>mac:手環MAC<br>year:指定年份",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"year",
                    "mac":"22:22:22:22:22",
                    "year":2016
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/api/data/sleep",
            test:[{
                buttonTitle: "取得使用者睡眠資訊(月)",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為month<br>mac:手環MAC<br>year:指定年份",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"month",
                    "mac":"22:22:22:22:22",
                    "year":2016,
                    "month":2
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/api/data/sleep",
            test:[{
                buttonTitle: "取得使用者睡眠資訊(週)",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為week<br>mac:手環MAC<br>stdate:開始日期yyyy-mm-dd<br>eddate:結束日期yyyy-mm-dd",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"week",
                    "mac":"22:22:22:22:22",
                    "stdate":"2016-01-31",
                    "eddate":"2016-02-06"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/api/data/sleep",
            test:[{
                buttonTitle: "取得使用者睡眠資訊(日)",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為day<br>mac:手環MAC<br>day:指定日期yyyy-mm-dd",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"day",
                    "mac":"22:22:22:22:22",
                    "date":"2016-02-02"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/api/data/activity",
            test:[{
                buttonTitle: "取得使用者活動量資訊(年)",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為range<br>mac:手環MAC<br>year:年份",
                data: {
                    "key": "3fac51abf9b621e5327ec96a1b59dd3e",
                    "account": "aaaa@gmail.com",
                    "type": "year",
                    "class": "activity",
                    "mac": "22:22:22:22:22",
                    "year": 2016
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/api/data/activity",
            test:[{
                buttonTitle: "取得使用者活動量資訊(區間)",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為range<br>mac:手環MAC<br>stdate:開始日期yyyy-mm-dd<br>eddate:結束日期yyyy-mm-dd",
                data: {
                    "key": "3fac51abf9b621e5327ec96a1b59dd3e",
                    "account": "aaaa@gmail.com",
                    "type": "range",
                    "class": "activity",
                    "mac": "22:22:22:22:22",
                    "stdate": "2017-03-14",
                    "eddate": "2017-03-14"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/api/data/activity",
            test:[{
                buttonTitle: "取得使用者活動量資訊(月)",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為month<br>mac:手環MAC<br>year:指定年份",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"month",
                    "mac":"22:22:22:22:22",
                    "year":2016,
                    "month":2
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/api/data/activity",
            test:[{
                buttonTitle: "取得使用者活動量資訊(週)",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為week<br>mac:手環MAC<br>stdate:開始日期yyyy-mm-dd<br>eddate:結束日期yyyy-mm-dd",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"week",
                    "mac":"22:22:22:22:22",
                    "stdate":"2016-01-31",
                    "eddate":"2016-02-06"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/api/data/activity",
            test:[{
                buttonTitle: "取得使用者活動量資訊(日)",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為day<br>mac:手環MAC<br>day:指定日期yyyy-mm-dd",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"day",
                    "mac":"22:22:22:22:22",
                    "date":"2016-02-02"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/api/data/weight",
            test:[{
                buttonTitle: "取得使用者體重資訊",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:欲查詢之帳號(預設為email)",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/api/data/goalByDate",
            test:[{
                buttonTitle: "取得用戶每日目標值",
                desc: "account:預查詢之帳號<br>mac:手環MAC<br>date:預查詢之日期yyyy-mm-dd",
                data: {
                    "account": "aaaa@gmail.com",
                    "mac": "22:22:22:22:22",
                    "date": "2016-11-14"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/api/data/userRingMac",
            test:[{
                buttonTitle: "取得手環mac序號",
                desc: "key:固定服務驗證碼8fcad9c6b114a190b58a7852bdd5f67b<br>account:欲查詢之帳號(預設為email)",
                data: {
                    "key": "8fcad9c6b114a190b58a7852bdd5f67b",
                    "account": "aaaa@gmail.com"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/api/data/ringTime",
            test:[{
                buttonTitle: "取得手環最後同步時間",
                desc: "key:固定服務驗證碼6abdf4ade610973d36ead20ebe8ed972<br>account:欲查詢之帳號(預設為email)<br>mac:手環MAC",
                data: {
                    "key": "6abdf4ade610973d36ead20ebe8ed972",
                    "account": "aaaa@gmail.com",
                    "mac": "22:22:22:22:22"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/api/data/userInfo",
            test:[{
                buttonTitle: "取得使用者基本資料",
                desc: "key:固定服務驗證碼cd81864fabfa2fa4358e85de7d0008c0<br>account:欲查詢之帳號(預設為email)",
                data: {
                    "key": "cd81864fabfa2fa4358e85de7d0008c0",
                    "account": "aaaa@gmail.com"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/api/data/foodClass",
            test:[{
                buttonTitle: "取得食物分類",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>lang:語系(tw繁體中文cn簡體中文us英文)",
                data: {
                    "key": "3fac51abf9b621e5327ec96a1b59dd3e",
                    "lang": "tw"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/api/data/foodItem",
            test:[{
                buttonTitle: "取得食物分類項目",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>foodid:食物分類ID",
                data: {
                    "key": "3fac51abf9b621e5327ec96a1b59dd3e",
                    "foodid": "F000003"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/api/data/uploadFoodPlan",
            test:[{
                buttonTitle: "上傳用戶飲食計劃",
                desc: "key:固定服務驗證碼8822f49812835b3a74539ae2ae52673f<br>account:欲查詢之帳號(預設為email)<br>格式參照breakfast",
                data: {
                    "key": "8822f49812835b3a74539ae2ae52673f",
                    "account": "aaaa@gmail.com",
                    "data": {
                        "plandatetime": "2017-03-14",
                        "breakfast": [{
                            "food_id": "A001",
                            "food": "米飯",
                            "units": 1,
                            "kcal": 183
                        }],
                        "lunch": [],
                        "dinner": [],
                        "other": [],
                        "totalkcal": 183
                    }
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/api/data/downloadFoodPlan",
            test:[{
                buttonTitle: "取得用戶飲食計劃",
                desc: "key:固定服務驗證碼e7bfaeb34e84f8c123de9460464602a9<br>account:欲查詢之帳號(預設為email)<br>plandatetime:欲查詢日期",
                data: {
                    "key": "e7bfaeb34e84f8c123de9460464602a9",
                    "account": "aaaa@gmail.com",
                    "plandatetime": "2017-03-14"
                }
            }]
        },{
            type: "POST",
            url: "http://139.162.112.225/api/data/totalKCal",
            test:[{
                buttonTitle: "取得用戶飲食計劃熱量值",
                desc: "key:固定服務驗證碼5764d940ca640a9d44905f5301ffc24f<br>account:欲查詢之帳號(預設為email)<br>type:只有week可以選擇<br>month:指定月份<br>year:指定年份<br>stdate:開始日期<br>eddate:結束日期",
                data: {
                    "key": "5764d940ca640a9d44905f5301ffc24f",
                    "account": "aaaa@gmail.com",
                    "type": "week",
                    "year": 2017,
                    "month": 5,
                    "date": "2017-05-25",
                    "stdate": "2017-05-25",
                    "eddate": "2017-05-31"
                }
            }]
        }],
        wismelinode_ring:[{
            type: 'POST',
            url: "http://139.162.112.225/api/ring/data",
            test: [{
                buttonTitle : "傳送手環資訊",
                desc: "以下內容須將JSON資料壓縮成GZIP格式",
                data: {
                    "key": "b25e41a5bb36313724f69465695c856a",
                    "account": "aaaa@gmail.com",
                    "pwd": "098f6bcd4621d373cade4e832627b4f6",
                    "device_id": "A1A2df5ds6f8ds8d4d45s6d4f5dd21dS21fd25s",
                    "mac": "00:00:00:00:00",
                    "RAW_DATA": [{
                        "account": "aaaa@gmail.com",
                        "dateTime": "2016-01-01 00:00:00",
                        "date": "2016-01-01",
                        "time": "00:00:00",
                        "hour": 0,
                        "MAC": "00:00:00:00:00",
                        "dataType": "",
                        "stepsPM": 10,
                        "distancePM": 10,
                        "calariePM": 10,
                        "heartRateAvePM": 1,
                        "heartRateMaxPM": 1,
                        "heartRateMinPM": 1,
                        "activityPM": 1,
                        "weigthPM": 1,
                        "heightPM": 1,
                        "G1": 1,
                        "G2": 1,
                        "G3": 1,
                        "G4": 1,
                        "G5": 1,
                        "G6": 1,
                        "G7": 1,
                        "G8": 1,
                        "G9": 1,
                        "G10": 1
                    }, {
                        "account": "aaaa@gmail.com",
                        "dateTime": "2016-01-01 00:00:01",
                        "date": "2016-01-01",
                        "time": "00:00:00",
                        "hour": 0,
                        "MAC": "00:00:00:00:00",
                        "dataType": "",
                        "stepsPM": 20,
                        "distancePM": 20,
                        "calariePM": 20,
                        "heartRateAvePM": 2,
                        "heartRateMaxPM": 2,
                        "heartRateMinPM": 2,
                        "activityPM": 2,
                        "weigthPM": 2,
                        "heightPM": 2,
                        "G1": 2,
                        "G2": 2,
                        "G3": 2,
                        "G4": 2,
                        "G5": 2,
                        "G6": 2,
                        "G7": 2,
                        "G8": 2,
                        "G9": 2,
                        "G10": 2
                    }],
                    "STEPS_DATA": [{
                        "account": "aaaa@gmail.com",
                        "dateTime": "2016-01-01 00:00:00",
                        "date": "2016-01-01",
                        "time": "22:22:21",
                        "year": 2016,
                        "month": 1,
                        "MAC": "00:00:00:00:00",
                        "goalStep": 500,
                        "stepSum": 300,
                        "hour1": 0,
                        "hour2": 0,
                        "hour3": 0,
                        "hour4": 10,
                        "hour5": 10,
                        "hour6": 10,
                        "hour7": 10,
                        "hour8": 10,
                        "hour9": 20,
                        "hour10": 20,
                        "hour11": 20,
                        "hour12": 30,
                        "hour13": 30,
                        "hour14": 30,
                        "hour15": 30,
                        "hour16": 40,
                        "hour17": 0,
                        "hour18": 0,
                        "hour19": 0,
                        "hour20": 0,
                        "hour21": 0,
                        "hour22": 0,
                        "hour23": 0,
                        "hour24": 0,
                        "completeness": 50
                    }],
                    "DISTANCE_DATA": [{
                        "account": "aaaa@gmail.com",
                        "dateTime": "2016-01-01 00:00:00",
                        "date": "2016-01-01",
                        "time": "22:22:21",
                        "year": 2016,
                        "month": 1,
                        "MAC": "00:00:00:00:00",
                        "goalDistance": 500,
                        "distanceSum": 300,
                        "hour1": 0,
                        "hour2": 0,
                        "hour3": 0,
                        "hour4": 10,
                        "hour5": 10,
                        "hour6": 10,
                        "hour7": 10,
                        "hour8": 10,
                        "hour9": 20,
                        "hour10": 20,
                        "hour11": 20,
                        "hour12": 30,
                        "hour13": 30,
                        "hour14": 30,
                        "hour15": 30,
                        "hour16": 40,
                        "hour17": 0,
                        "hour18": 0,
                        "hour19": 0,
                        "hour20": 0,
                        "hour21": 0,
                        "hour22": 0,
                        "hour23": 0,
                        "hour24": 0,
                        "completeness": 50
                    }],
                    "CALARIE_DATA": [{
                        "account": "aaaa@gmail.com",
                        "dateTime": "2016-01-01 00:00:00",
                        "date": "2016-01-01",
                        "time": "22:22:21",
                        "year": 2016,
                        "month": 1,
                        "MAC": "00:00:00:00:00",
                        "goalCal": 500,
                        "calSum": 300,
                        "hour1": 0,
                        "hour2": 0,
                        "hour3": 0,
                        "hour4": 10,
                        "hour5": 10,
                        "hour6": 10,
                        "hour7": 10,
                        "hour8": 10,
                        "hour9": 20,
                        "hour10": 20,
                        "hour11": 20,
                        "hour12": 30,
                        "hour13": 30,
                        "hour14": 30,
                        "hour15": 30,
                        "hour16": 40,
                        "hour17": 0,
                        "hour18": 0,
                        "hour19": 0,
                        "hour20": 0,
                        "hour21": 0,
                        "hour22": 0,
                        "hour23": 0,
                        "hour24": 0,
                        "completeness": 50
                    }],
                    "SLEEP_DATA": [{
                        "account": "aaaa@gmail.com",
                        "dateTime": "2016-01-01 00:00:00",
                        "date": "2016-01-01",
                        "time": "10:30:59",
                        "year": 2016,
                        "month": 5,
                        "MAC": "00:00:00:00:00",
                        "sleepSum": 5000,
                        "lightsleepSum": 2000,
                        "deepsleepSum": 3000
                    }],
                    "HEARTRATE_DATA": [{
                        "account": "aaaa@gmail.com",
                        "dateTime": "2016-01-01 00:00:00",
                        "date": "2016-01-01",
                        "time": "10:30:59",
                        "MAC": "00:00:00:00:00",
                        "heartRateAvePM": 90,
                        "heartRateMaxPM": 100,
                        "heartRateMinPM": 70
                    }],
                    "ACTIVITY_DATA": [{
                        "account": "aaaa@gmail.com",
                        "dateTime": "2016-01-01 00:00:00",
                        "date": "2016-01-01",
                        "time": "10:30:59",
                        "year": 2016,
                        "month": 6,
                        "MAC": "00:00:00:00:00",
                        "lowActivityMin": 5000,
                        "generalActivityMin": 3000,
                        "bigActivityMin": 3000,
                        "highActivityMin": 3000,
                        "maxActivityMin": 3000,
                        "restActivityMin": 3000,
                        "goalActivity": 20
                    }],
                    "WEIGHT_DATA": [{
                        "account": "aaaa@gmail.com",
                        "dateTime": "2016-01-01 00:00:00",
                        "date": "2016-01-01",
                        "time": "10:30:59",
                        "year": 2016,
                        "month": 6,
                        "weight": 80
                    }]
                }
            }]
        },{
            type: 'POST',
            url: "http://139.162.112.225/api/ring/userData",
            test: [{
                buttonTitle : "傳送使用者資料",
                desc: "",
                data: {
                    "key": "ccecb53b5e8cb2eda139c2a4257cbc86",
                    "account": "aaaa@gmail.com",
                    "pwd": "098f6bcd4621d373cade4e832627b4f6",
                    "USER_MESSAGE": {
                        "account": "aaaa@gmail.com",
                        "password": "098f6bcd4621d373cade4e832627b4f6",
                        "userName": "peter",
                        "sex": "男",
                        "birthday": "2016-01-02",
                        "portrait": "iVBORw0KGgoAAAANSUhEUgAAAEQAAABECAIAAAC3cQTlAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAACLSURBVGhD3ccxDQAwDMCw8odbAutvBpnkx/P2Hz7Np/k0n+bTfJpP82k+zaf5NJ/m03yaT/NpPs2n+TSf5tN8mk/zaT7Np/k0n+bTfJpP82k+zaf5NJ/m03yaT/NpPs2n+TSf5tN8mk/zaT7Np/k0n+bTfJpP82k+zaf5NJ/m03yaT/NpPs2n+bCdA7d8Ex7dcvk5AAAAAElFTkSuQmCC",
                        "height": 167,
                        "weight": 55,
                        "goalStep": 3000,
                        "goalDistance": 3000,
                        "goalCalarie": 3000,
                        "goalActivity": 20
                    },
                    "USER_SETTINGS_MESSAGE": {
                        "account": "aaaa@gmail.com",
                        "wearPosition": "a",
                        "heartRateTime1": "2016-01-01 00:00:00",
                        "heartRateSwitch1": 0,
                        "heartRateTime2": "2016-01-01 00:00:00",
                        "heartRateSwitch2": 0,
                        "heartRateTime3": "2016-01-01 00:00:00",
                        "heartRateSwitch3": 0,
                        "heartRateTime4": "2016-01-01 00:00:00",
                        "heartRateSwitch4": 0,
                        "heartRateTime5": "2016-01-01 00:00:00",
                        "heartRateSwitch5": 0,
                        "sdStartTime": "2016-01-01 00:00:00",
                        "sdEndTime": "2016-01-01 00:00:00",
                        "sdIntervalTime": "2016-01-01 00:00:00",
                        "sdSwitch": 0,
                        "phoneSwitch": 0,
                        "noteSwitch": 0,
                        "messageSwitch": 0,
                        "bitcomSwitch": 0,
                        "alarmclockTime1": "2016-01-01 00:00:00",
                        "alarmclockSwitch1": 0,
                        "alarmclockTime2": "2016-01-01 00:00:00",
                        "alarmclockSwitch2": 0,
                        "alarmclockTime3": "2016-01-01 00:00:00",
                        "alarmclockSwitch3": 0,
                        "alarmclockTime4": "2016-01-01 00:00:00",
                        "alarmclockSwitch4": 0,
                        "alarmclockTime5": "2016-01-01 00:00:00",
                        "alarmclockSwitch5": 0,
                        "switch3G": 0,
                        "brigthnessValue": 0,
                        "unit": 0,
                        "electricQuantityValue": 3000,
                        "mapType": "google"
                    },
                    "DEVICES_MESSAGE": {
                        "account": "aaaa@gmail.com",
                        "ANDROID_MAC": "",
                        "bindingCode": "3000",
                        "solutionState": 1,
                        "IOS_UUID": "",
                        "dateTime": "2016-01-01 00:00:00",
                        "heartRateType": 0
                    }
                }
            }]
        },{
            type: 'POST',
            url: "http://139.162.112.225/api/ring/time",
            test: [{
                buttonTitle : "取得手環最後上傳時間",
                desc: "",
                data: {
                    "key": "60ea62840f54763fa29ae93ae7c70671",
                    "acc": "aaaa@gmail.com",
                    "pwd": "098f6bcd4621d373cade4e832627b4f6",
                    "mac": "00:00:00:00:00",
                }
            }]
        },{
            type: 'POST',
            url: "http://139.162.112.225/api/ring/download",
            test: [{
                buttonTitle : "取得手環下載資料",
                desc: "回傳格式為gzip",
                data: {
                    "key": "15df176a072c8e349f135ea567736834",
                    "acc": "aaaa@gmail.com",
                    "pwd": "098f6bcd4621d373cade4e832627b4f6",
                    "start_datetime":"2016-01-01 00:00",
                    "end_datetime":"2016-12-01 00:00",
                    "mac":"00:00:00:00:00"
                }
            }]
        },{
            type: 'POST',
            url: "http://139.162.112.225/api/ring/userDownload",
            test: [{
                buttonTitle : "取得使用者帳號資料",
                desc: "",
                data: {
                    "key": "c5ad4b06e0ebd3a68d92a78b3cf1a8f0",
                    "acc": "aaaa@gmail.com",
                    "pwd": "098f6bcd4621d373cade4e832627b4f6",
                }
            }]
        },{
            type: 'POST',
            url: "http://139.162.112.225/api/ring/delete",
            test: [{
                buttonTitle : "刪除手環RawData資料",
                desc: "",
                data:{
                    "key": "fb96b3f05bc6a9ac5c02c09d0f46192a",
                    "acc": "aaaa@gmail.com",
                    "pwd": "098f6bcd4621d373cade4e832627b4f6",
                    "mac":"00:00:00:00:00",
                    "datetime":"2016-01-01 00:00"
                }
            }]
        },{
            type: 'POST',
            url: "http://139.162.112.225/api/users",
            test: [{
                buttonTitle : "註冊使用者帳號",
                desc: "",
                data:{
                    "key": "d4ddaa1babd2730cac2f23387ddfefea",
                    "acc": "aaaa@gmail.com",
                    "pwd": "098f6bcd4621d373cade4e832627b4f6"
                }
            }]
        },{
            type: 'POST',
            url: "http://139.162.112.225/api/users/login",
            test: [{
                buttonTitle : "App帳號驗證",
                desc: "",
                data:{
                    "key": "27fcc3c31b374c0b98c3a586424fd8be",
                    "acc": "aaaa@gmail.com",
                    "pwd": "098f6bcd4621d373cade4e832627b4f6"
                }
            }]
        }],
        wismelinodebackup:[{
            type: 'GET',
            url: "http://139.162.112.225/api/backup",
            test: [{
                buttonTitle : "下載資料庫備份檔案",
                desc: "",
                data: {},
                data2: {
                    downloadUrl: "www.wisdat.net/restapiWismetest/backup.tar.gz",
                    success: true
                }
            }]

        }],
        wisme2Frontend:[{
            type: "POST",
            url: "http://103.211.167.83/WIS_SPORTINFO",
            test:[{
                buttonTitle: "取得使用者運動資訊(年)",
                desc: "key:固定服務驗證碼c3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為year<br>class:型態(cal、step、distance)分別卡路里;步數;距離<br>mac:手環MAC<br>year:指定年份",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"year",
                    "class":"cal",
                    "mac":"22:22:22:22:22",
                    "year":2016
                }
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_SPORTINFO",
            test:[{
                buttonTitle: "取得使用者運動資訊(月)",
                desc: "key:固定服務驗證碼c3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為month<br>class:型態(cal、step、distance)分別卡路里;步數;距離<br>mac:手環MAC<br>year:指定年份<br>month:指定月份",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"month",
                    "class":"cal",
                    "mac":"22:22:22:22:22",
                    "year":2016,
                    "month":2
                }
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_SPORTINFO",
            test:[{
                buttonTitle: "取得使用者運動資訊(週)",
                desc: "key:固定服務驗證碼c3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為week<br>class:型態(cal、step、distance)分別卡路里;步數;距離<br>mac:手環MAC<br>stdate:開始日期yyyy-mm-dd<br>eddate:結束日期yyyy-mm-dd",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"week",
                    "class":"cal",
                    "mac":"22:22:22:22:22",
                    "stdate":"2016-01-31",
                    "eddate ":"2016-02-06"
                }
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_SPORTINFO",
            test:[{
                buttonTitle: "取得使用者運動資訊(日)",
                desc: "key:固定服務驗證碼c3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為day<br>class:型態(cal、step、distance)分別卡路里;步數;距離<br>mac:手環MAC<br>date:指定日期yyyy-mm-dd",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"day",
                    "class":"cal",
                    "mac":"22:22:22:22:22",
                    "date":"2016-02-02"
                }
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_SPORTINFO",
            test:[{
                buttonTitle: "取得使用者睡眠資訊(年)",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為year<br>mac:手環MAC<br>year:指定年份",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"year",
                    "mac":"22:22:22:22:22",
                    "year":2016
                }
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_SLEEPINFO",
            test:[{
                buttonTitle: "取得使用者睡眠資訊(月)",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為month<br>mac:手環MAC<br>year:指定年份",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"month",
                    "mac":"22:22:22:22:22",
                    "year":2016,
                    "month":2
                }
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_SLEEPINFO",
            test:[{
                buttonTitle: "取得使用者睡眠資訊(週)",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為week<br>mac:手環MAC<br>stdate:開始日期yyyy-mm-dd<br>eddate:結束日期yyyy-mm-dd",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"week",
                    "mac":"22:22:22:22:22",
                    "stdate":"2016-01-31",
                    "eddate":"2016-02-06"
                }
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_SLEEPINFO",
            test:[{
                buttonTitle: "取得使用者睡眠資訊(日)",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為day<br>mac:手環MAC<br>day:指定日期yyyy-mm-dd",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"day",
                    "mac":"22:22:22:22:22",
                    "date":"2016-02-02"
                }
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_GETACTIVITYBYDATE",
            test:[{
                buttonTitle: "取得使用者活動量資訊(年)",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為range<br>mac:手環MAC<br>year:年份",
                data: {
                    "key": "3fac51abf9b621e5327ec96a1b59dd3e",
                    "account": "aaaa@gmail.com",
                    "type": "year",
                    "class": "activity",
                    "mac": "22:22:22:22:22",
                    "year": 2016
                }
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_GETACTIVITYBYDATE",
            test:[{
                buttonTitle: "取得使用者活動量資訊(區間)",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為range<br>mac:手環MAC<br>stdate:開始日期yyyy-mm-dd<br>eddate:結束日期yyyy-mm-dd",
                data: {
                    "key": "3fac51abf9b621e5327ec96a1b59dd3e",
                    "account": "aaaa@gmail.com",
                    "type": "range",
                    "class": "activity",
                    "mac": "22:22:22:22:22",
                    "stdate": "2017-03-14",
                    "eddate": "2017-03-14"
                }
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_GETACTIVITYBYDATE",
            test:[{
                buttonTitle: "取得使用者活動量資訊(月)",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為month<br>mac:手環MAC<br>year:指定年份",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"month",
                    "mac":"22:22:22:22:22",
                    "year":2016,
                    "month":2
                }
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_GETACTIVITYBYDATE",
            test:[{
                buttonTitle: "取得使用者活動量資訊(週)",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為week<br>mac:手環MAC<br>stdate:開始日期yyyy-mm-dd<br>eddate:結束日期yyyy-mm-dd",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"week",
                    "mac":"22:22:22:22:22",
                    "stdate":"2016-01-31",
                    "eddate":"2016-02-06"
                }
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_GETACTIVITYBYDATE",
            test:[{
                buttonTitle: "取得使用者活動量資訊(日)",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:帳號(預設為email)<br>type:固定為day<br>mac:手環MAC<br>day:指定日期yyyy-mm-dd",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com",
                    "type":"day",
                    "mac":"22:22:22:22:22",
                    "date":"2016-02-02"
                }
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_WEIGHTINFO",
            test:[{
                buttonTitle: "取得使用者體重資訊",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>account:欲查詢之帳號(預設為email)",
                data: {
                    "key":"3fac51abf9b621e5327ec96a1b59dd3e",
                    "account":"aaaa@gmail.com"
                }
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_LOGIN",
            test:[{
                buttonTitle: "會員帳號登入(no ccap)",
                desc: "key:固定服務驗證碼27fcc3c31b374c0b98c3a586424fd8be<br>account:帳號(email)<br>pwd: MD5加密過的密碼",
                data: {
                    "acc": "aaaa@gmail.com",
                    "pwd": "098f6bcd4621d373cade4e832627b4f6",
                    "key": "27fcc3c31b374c0b98c3a586424fd8be"
                }
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_LOGOUT",
            test:[{
                buttonTitle: "會員帳號登出",
                desc: "",
                data: {}
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_GETGOALBYDATE",
            test:[{
                buttonTitle: "取得用戶每日目標值",
                desc: "account:預查詢之帳號<br>mac:手環MAC<br>date:預查詢之日期yyyy-mm-dd",
                data: {
                    "account": "aaaa@gmail.com",
                    "mac": "22:22:22:22:22",
                    "date": "2016-11-14"
                }
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_GETRINGMAC",
            test:[{
                buttonTitle: "取得用戶手環資料",
                desc: "key:固定服務驗證碼8fcad9c6b114a190b58a7852bdd5f67b<br>account:欲查詢之帳號(預設為email)",
                data: {
                    "key": "8fcad9c6b114a190b58a7852bdd5f67b",
                    "account": "aaaa@gmail.com"
                }
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_GETRINGTIME",
            test:[{
                buttonTitle: "取得手環最後同步時間",
                desc: "key:固定服務驗證碼6abdf4ade610973d36ead20ebe8ed972<br>account:欲查詢之帳號(預設為email)<br>mac:手環MAC",
                data: {
                    "key": "6abdf4ade610973d36ead20ebe8ed972",
                    "account": "aaaa@gmail.com",
                    "mac": "22:22:22:22:22"
                }
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_USERINFODOWNLOAD",
            test:[{
                buttonTitle: "取得使用者基本資料",
                desc: "key:固定服務驗證碼cd81864fabfa2fa4358e85de7d0008c0<br>account:欲查詢之帳號(預設為email)<>",
                data: {
                    "key": "cd81864fabfa2fa4358e85de7d0008c0",
                    "account": "aaaa@gmail.com"
                }
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_UP_USER_HEADIMG",
            test:[{
                buttonTitle: "使用者上傳大頭貼",
                desc: "key:固定服務驗證碼ce7f54633d7f55ad191e25e12dae1d4e<br>account:帳號<br>imgData:經編碼為base64字串後的圖片",
                data: {
                    "key": "ce7f54633d7f55ad191e25e12dae1d4e",
                    "account": "aaaa@gmail.com",
                    "imgData": "data:image/png;base64,iVBORw0KGgoAAAANS…."
                }
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_GETUSERBLOCKSETTING",
            test:[{
                buttonTitle: "取得用戶區塊顯示設定",
                desc: "key:固定服務驗證碼fb186bad245098045eede52ea81973f9<br>account:帳號(email)",
                data: {
                    "key": "fb186bad245098045eede52ea81973f9",
                    "account": "aaaa@gmail.com"
                }
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_UPDATEUSERBLOCKSETTING",
            test:[{
                buttonTitle: "取得用戶區塊顯示設定",
                desc: "key:固定服務驗證碼2ae72b616d555ea77067908d0721982e<br>account:帳號(email)<br/>exp_block_setting:區塊內容",
                data: {
                    "key": "2ae72b616d555ea77067908d0721982e",
                    "account": "aaaa@gmail.com",
                    "exp_block_setting": ["calories", "step", "distance", "weight", "sleep", "activity", "food"]
                }
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_GETFOODCLASS",
            test:[{
                buttonTitle: "取得食物分類",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>lang:語系(tw繁體中文cn簡體中文us英文)",
                data: {
                    "key": "3fac51abf9b621e5327ec96a1b59dd3e",
                    "lang": "tw"
                }
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_GETFOODCLASSITEM",
            test:[{
                buttonTitle: "取得食物分類項目",
                desc: "key:固定服務驗證碼3fac51abf9b621e5327ec96a1b59dd3e<br>foodid:食物分類ID",
                data: {
                    "key": "3fac51abf9b621e5327ec96a1b59dd3e",
                    "foodid": "F000003"
                }
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_UPLOADFOODPLAN",
            test:[{
                buttonTitle: "上傳用戶飲食計劃",
                desc: "key:固定服務驗證碼8822f49812835b3a74539ae2ae52673f<br>account:欲查詢之帳號(預設為email)<br>格式參照breakfast",
                data: {
                    "key": "8822f49812835b3a74539ae2ae52673f",
                    "account": "aaaa@gmail.com",
                    "data": {
                        "plandatetime": "2017-03-14",
                        "breakfast": [{
                            "food_id": "A001",
                            "food": "米飯",
                            "units": 1,
                            "kcal": 183
                        }],
                        "lunch": [],
                        "dinner": [],
                        "other": [],
                        "totalkcal": 183
                    }
                }
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_DOWNLOADFOODPLAN",
            test:[{
                buttonTitle: "取得用戶飲食計劃",
                desc: "key:固定服務驗證碼e7bfaeb34e84f8c123de9460464602a9<br>account:欲查詢之帳號(預設為email)<br>plandatetime:欲查詢日期",
                data: {
                    "key": "e7bfaeb34e84f8c123de9460464602a9",
                    "account": "aaaa@gmail.com",
                    "plandatetime": "2017-03-14"
                }
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_GETFOODTOTALKCAL",
            test:[{
                buttonTitle: "取得用戶飲食計劃熱量值",
                desc: "key:固定服務驗證碼5764d940ca640a9d44905f5301ffc24f<br>account:欲查詢之帳號(預設為email)<br>type:只有week可以選擇<br>month:指定月份<br>year:指定年份<br>stdate:開始日期<br>eddate:結束日期",
                data: {
                    "key": "5764d940ca640a9d44905f5301ffc24f",
                    "account": "aaaa@gmail.com",
                    "type": "week",
                    "year": 2017,
                    "month": 5,
                    "date": "2017-05-25",
                    "stdate": "2017-05-25",
                    "eddate": "2017-05-31"
                }
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_GETFOODTOTALKCAL",
            test:[{
                buttonTitle: "取得用戶飲食計劃熱量值",
                desc: "key:固定服務驗證碼5764d940ca640a9d44905f5301ffc24f<br>account:欲查詢之帳號(預設為email)<br>type:只有week可以選擇<br>month:指定月份<br>year:指定年份<br>stdate:開始日期<br>eddate:結束日期",
                data: {
                    "key": "5764d940ca640a9d44905f5301ffc24f",
                    "account": "aaaa@gmail.com",
                    "type": "week",
                    "year": 2017,
                    "month": 5,
                    "date": "2017-05-25",
                    "stdate": "2017-05-25",
                    "eddate": "2017-05-31"
                }
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_GETFOODTOTALKCAL",
            test:[{
                buttonTitle: "取得用戶飲食計劃熱量值",
                desc: "key:固定服務驗證碼5764d940ca640a9d44905f5301ffc24f<br>account:欲查詢之帳號(預設為email)<br>type:只有week可以選擇<br>month:指定月份<br>year:指定年份<br>stdate:開始日期<br>eddate:結束日期",
                data: {
                    "key": "5764d940ca640a9d44905f5301ffc24f",
                    "account": "aaaa@gmail.com",
                    "type": "week",
                    "year": 2017,
                    "month": 5,
                    "date": "2017-05-25",
                    "stdate": "2017-05-25",
                    "eddate": "2017-05-31"
                }
            }]
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_DELFOODPLAN",
            test:[{
                buttonTitle: "用戶刪除飲食計畫",
                desc: "key:固定服務驗證碼4d68e7fa9845a8959bed0af23ab05414<br>account:欲查詢之帳號(預設為email)<br>plandatetime:日期",
                data: {
                    "key": "4d68e7fa9845a8959bed0af23ab05414",
                    "account": "aaaa@gmail.com",
                    "plandatetime": "2017-05-31"
                }
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_CLRFOODAVGKCAL",
            test:[{
                buttonTitle: "用戶清除飲食計畫卡路里平均值",
                desc: "key:固定服務驗證碼48a7c11ed1ccb8766e92d879c106a741<br>account:欲查詢之帳號(預設為email)<br>plandatetime:日期",
                data: {
                    "key": "48a7c11ed1ccb8766e92d879c106a741",
                    "account": " aaaa@gmail.com",
                    "plandatetime": "2017-06-01"
                }
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_RESETACCOUNT",
            test:[{
                buttonTitle: "用戶重設密碼",
                desc: "key:固定服務驗證碼9e697e63b257e4a630dd4d4409c05a45<br>account:欲查詢之帳號(預設為email)<br>plandatetime:日期",
                data: {
                    "key": "9e697e63b257e4a630dd4d4409c05a45",
                    "account": " aaaa@gmail.com"
                }
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_VERIFICATIONRSTACCOUNT",
            test:[{
                buttonTitle: "用戶忘記帳號token驗證",
                desc: "a:帳號<br>v:驗證參數<br>t:驗證參數<br>key:固定服務驗證碼2d6ae242b526bbd0245b1bb8be98ea93",
                data: {
                    "a": "aaaa@gmail.com",
                    "t": "1496297114298",
                    "v": "87910bc8051235463e57ad306a5deeb2",
                    "key": "2d6ae242b526bbd0245b1bb8be98ea93"
                }
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_NEWPWDRECOVERY",
            test:[{
                buttonTitle: "用戶送出新的密碼",
                desc: "a:帳號<br>v:驗證參數<br>t:驗證參數<br>key:固定服務驗證碼1822e527f87be633ba8822e0709007be<br>passkey:驗證驗證信連結取得之驗證key<br>password:MD5新密碼",
                data: {
                    "a": "aaaa@gmail.com",
                    "passkey": "65d956005af18c6a5159f7f67341ef4a",
                    "password": "52467e98ed69ad0e5c3cf1f27212e8ad",
                    "v": "87910bc8051235463e57ad306a5deeb2",
                    "key": "1822e527f87be633ba8822e0709007be"
                }
            }] 
        }],
        wisme2Websetting:[{
            type: "POST",
            url: "http://103.211.167.83/WIS_GETPAGECONFIGFROMDB",
            test:[{
                buttonTitle: "取得網站頁面設定檔",
                desc: "configname:設定檔名稱<br>index_tw 繁中頁首<br>index_cn 簡中頁首<br>index_en 英文頁首",
                data: {
                    "configname": "index_tw"
                }
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_GETPAGECONFIGFROMDB",
            test:[{
                buttonTitle: "取得網站頁尾設定檔",
                desc: "configname:設定檔名稱<br>homefooter_tw 繁中頁首<br>homefooter_cn 簡中頁首<br>homefooter_en 英文頁首",
                data: {
                    "configname": "homefooter_tw"
                }
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_GETPAGECONFIGFROMDB",
            test:[{
                buttonTitle: "取得網站內容設定檔",
                desc: "configname:設定檔名稱<br>homecontent_tw 繁中頁首<br>homecontent_cn 簡中頁首<br>homecontent_en 英文頁首",
                data: {
                    "configname": "homecontent_tw"
                }
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_GETPAGECONFIGFROMDB",
            test:[{
                buttonTitle: "取得使用條款頁面設定檔",
                desc: "configname:設定檔名稱<br>termsofuse_tw 繁中頁首<br>termsofuse_cn 簡中頁首<br>termsofuse_en 英文頁首",
                data: {
                    "configname": "termsofuse_tw"
                }
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_GETPAGECONFIGFROMDB",
            test:[{
                buttonTitle: "取得隱私權頁面設定檔",
                desc: "configname:設定檔名稱<br>privacypolicy_tw 繁中頁首<br>privacypolicy_cn 簡中頁首<br>privacypolicy_en 英文頁首",
                data: {
                    "configname": "privacypolicy_tw"
                }
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_GETPAGECONFIGFROMDB",
            test:[{
                buttonTitle: "取得關於我們頁面設定檔",
                desc: "configname:設定檔名稱<br>aboutus_tw 繁中頁首<br>aboutus_cn 簡中頁首<br>aboutus_en 英文頁首",
                data: {
                    "configname": "aboutus_tw"
                }
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_GETPAGECONFIGFROMDB",
            test:[{
                buttonTitle: "取得商業合作頁面設定檔",
                desc: "configname:設定檔名稱<br>cooperation_tw 繁中頁首<br>cooperation_cn 簡中頁首<br>cooperation_en 英文頁首",
                data: {
                    "configname": "cooperation_tw"
                }
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_INSERTPAGECONFIGTODB",
            test:[{
                buttonTitle: "取得商業合作頁面設定檔",
                desc: "",
                data: {
                    "configname": "index_tw",
                    "configdata": {
                        "bgcoler": "#c0c0c0",
                        "bgimage": "WisdatHttp/images/about_us-AD2-A-20161107.jpg",
                        "bgtype": "0",
                        "fontcolor": "#ffffff",
                        "fontsize": "14",
                        "fonttype": "Microsoft JhengHei",
                        "logoimage": "WisdatHttp/images/wd-logo-tw.png",
                        "logolink": "index.html",
                        "logoshowhide": true,
                        "logourltype": 0,
                        "mainnav": ["product", "member", "usermanual", "about", "login"],
                        "subnavmenu": {
                            "fontcolor": "#ffffff",
                            "fontsize": "14",
                            "fonttype": "Microsoft JhengHei"
                        }
                    }
                }
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_INSERTPAGECONFIGTODB",
            test:[{
                buttonTitle: "設定頁面頁尾設定檔",
                desc: "",
                data: {
                    "configname": "homefooter_tw",
                    "configdata": {
                        "langlogoimg": "WisdatHttp/images/wisdat_earth.png",
                        "social1logoimg": "WisdatHttp/images/wisdat_FB.png",
                        "social1logolink": "",
                        "social2logoimg": "WisdatHttp/images/wisdat_wechat.png",
                        "social2logolink": "",
                        "social3logoimg": "WisdatHttp/images/wisdat_Instagram.png",
                        "social3logolink": "",
                        "social4logoimg": "WisdatHttp/images/wisdat_youtube.png",
                        "social4logolink": "",
                        "footernav": ["lang", "social1", "social2", "social3", "social4"],
                        "cropshowhide": true,
                        "cropcontent": "© 2017 WisDat, Inc.保留所有權利。",
                        "cropfonttype": "serif",
                        "cropfontsize": "14",
                        "cropfontcolor": "#ffffff",
                        "croplink": "index.html",
                        "ruleshowhide": true,
                        "rulecontent": "使用條款",
                        "rulefonttype": "sans-serif",
                        "rulefontsize": "14",
                        "rulefontcolor": "#ffffff",
                        "rulelink": "termsofuse.html",
                        "privaceshowhide": true,
                        "privacecontent": "隱私權政策",
                        "privacefonttype": "PMingLiU",
                        "privacefontsize": "14",
                        "privacefontcolor": "#ffffff",
                        "privacelink": "privacypolicy.html",
                        "webbgtype": "1",
                        "webbgcolor": "#07539e",
                        "webbgimage": "WisdatHttp/images/BG2.png"
                    }
                }
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_INSERTPAGECONFIGTODB",
            test:[{
                buttonTitle: "設定首頁頁面內容設定檔",
                desc: "",
                data: {"configname":"homecontent_tw","configdata":{"leftlink":{"image":"WisdatHttp/images/news-tw.png","url":"news.html"},"centerlink":{"image":"WisdatHttp/images/support-tw.png","url":"support.html"},"rightlink":{"image":"WisdatHttp/images/business-tw.png","url":"cooperation.html"},"carouselset":[{"carouselsort":0,"carouselimage":"WisdatHttp/images/carousel/AD-wisme-A-2017-0206.jpg","carousellink":""},{"carouselsort":1,"carouselimage":"WisdatHttp/images/carousel/AD-wisme-B-2017-0123.jpg","carousellink":""}]}}
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_INSERTPAGECONFIGTODB",
            test:[{
                buttonTitle: "設定使用條款頁面內容設定檔",
                desc: "",
                data:{
                    "configname": "termsofuse_tw",
                    "configdata": {
                        "contenttermsofusepolicy": "%3Cblockquote%3E%0A..."
                    }
                }
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_INSERTPAGECONFIGTODB",
            test:[{
                buttonTitle: "設定隱私權頁面內容設定檔",
                desc: "",
                data:{
                    "configname": "privacypolicy_tw",
                    "configdata": {
                        "contentprivacypolicy": "%3Cblockquote%3E%0A…"
                    }
                }
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_INSERTPAGECONFIGTODB",
            test:[{
                buttonTitle: "設定關於我們頁面內容設定檔",
                desc: "",
                data:{
                    "configname": "aboutus_tw",
                    "configdata": {
                        "aboutusadimage": "WisdatHttp/images/About-us-AD-2017-0120-tiny.jpg",
                        "contentaboutwd": "<!doctype html>\n<html>\n<head>\n<meta charset:\"utf-8\">\n<title>...",
                        "contentwdteam": " <!doctype html>\n<html>\n...",
                        "contentwdcontact": " <!doctype html>\n<html>\n...",
                        "footeradimage ": "WisdatHttp/images/about_us-AD2-A-20161107-tiny.jpg "
                    }
                }
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_INSERTPAGECONFIGTODB",
            test:[{
                buttonTitle: "設定商業合作頁面內容設定檔",
                desc: "",
                data:{
                    "configname": "cooperation_tw",
                    "configdata": {
                        "cooperationadimage": "WisdatHttp/images/Business_Cooperation-AD1-B-tiny.jpg",
                        "contentpartners": "",
                        "contentprovidebusiness": "",
                        "contentcontact": "",
                        "contentcustom": "%3Cp%3E%3Cbr%20/%3E…",
                        "footeradimage": "WisdatHttp/images/Business_Cooperation-AD2-A-tiny.jpg"
                    }
                }
            }] 
        }],
        wisme2Admin:[{
            type: "POST",
            url: "http://103.211.167.83/WIS_BACKEND_LOGIN",
            test:[{
                buttonTitle: "形象網站管理後台帳號登入",
                desc: "account:帳號<br>ccap:圖形驗證碼<br>key:服務驗證碼4131cd9caef9be8c92124c8b8816e358<br>password:MD5密碼",
                data:{
                    "account": "wisdat",
                    "password": "40580b053457c60e2dfa5d6de9d23a65",
                    "ccap": "739618",
                    "key": "4131cd9caef9be8c92124c8b8816e358"
                }
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_BACKEND_LOGIN",
            test:[{
                buttonTitle: "形象網站管理後台帳號登出",
                desc: "",
                data:{}
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_BACKEND_CHECK_STATUS",
            test:[{
                buttonTitle: "管理後台管理帳號登入狀態驗證",
                desc: "自動抓取cookie和session token進行驗證",
                data:{}
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_UPDATE_BACKEND_MANAGER",
            test:[{
                buttonTitle: "形象網站管理後台帳號登入",
                desc: "account:帳號<br>password:新密碼（若不修改密碼請留空白）<br>key:服務驗證碼b8398fd8001d4046831ad5be01286f37<br>name:帳號顯示名稱",
                data:{
                    "account": "root",
                    "password": "81dc9bdb52d04dc20036dbd8313ed055",
                    "name": "root",
                    "key": "b8398fd8001d4046831ad5be01286f37"
                }
            }] 
        },{
            type: "GET",
            url: "http://103.211.167.83/WIS_GETSYSINFO",
            test:[{
                buttonTitle: "取得伺服器系統資訊",
                desc: "",
                data:{}
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_GETMEMBER_STATUS",
            test:[{
                buttonTitle: "取得會員狀態資訊",
                desc: "",
                data:{}
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_GETMEMBER",
            test:[{
                buttonTitle: "取得會員資料清單",
                desc: "limit:每頁限制比數<br>page:頁碼<br>acc:帳號條件<br>name:用戶名條件<br>email:用戶email條件",
                data:{
                    "limit": "15",
                    "page": 1,
                    "acc": "",
                    "name": "",
                    "mail": ""
                }
            }] 
        },{
            type: "GET",
            url: "http://103.211.167.83/WIS_EXPORT_ALL_RAWDATA_COMBINED_MEMBERDATA",
            test:[{
                buttonTitle: "匯出全部手環原始資料與會員資料內容",
                desc: "",
                data:{}
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_EXPORT_RAWDATA_COMBINED_MEMBERDATA_BY_MEMBERACCOUNT",
            test:[{
                buttonTitle: "匯出指定帳號手環原始資料與會員資料內容",
                desc: "acc:帳號<br>startday:開始日期<br>結束日期",
                data:{
                    "acc": "aaaa@gmail.com",
                    "startday": "2017-06-01",
                    "endday": "2017-06-01"
                }
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_INSERTCATEGORYDATATODB",
            test:[{
                buttonTitle: "新增產品類別資料到資料表",
                desc: "language:語言<br>categoryname:產品類別名稱<br>categorytype:產品類別上下線",
                data:{
                    "language": "tw",
                    "categoryname": "分類1",
                    "categorytype": true
                }                
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_GETCATEGORYDATABYLANGUAGEFROMDB",
            test:[{
                buttonTitle: "透過語系取得產品類別資料",
                desc: "language:語言",
                data:{"language":"tw"}               
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_INSERTCATEGORYDATATODB",
            test:[{
                buttonTitle: "更新產品類別資料到資料表",
                desc: "categoryid:產品類別ID<br>categoryname:產品類別名稱<br>categorytype:產品類別上下線",
                data:{
                    "categoryid": "592fe58d67a1f3ed0ddad621",
                    "categoryname": "分類1",
                    "categorytype": true
                }                
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_INSERTCATEGORYDATATODB",
            test:[{
                buttonTitle: "新增產品類別資料到資料表",
                desc: "language:語言<br>productitemname:產品項目名稱<br>productitemtype:產品項目上下線<br>categoryid:父產品類別ID",
                data:{
                    "language": "tw",
                    "categoryid": "592fe58d67a1f3ed0ddad621",
                    "productitemname": "項目1",
                    "productitemtype": true
                }                
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_GETPRODUCTITEMDATABYLANGUAGEFROMDB",
            test:[{
                buttonTitle: "透過語系取得產品類別資料",
                desc: "language:語言",
                data:{"language":"tw"}               
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_UPDATEPRODUCTITEMDATATODB",
            test:[{
                buttonTitle: "更新產品項目資料到資料表",
                desc: "categoryid:父產品類別ID<br>productitemid:產品項目ID<br>productitemname:產品項目名稱<br>productitemtype:產品項目上下線<br>language:語言別",
                data: {
                    "categoryid": "57c4e5296b388c5f47a9dfd5",
                    "productitemid": "58993b11d914a2722adc47e9",
                    "productitemname": "‧分類項目666",
                    "productitemtype": true,
                    "language": "tw"
                }                
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_INSERTPRODUCTDATATODB",
            test:[{
                buttonTitle: "新增產品資料到資料表",
                desc: "categoryid:父產品類別ID<br>productitemid:產品項目ID<br>productitemname:產品項目名稱<br>productitemtype:產品項目上下線<br>language:語言別",
                data: {
                    "productitemid": "592ffc8b67a1f3ed0ddad622",
                    "language": "tw",
                    "productname": "新增產品3",
                    "productshowstyle": 0,
                    "producttype": true,
                    "productconfig": {
                        "producttopfonttype": "sans-serif",
                        "producttopfontsize": "18",
                        "producttopfontcolor": "#ffffff",
                        "producttopbgcolor": "#808080",
                        "productimage1": "",
                        "productimage2": "",
                        "productimage3": "",
                        "productimage4": "",
                        "productimage5": "",
                        "productcolortype": true,
                        "productstylename": "",
                        "productstylefonttype": "serif",
                        "productstylefontsize": "",
                        "productstylefontcolor": "#000000",
                        "productstyleset": [{
                            "productstyleimage": "WisdatProductData/images/giphy.gif",
                            "productstylereplaceimage": "WisdatProductData/images/giphy.gif"
                        }],
                        "productcontent": "",
                        "productusermanualtype": true,
                        "productusermanual": "WisdatProductData/ebook/0215-um-wisme-I06-tw"
                    }
                }                
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_INSERTPRODUCTDATATODB",
            test:[{
                buttonTitle: "新增一頁式產品資料樣式到資料表",
                desc: "categoryid:父產品類別ID<br>productitemid:產品項目ID<br>productitemname:產品項目名稱<br>productitemtype:產品項目上下線<br>language:語言別",
                data: {
                    "productitemid": "592ffc8b67a1f3ed0ddad622",
                    "language": "tw",
                    "productname": "新增產品1",
                    "productshowstyle": 1,
                    "producttype": true,
                    "productconfig": {
                        "producttopfonttype": "sans-serif",
                        "producttopfontsize": "18",
                        "producttopfontcolor": "#ffffff",
                        "producttopbgcolor": "#808080",
                        "productlistimage": "WisdatProductData/images/Wisme-I06-500x500-tiny.jpg",
                        "productdmset": [{
                            "dmsort": 0,
                            "dmimage": "WisdatProductData/images/Wisme-banner-I06-01-tiny.jpg",
                            "dmlink": ""
                        }],
                        "productusermanualtype": true,
                        "productusermanual": "WisdatProductData/ebook/0215-um-wisme-I06-tw"
                    }
                }                
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_GETPRODUCTITEMDATABYLANGUAGEFROMDB",
            test:[{
                buttonTitle: "透過產品名稱取得產品資料",
                desc: "language:語言<br>productname:產品名稱",
                data:{
                    "language":"tw",
                    "productname":"新增產品3"
                }
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_GETPRODUCTDATABYLANGUAGEFROMDB",
            test:[{
                buttonTitle: "透過語系取得產品資料",
                desc: "language:語言",
                data:{
                    "language":"tw",
                }
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_UPDATEPRODUCTDATATODB",
            test:[{
                buttonTitle: "更新產品資料樣式",
                desc: "language:語言",
                data:[{
                    "productid": "5931045267a1f3ed0ddad624",
                    "productitemid": "592ffc8b67a1f3ed0ddad622",
                    "language": "tw",
                    "productname": "新增產品1",
                    "productshowstyle": 1,
                    "producttype": true,
                    "productconfig": {
                        "producttopfonttype": "sans-serif",
                        "producttopfontsize": "18",
                        "producttopfontcolor": "#ffffff",
                        "producttopbgcolor": "#808080",
                        "productlistimage": "WisdatProductData/images/Wisme-I06-500x500-tiny.jpg",
                        "productdmset": [{
                            "dmsort": 0,
                            "dmimage": "WisdatProductData/images/Wisme-banner-I06-01-tiny.jpg",
                            "dmlink": ""
                        }, {
                            "dmsort": 1,
                            "dmimage": "WisdatProductData/images/Wisme-banner-02-tiny.jpg",
                            "dmlink": ""
                        }, {
                            "dmsort": 2,
                            "dmimage": "WisdatProductData/images/Wisme-banner-03-tiny.jpg",
                            "dmlink": ""
                        }, {
                            "dmsort": 3,
                            "dmimage": "WisdatProductData/images/Wisme-banner-04-tiny.jpg",
                            "dmlink": ""
                        }, {
                            "dmsort": 4,
                            "dmimage": "WisdatProductData/images/I06-specs_and_details-2017-0206.jpg",
                            "dmlink": ""
                        }],
                        "productusermanualtype": true,
                        "productusermanual": "WisdatProductData/ebook/0215-um-wisme-I06-tw"
                    }
                }, {
                    "productid": "59310d3b67a1f3ed0ddad625",
                    "productitemid": "592ffc8b67a1f3ed0ddad622",
                    "language": "tw",
                    "productname": "新增產品3",
                    "productshowstyle": 0,
                    "producttype": true,
                    "productconfig": {
                        "producttopfonttype": "sans-serif",
                        "producttopfontsize": "18",
                        "producttopfontcolor": "#ffffff",
                        "producttopbgcolor": "#808080",
                        "productimage1": "",
                        "productimage2": "",
                        "productimage3": "",
                        "productimage4": "",
                        "productimage5": "",
                        "productcolortype": true,
                        "productstylename": "",
                        "productstylefonttype": "serif",
                        "productstylefontsize": "",
                        "productstylefontcolor": "#000000",
                        "productstyleset": [{
                            "productstyleimage": "WisdatProductData/images/giphy.gif",
                            "productstylereplaceimage": "WisdatProductData/images/giphy.gif"
                        }],
                        "productcontent": "",
                        "productusermanualtype": true,
                        "productusermanual": "WisdatProductData/ebook/0215-um-wisme-I06-tw"
                    }
                }]
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_UPDATEPRODUCTUSERMANUALDATABYPRODUCTIDTODB",
            test:[{
                buttonTitle: "根據產品ID取得產品說明書資料",
                desc: "productid:產品ID",
                data:{"productid":"57dfacdd0be16f173ad520ce"} 
            }] 
        },{
            type: "POST",
            url: "http://103.211.167.83/WIS_UPDATEPRODUCTUSERMANUALDATABYPRODUCTIDTODB",
            test:[{
                buttonTitle: "根據產品ID取得產品說明書資料",
                desc: "productid:產品ID",
                data:[{
                    "productid": "57c7e5f90e3b785d0ae51f45",
                    "productusermanual": "WisdatProductData/ebook/UM-Wisbit-08A-tw-20170213",
                    "productusermanualtype": false
                }, {
                    "productid": "5875bef29593414795a155fc",
                    "productusermanual": "WisdatProductData/ebook/wisfit-03B-tw",
                    "productusermanualtype": true
                }]  
            }] 
        }],
        wisme2ring:[{
            type: 'POST',
            url: "http://103.211.167.83/WISME_RINGDATA",
            test: [{
                buttonTitle : "傳送手環資訊",
                desc: "以下內容須將JSON資料壓縮成GZIP格式",
                data: {
                    "key": "b25e41a5bb36313724f69465695c856a",
                    "account": "aaaa@gmail.com",
                    "pwd": "098f6bcd4621d373cade4e832627b4f6",
                    "device_id": "A1A2df5ds6f8ds8d4d45s6d4f5dd21dS21fd25s",
                    "mac": "00:00:00:00:00",
                    "RAW_DATA": [{
                        "account": "aaaa@gmail.com",
                        "dateTime": "2016-01-01 00:00:00",
                        "date": "2016-01-01",
                        "time": "00:00:00",
                        "hour": 0,
                        "MAC": "00:00:00:00:00",
                        "dataType": "",
                        "stepsPM": 10,
                        "distancePM": 10,
                        "calariePM": 10,
                        "heartRateAvePM": 1,
                        "heartRateMaxPM": 1,
                        "heartRateMinPM": 1,
                        "activityPM": 1,
                        "weigthPM": 1,
                        "heightPM": 1,
                        "G1": 1,
                        "G2": 1,
                        "G3": 1,
                        "G4": 1,
                        "G5": 1,
                        "G6": 1,
                        "G7": 1,
                        "G8": 1,
                        "G9": 1,
                        "G10": 1
                    }, {
                        "account": "aaaa@gmail.com",
                        "dateTime": "2016-01-01 00:00:01",
                        "date": "2016-01-01",
                        "time": "00:00:00",
                        "hour": 0,
                        "MAC": "00:00:00:00:00",
                        "dataType": "",
                        "stepsPM": 20,
                        "distancePM": 20,
                        "calariePM": 20,
                        "heartRateAvePM": 2,
                        "heartRateMaxPM": 2,
                        "heartRateMinPM": 2,
                        "activityPM": 2,
                        "weigthPM": 2,
                        "heightPM": 2,
                        "G1": 2,
                        "G2": 2,
                        "G3": 2,
                        "G4": 2,
                        "G5": 2,
                        "G6": 2,
                        "G7": 2,
                        "G8": 2,
                        "G9": 2,
                        "G10": 2
                    }],
                    "STEPS_DATA": [{
                        "account": "aaaa@gmail.com",
                        "dateTime": "2016-01-01 00:00:00",
                        "date": "2016-01-01",
                        "time": "22:22:21",
                        "year": 2016,
                        "month": 1,
                        "MAC": "00:00:00:00:00",
                        "goalStep": 500,
                        "stepSum": 300,
                        "hour1": 0,
                        "hour2": 0,
                        "hour3": 0,
                        "hour4": 10,
                        "hour5": 10,
                        "hour6": 10,
                        "hour7": 10,
                        "hour8": 10,
                        "hour9": 20,
                        "hour10": 20,
                        "hour11": 20,
                        "hour12": 30,
                        "hour13": 30,
                        "hour14": 30,
                        "hour15": 30,
                        "hour16": 40,
                        "hour17": 0,
                        "hour18": 0,
                        "hour19": 0,
                        "hour20": 0,
                        "hour21": 0,
                        "hour22": 0,
                        "hour23": 0,
                        "hour24": 0,
                        "completeness": 50
                    }],
                    "DISTANCE_DATA": [{
                        "account": "aaaa@gmail.com",
                        "dateTime": "2016-01-01 00:00:00",
                        "date": "2016-01-01",
                        "time": "22:22:21",
                        "year": 2016,
                        "month": 1,
                        "MAC": "00:00:00:00:00",
                        "goalDistance": 500,
                        "distanceSum": 300,
                        "hour1": 0,
                        "hour2": 0,
                        "hour3": 0,
                        "hour4": 10,
                        "hour5": 10,
                        "hour6": 10,
                        "hour7": 10,
                        "hour8": 10,
                        "hour9": 20,
                        "hour10": 20,
                        "hour11": 20,
                        "hour12": 30,
                        "hour13": 30,
                        "hour14": 30,
                        "hour15": 30,
                        "hour16": 40,
                        "hour17": 0,
                        "hour18": 0,
                        "hour19": 0,
                        "hour20": 0,
                        "hour21": 0,
                        "hour22": 0,
                        "hour23": 0,
                        "hour24": 0,
                        "completeness": 50
                    }],
                    "CALARIE_DATA": [{
                        "account": "aaaa@gmail.com",
                        "dateTime": "2016-01-01 00:00:00",
                        "date": "2016-01-01",
                        "time": "22:22:21",
                        "year": 2016,
                        "month": 1,
                        "MAC": "00:00:00:00:00",
                        "goalCal": 500,
                        "calSum": 300,
                        "hour1": 0,
                        "hour2": 0,
                        "hour3": 0,
                        "hour4": 10,
                        "hour5": 10,
                        "hour6": 10,
                        "hour7": 10,
                        "hour8": 10,
                        "hour9": 20,
                        "hour10": 20,
                        "hour11": 20,
                        "hour12": 30,
                        "hour13": 30,
                        "hour14": 30,
                        "hour15": 30,
                        "hour16": 40,
                        "hour17": 0,
                        "hour18": 0,
                        "hour19": 0,
                        "hour20": 0,
                        "hour21": 0,
                        "hour22": 0,
                        "hour23": 0,
                        "hour24": 0,
                        "completeness": 50
                    }],
                    "SLEEP_DATA": [{
                        "account": "aaaa@gmail.com",
                        "dateTime": "2016-01-01 00:00:00",
                        "date": "2016-01-01",
                        "time": "10:30:59",
                        "year": 2016,
                        "month": 5,
                        "MAC": "00:00:00:00:00",
                        "sleepSum": 5000,
                        "lightsleepSum": 2000,
                        "deepsleepSum": 3000
                    }],
                    "HEARTRATE_DATA": [{
                        "account": "aaaa@gmail.com",
                        "dateTime": "2016-01-01 00:00:00",
                        "date": "2016-01-01",
                        "time": "10:30:59",
                        "MAC": "00:00:00:00:00",
                        "heartRateAvePM": 90,
                        "heartRateMaxPM": 100,
                        "heartRateMinPM": 70
                    }],
                    "ACTIVITY_DATA": [{
                        "account": "aaaa@gmail.com",
                        "dateTime": "2016-01-01 00:00:00",
                        "date": "2016-01-01",
                        "time": "10:30:59",
                        "year": 2016,
                        "month": 6,
                        "MAC": "00:00:00:00:00",
                        "lowActivityMin": 5000,
                        "generalActivityMin": 3000,
                        "bigActivityMin": 3000,
                        "highActivityMin": 3000,
                        "maxActivityMin": 3000,
                        "restActivityMin": 3000,
                        "goalActivity": 20
                    }],
                    "WEIGHT_DATA": [{
                        "account": "aaaa@gmail.com",
                        "dateTime": "2016-01-01 00:00:00",
                        "date": "2016-01-01",
                        "time": "10:30:59",
                        "year": 2016,
                        "month": 6,
                        "weight": 80
                    }]
                }
            }]
        },{
            type: 'POST',
            url: "http://103.211.167.83/WISME_USERRINGDATA",
            test: [{
                buttonTitle : "傳送使用者資料",
                desc: "",
                data: {
                    "key": "ccecb53b5e8cb2eda139c2a4257cbc86",
                    "account": "aaaa@gmail.com",
                    "pwd": "098f6bcd4621d373cade4e832627b4f6",
                    "USER_MESSAGE": {
                        "account": "aaaa@gmail.com",
                        "password": "098f6bcd4621d373cade4e832627b4f6",
                        "userName": "peter",
                        "sex": "男",
                        "birthday": "2016-01-02",
                        "portrait": "iVBORw0KGgoAAAANSUhEUgAAAEQAAABECAIAAAC3cQTlAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAACLSURBVGhD3ccxDQAwDMCw8odbAutvBpnkx/P2Hz7Np/k0n+bTfJpP82k+zaf5NJ/m03yaT/NpPs2n+TSf5tN8mk/zaT7Np/k0n+bTfJpP82k+zaf5NJ/m03yaT/NpPs2n+TSf5tN8mk/zaT7Np/k0n+bTfJpP82k+zaf5NJ/m03yaT/NpPs2n+bCdA7d8Ex7dcvk5AAAAAElFTkSuQmCC",
                        "height": 167,
                        "weight": 55,
                        "goalStep": 3000,
                        "goalDistance": 3000,
                        "goalCalarie": 3000,
                        "goalActivity": 20
                    },
                    "USER_SETTINGS_MESSAGE": {
                        "account": "aaaa@gmail.com",
                        "wearPosition": "a",
                        "heartRateTime1": "2016-01-01 00:00:00",
                        "heartRateSwitch1": 0,
                        "heartRateTime2": "2016-01-01 00:00:00",
                        "heartRateSwitch2": 0,
                        "heartRateTime3": "2016-01-01 00:00:00",
                        "heartRateSwitch3": 0,
                        "heartRateTime4": "2016-01-01 00:00:00",
                        "heartRateSwitch4": 0,
                        "heartRateTime5": "2016-01-01 00:00:00",
                        "heartRateSwitch5": 0,
                        "sdStartTime": "2016-01-01 00:00:00",
                        "sdEndTime": "2016-01-01 00:00:00",
                        "sdIntervalTime": "2016-01-01 00:00:00",
                        "sdSwitch": 0,
                        "phoneSwitch": 0,
                        "noteSwitch": 0,
                        "messageSwitch": 0,
                        "bitcomSwitch": 0,
                        "alarmclockTime1": "2016-01-01 00:00:00",
                        "alarmclockSwitch1": 0,
                        "alarmclockTime2": "2016-01-01 00:00:00",
                        "alarmclockSwitch2": 0,
                        "alarmclockTime3": "2016-01-01 00:00:00",
                        "alarmclockSwitch3": 0,
                        "alarmclockTime4": "2016-01-01 00:00:00",
                        "alarmclockSwitch4": 0,
                        "alarmclockTime5": "2016-01-01 00:00:00",
                        "alarmclockSwitch5": 0,
                        "switch3G": 0,
                        "brigthnessValue": 0,
                        "unit": 0,
                        "electricQuantityValue": 3000,
                        "mapType": "google"
                    },
                    "DEVICES_MESSAGE": {
                        "account": "aaaa@gmail.com",
                        "ANDROID_MAC": "",
                        "bindingCode": "3000",
                        "solutionState": 1,
                        "IOS_UUID": "",
                        "dateTime": "2016-01-01 00:00:00",
                        "heartRateType": 0
                    }
                }
            }]
        },{
            type: 'POST',
            url: "http://103.211.167.83/WISME_GETRINGTIME",
            test: [{
                buttonTitle : "取得手環最後上傳時間",
                desc: "",
                data: {
                    "key": "60ea62840f54763fa29ae93ae7c70671",
                    "acc": "aaaa@gmail.com",
                    "pwd": "098f6bcd4621d373cade4e832627b4f6",
                    "mac": "00:00:00:00:00",
                }
            }]
        },{
            type: 'POST',
            url: "http://103.211.167.83/WISME_RINGDOWNLOAD",
            test: [{
                buttonTitle : "取得手環下載資料",
                desc: "回傳格式為gzip",
                data: {
                    "key": "15df176a072c8e349f135ea567736834",
                    "acc": "aaaa@gmail.com",
                    "pwd": "098f6bcd4621d373cade4e832627b4f6",
                    "start_datetime":"2016-01-01 00:00",
                    "end_datetime":"2016-12-01 00:00",
                    "mac":"00:00:00:00:00"
                }
            }]
        },{
            type: 'POST',
            url: "http://103.211.167.83/WISME_USERRINGDOWNLOAD",
            test: [{
                buttonTitle : "取得使用者帳號資料",
                desc: "",
                data: {
                    "key": "c5ad4b06e0ebd3a68d92a78b3cf1a8f0",
                    "acc": "aaaa@gmail.com",
                    "pwd": "098f6bcd4621d373cade4e832627b4f6",
                }
            }]
        },{
            type: 'POST',
            url: "http://103.211.167.83/WISME_RINGDELETE",
            test: [{
                buttonTitle : "刪除手環RawData資料",
                desc: "",
                data:{
                    "key": "fb96b3f05bc6a9ac5c02c09d0f46192a",
                    "acc": "aaaa@gmail.com",
                    "pwd": "098f6bcd4621d373cade4e832627b4f6",
                    "mac":"00:00:00:00:00",
                    "datetime":"2016-01-01 00:00"
                }
            }]
        },{
            type: 'POST',
            url: "http://103.211.167.83/WIS_REGISTER",
            test: [{
                buttonTitle : "註冊使用者帳號",
                desc: "",
                data:{
                    "key": "d4ddaa1babd2730cac2f23387ddfefea",
                    "acc": "aaaa@gmail.com",
                    "pwd": "098f6bcd4621d373cade4e832627b4f6"
                }
            }]
        },{
            type: 'POST',
            url: "http://103.211.167.83/WISME_LOGIN",
            test: [{
                buttonTitle : "App帳號驗證",
                desc: "",
                data:{
                    "key": "c9645bd2a18af679655dab465b8e1c8c",
                    "acc": "aaaa@gmail.com",
                    "pwd": "098f6bcd4621d373cade4e832627b4f6"
                }
            }]
        }],
        wisme2EmailAPI : [{
            type: "POST",
            url: "http://103.211.167.83/WIS_EMAILAPI",
            test:[{
                buttonTitle: "發送客製信件",
                desc: "",
                data: {
	                "receiver": "aaaa@gmail.com",
	                "subject": "test",
	                "content": "test",
	                "key": "2183e29c2b2b916f7d861b9249475313"
                }
            }]
        }]	 
    },

    /**
     * 取得API測試資料
     * @parmas 
     */
    testReady : function(API, APIseq, testSeq) {
        var API = this.conf[API][APIseq];
        console.log("載入測試資料: "+ API['test'][testSeq]['desc']);
        
        var data = {
            desc: API['test'][testSeq]['desc'],
            type: API['type'],
            url: API['url'],
            data: JSON.stringify(API['test'][testSeq]['data'])
        }
        
        return data;
    },
    
    /**
     * API測試發送資料(從html索取)
     * @parmas 
     */
    testAPI : function(type, url, data) {
        var API = this.conf[API][APIseq];
        console.log("測試: "+ API['test'][testSeq]['desc']);
        
        $.ajax({
            type: type,
            contentType: "application/json; charset=UTF-8;",
            url: url,
            data: data,
            success: function(msg){
                console.log(msg);
            },
            error:function(xhr, ajaxOptions, thrownError){ 
                console.log(xhr.status); 
                console.log(thrownError); 
            }
        });
    }
};
})(jQuery);
