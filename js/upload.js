(function($) {
            $.upload = $.upload || {version:'0.0.1'};
            var upload = function(dom,opts) { //[--plugin define
                        var me=$(dom);
                        // public methods
                        $.extend(this, {
                                    init: function() {
                                                init();
                                    },
                                    paste: function( src ) {
                                                paste( src );
                                    },
                                    handleFileUpload: function() {
                                                handleFileUpload();
                                    },
                                    options: function() {
                                                return opts;
                                    },
                                    _SetOpts: function( options ) {
                                                $.extend(opts,options);
                                    }
                        });

                        function init()
                        {
                            
                                    opts.media_obj  = $("#upload_bottom");
                                    opts.media_obj
                                    .unbind('dragenter').bind( 'dragenter' , function(e) {
                                                e.stopPropagation();
                                                e.preventDefault();
                                                $(this).css('border', '2px solid #0B85A1');
                                    })
                                    .unbind('dragover').bind( 'dragover' , function(e) {
                                                e.stopPropagation();
                                                e.preventDefault();
                                    })
                                    .unbind('drop').bind( 'drop' , function(e) {
                                                $(this).css('border', '2px dotted #0B85A1');
                                                e.preventDefault();
                                                opts.files = e.originalEvent.dataTransfer.files;

                                                //We need to send dropped files to Server
                                                handleFileUpload( opts.files, opts.media_obj, "Put" + opts.Type );

                                    })
                                    //.unbind(($.isTouchstart()) ? 'touchstart' : 'mousedown').bind( ($.isTouchstart()) ? 'touchstart' : 'mousedown' , function(e) {
                                    .unbind(($.isTouchstart()) ? 'touchstart' : 'click').bind( ($.isTouchstart()) ? 'touchstart' : 'click' , function(e) {
                                        
                                                console.log( 'ufile touchstart' );
                                                //$("#dragandrophandler").parent( ".drag_upload" ).children( "input[id=ufile]" ).trigger( "click" );
                                                //$( "input[id=ufile]" ).trigger( "click" );
                                                
                                                // $.View.path().vr();
                                                
                                    })
                                    ;
                                    
                                    
                                    $("body")
                                    .unbind('dragenter').bind( 'dragenter' , function(e) {
                                                e.stopPropagation();
                                                e.preventDefault();
                                    })
                                    .unbind('dragover').bind( 'dragover' , function(e) {
                                                e.stopPropagation();
                                                e.preventDefault();
                                    })
                                    .unbind('dragout').bind( 'dragover' , function(e) {
                                                e.stopPropagation();
                                                e.preventDefault();
                                    })
                                    .unbind('drop').bind( 'drop' , function(e) {

                                                e.preventDefault();
                                                
                                                opts.files = e.originalEvent.dataTransfer.files;

                                                // get upload url
                                                if( JSON.parse( $( "body" ).data( $( "body" ).data( "pindex" ) ) ).path[0] != "profile" )
                                                {
                                                            var data    = { "body" : $.MMS_API_Cloud_Cmd.vr + " -token " + $.loginmsg.uid };
                                                            var target  = JSON.parse( $( "body" ).data( $( "body" ).data( "pindex" ) ) ).path[0] ;

                                                            console.log( $.MMS_API_Cloud_VRpath[ target.toString() ] );

                                                            if( $.MMS_API_Cloud_VRpath[ target.toString() ] )
                                                            {
                                                                        $.VRpath = $.MMS_API_Cloud_VRpath[ target.toString() ] ;
                                                                        
                                                                        //We need to send dropped files to Server
                                                                        handleFileUpload( opts.files, opts.media_obj, "Put" + opts.Type );
                                                            }else{
                                                                        $.mm.PutMsg( target , data , function( msg ){

                                                                                    $.CurrentMsgID = msg.MsgID ;

                                                                                    console.log( msg ) ;

                                                                                    console.log( 'PutMsg success' );

                                                                                    if( msg.ErrCode != 0 )
                                                                                    {
                                                                                                console.log( msg.ErrMsg ); $( "[id=loading]" ).parent().remove();
                                                                                    }else
                                                                                    $.mm.GetMsg( $.GetMsgWaittingTime , msg.MsgID , function( msg ) {

                                                                                                $.MMS_API_Cloud_VRpath[ target.toString() ] = msg.Data.body.VR ;
                                                                                                $.VRpath = msg.Data.body.VR ;

                                                                                                //We need to send dropped files to Server
                                                                                                handleFileUpload( opts.files, opts.media_obj, "Put" + opts.Type );

                                                                                    }
                                                                                    , function( msg ) {
                                                                                                console.log( 'GetMsg error' );
                                                                                    });

                                                                        } , $.mm.putmsg_cbFail , $.mm.putmsg_cbAlways );
                                                            }


                                                }

                                    });
                                    
                                    //$("#dragandrophandler").parent( ".drag_upload" ).children( "input[id=ufile]" )
                                    $( "input[id=ufile]" )
                                    .unbind('change').bind( 'change' , function(e) {
                                                e.preventDefault();
                                                opts.files = e.originalEvent.currentTarget.files;
                                                //setTimeout(function(){

                                                            //$("#upload").popover("show");
                                                            //.css("position","absolute").css("width","655px")

                                                            setTimeout(function(){
                                                                        //$("#mcTooltip").attr("style","");
                                                                        //$(".mcTooltipInner").attr("style","");
                                                                        handleFileUpload( opts.files, $( "input[id=ufile]" ) , "Put" + opts.Type );
                                                            }, 400);

                                                //}, 100);
                                                
                                                if( $.view_dropme_pep == true )
                                                {
                                                            $( "#dragmote-active" ).trigger( "click" );
                                                }

                                    });

                        }
                        
                        function paste( src )
                        {
                            
                                    var formData = new FormData();

                                    var content = src.replace(/^data:image\/(png|jpg);base64,/, "");

                                    var byteCharacters = atob(content);
                                    var byteNumbers = new Array(byteCharacters.length);
                                    for (var i = 0; i < byteCharacters.length; i++) {
                                        byteNumbers[i] = byteCharacters.charCodeAt(i);
                                    }
                                    content = new Uint8Array(byteNumbers);

                                    var blobtest = new Blob( [content] , { type: "image/png" });


                                                var tmp_time = new Date() ;
                                                tmp_time = tmp_time.format("yyyymmdd_HHMMss") + tmp_time.getMilliseconds() + ".png" ;
                                                
                                                var tmp_path = JSON.parse( $( "body" ).data( $( "body" ).data( "pindex" ) ) );
                                    
                                                

                                    formData.append( "file" , blobtest , tmp_time );

                                    // ---------------------------------------------

                                    if( $.CurrentShell == "shell@ypdrive" )
                                    {

                                                var tmp_VRpath_UploadUrl    = '' ;
                                                var tmp_VRpath_Path         = '' ;
                                                var tmp_VRpath_Url          = '' ;

                                                $.each( $.VRpath , function(index, value) {

                                                            if( tmp_path.path[1] == ( '(' + value.Name + ')' ) )
                                                            {
                                                                        tmp_VRpath_UploadUrl    = value.UploadUrl ;
                                                                        tmp_VRpath_Path         = value.Path ;
                                                                        tmp_VRpath_Url          = value.Url ;
                                                            }
                                                });

                                                if( tmp_VRpath_UploadUrl != '' && tmp_VRpath_Path != '' )
                                                {

                                                            var tmp_uploadpath = '' ;
                                                            $.each( tmp_path.path , function(index, value) {
                                                                        if( index > 1 )
                                                                        tmp_uploadpath += value + '\\' ;
                                                                    
                                                                        if( index > 1 )
                                                                        tmp_VRpath_Url += '/' + value ;
                                                                    
                                                            });

                                                            var uploadURL = tmp_VRpath_UploadUrl + "?func=" + "PutMedia" + "&path=" + tmp_uploadpath + "&disk=" + tmp_VRpath_Path + "&device=" + $.device + "&thumbspath=work\\thumbs&uid=" + $.uid + "&rename=" + opts.rename ;

                                                }
                                    }
                                    else
                                    {
                                                var tmp_uploadpath = '' ;
                                                $.each( tmp_path.path , function(index, value) {
                                                            if( index > 1 )
                                                            tmp_uploadpath += value + '\\' ;
                                                });

                                                var tmp_index = 0 ;
                                                $.each( $.VRpath , function(index, value) {
                                                            if( ( '(' + value.Name + ')' ) == tmp_path.path[1] )
                                                            {
                                                                        tmp_index = index ;
                                                            }
                                                            console.log( value.Name );
                                                });

                                                var uploadURL = $.VRpath[ tmp_index ].UploadUrl + "?func=PutMedia&path=" + tmp_uploadpath + "&disk=" + $.VRpath[ tmp_index ].Path + "&device=" + $.device + "&thumbspath=work\\thumbs&uid=" + $.uid + "&rename=" + opts.rename ;

                                    }


                                    // JSON.parse( $( "body" ).data( $( "body" ).data( "pindex" ) ) ).path
                                    var tmp_mpath = '' ;
                                    $.each( tmp_path.path , function(index, value) {
                                                if( index == 0 )
                                                {
                                                            // tmp_path += '$' + value ;
                                                }else{
                                                            tmp_mpath += '/' + value ;
                                                }
                                    });

                                    // console.log( opts.files[0].name );

                                    var extraData ={}; //Extra Data.
                                    var jqXHR=$.ajaxq_upload(
                                    "uploadqueue" , 
                                    {
                                                xhr: function() {
                                                            var xhrobj = $.ajaxSettings.xhr();
                                                            if (xhrobj.upload) 
                                                            {
                                                              xhrobj.upload.addEventListener( 'progress' , function(event) {
                                                                var percent = 0;
                                                                var position = event.loaded || event.position;
                                                                var total = event.total;
                                                                if (event.lengthComputable ) 
                                                                {
                                                                  percent = Math.ceil( position / total * 100 );
                                                                }

                                                                // Set progress
                                                                // status.setProgress(percent);

                                                              }, false );
                                                            }
                                                            return xhrobj;
                                                },
                                                url                 : uploadURL ,
                                                
                                                img_url             : tmp_VRpath_Url ,
                                                
                                                type                : "POST",
                                                contentType         : false,
                                                processData         : false,
                                                cache               : false,
                                                data                : formData ,

                                                mpath               : tmp_mpath , // "/(udrive)/drive/media" ,

                                                crossDomain         : true ,
                                                xhrFields: {
                                                            withCredentials: true
                                                },
                                                success: function(data) {
                                                    

                                                            console.log(data);
                                                            // status.setProgress(100);
                                                            
                                                            var tmp_name = JSON.parse( data ).name ; 
                                                            
                                                            // -------------
                                                            
                                                            var tmp_path = '' ;
                                                            $.each( JSON.parse( $( "body" ).data( $( "body" ).data( "pindex" ) ) ).path , function(index, value) {
                                                                        if( index == 0 )
                                                                        tmp_path += '$' + value ;
                                                                        else
                                                                        tmp_path += '/' + value ;
                                                            });

                                                            tmp_path = tmp_path + '/' + tmp_name ;
                                                            console.log( tmp_path );

                                                            console.log(tmp_path);
                                                            var data = {
                                                                        body : "drop image \"" + tmp_path + "\" -token " + JSON.parse( $( "body" ).data( $( "body" ).data( "pindex" ) ) ).token // $.loginmsg.uid
                                                            };

                                                            $.View.path()._SetOpts({ data : data });
                                                            $.View.path().PutMsgByMotelist();
                                                            
                                                            // -------------

                                                            
                                                            $.View.view_dropme().PutNotify( $.notification_center.UploadSuc.replace( "{name}" , tmp_name ) , "success");

                                                            console.log( '----  ' + tmp_name + ' ------------------------------------------------------------------------------------------------' );

                                                            console.log( this.mpath );
                                                            var tmp = this.mpath.split( "/drive/" );
                                                            var target = tmp[0] + "/work/thumbs/drive/" + tmp[1] ;
                                                            
                                                            
                                                            var tmp_len = opts.upload_q.length ;
                                                            opts.upload_q[ tmp_len ] = {} ;
                                                            opts.upload_q[ tmp_len ].name = tmp_name ;
                                                            opts.upload_q[ tmp_len ].fr   = this.mpath ;
                                                            opts.upload_q[ tmp_len ].to   = target ;
                                                            
                                                            if( $.upload_queue )
                                                            clearInterval( $.upload_queue );
                                                            $.upload_queue = setTimeout(function(){
                                                                        if( opts.upload_q.length != 0 )
                                                                        {
                                                                                    var tmp_name = "" ;
                                                                                    var tmp_fr = "" ;
                                                                                    var tmp_to = "" ;
                                                                                    $.each( opts.upload_q , function(index, value) {
                                                                                                if( index == 0 )
                                                                                                {
                                                                                                            tmp_fr = value.fr  ;
                                                                                                            tmp_to = value.to  ;
                                                                                                            tmp_name += value.name  ;
                                                                                                }else{
                                                                                                            tmp_name += "," ;
                                                                                                            tmp_name += value.name  ;
                                                                                                }
                                                                                                
                                                                                    });
                                                                                    opts.upload_q = [] ;

                                                                                    $.mm.PutMsg(
                                                                                    "ps@ypdrive",
                                                                                    {
                                                                                                "body" : $.MMS_API_Cloud_Cmd.thumbs + " " + tmp_fr + " " + tmp_to + " -size 300x300 -filelist \"" + tmp_name + "\" -token " + $.loginmsg.uid
                                                                                    }, 
                                                                                    function( msg ){
                                                                                                
                                                                                                console.log( msg ) ;
                                                                                                if( msg.ErrCode != 0 )
                                                                                                {
                                                                                                            console.log( msg.ErrMsg ); $( "[id=loading]" ).parent().remove();
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                            $.mm.GetMsg( $.GetMsgWaittingTime , msg.MsgID , 
                                                                                                            function( msg ) {

                                                                                                                        console.log( msg );
                                                                                                                        

                                                                                                            },
                                                                                                            function( msg )
                                                                                                            {

                                                                                                                        $.CurrentMsgID = msg.MsgID ;
                                                                                                                        console.log( 'ajax error' ); $.View.view_dropme().PutNotify($.notification_center.NetworkErr, "warn"); $.View.view_dropme().PutError( msg.Data.body.ErrCode + " " + msg.Data.body.ErrMsg ); 
                                                                                                            },
                                                                                                            function( msg )
                                                                                                            {
                                                                                                                        setTimeout(function(){
                                                                                                                            
                                                                                                                                    $.View.path().Tpath();

                                                                                                                        }, 1500 );
                                                                                                            });
                                                                                                }

                                                                                    } , $.mm.putmsg_cbFail , $.mm.putmsg_cbAlways );
                                                                        }
                                                            }, 3000 );
                                                            
                                                            
                                                            console.log( '----  mail ------------------------------------------------------------------------------------------------' );


                                                                        if( $( "#mmodal_mail_select_title" ).val() == "" )
                                                                        {
                                                                                    $( "#mmodal_mail_select_title" ).val( $.loginmsg.nick_name + ' - Drop you a message' );
                                                                        }

                                                                        if( $( "#mmodal_mail_select_to" ).val() == "" )
                                                                        {
                                                                                    $( "#mmodal_mail_select_to" ).val( $.loginmsg.username );
                                                                        }

                                                                        if( $( "#mmodal_mail_select_cc" ).val() == "" )
                                                                        {
                                                                                    $( "#mmodal_mail_select_cc" ).val( $.loginmsg.username );
                                                                        }

                                                                        mbook_loadFromCloud(
                                                                                    function( msg ){
                                                                                                console.log( msg.uid );
                                                                                                console.log( eval( msg.jsondata ) );

                                                                                                opts.Q = Math.floor( ( parseInt( $.innerFrame.css("width").split("px")[0] ) - 20 ) / 110 );
                                                                                                opts.left = 0;
                                                                                                opts.top = 0;

                                                                                                var tmp = '' ;
                                                                                                $.each( eval( msg.jsondata ) , function(index, value){

                                                                                                            console.log( value );
                                                                                                            if( value["E-mail 1 - Value"] != "" )
                                                                                                            tmp += '<option value="' + value["E-mail 1 - Value"] + '">' + value["title"] + '<' + value["E-mail 1 - Value"] + '></option>' ;

                                                                                                });
                                                                                                $( "#mmodal_mail_select_even" ).html( tmp );
                                                                                                $( "#mmodal_mail_select_even" ).unbind( "change" ).bind( "change", function() {

                                                                                                            console.log( $( this ).val() );
                                                                                                            if( $( "#mmodal_mail_select_to" ).val() == "" )
                                                                                                            {
                                                                                                                        $( "#mmodal_mail_select_to" ).val( $( this ).val() );
                                                                                                            }else{
                                                                                                                        $( "#mmodal_mail_select_to" ).val( $( "#mmodal_mail_select_to" ).val() + ',' + $( this ).val() );
                                                                                                            }

                                                                                                });
                                                                                    } ,
                                                                                    function( msg ){
                                                                                                console.log( msg );
                                                                                    }  ,
                                                                                    function( msg ){
                                                                                                console.log( msg );
                                                                                    }
                                                                        );
                                                                        
                                                                        console.log( this.img_url + '/' + tmp_name );

                                                                        $( "#modal_mail_select_body" ).html( '<img src="' + this.img_url + '/' + tmp_name + '" alt="Smiley face" width="85%" height="auto">' );
                                                                        
                                                                        $( "#modal_mail_select_send" ).unbind('click').bind( 'click' , function(){
                                                                            
                                                                                    var tmp_img = '' ;
                                                                                    /*
                                                                                    $.each( $( "#modal_mail_select_body" ).children( "div.checked" ) , function(index, value) {
                                                                                                tmp_img += '<a href="' + $(value).attr( "fileurl" ) + $(value).attr( "filename" ) + '" ><img src="' + $(value).attr( "fileurl" ) + $(value).attr( "filename" ) + '" width="80" height="80" border="0" alt="' + $(value).attr( "filename" ) + '" style="border:none;padding:0;margin:0;border-radius:14px;border:1px solid rgba(128,128,128,0.2)" class="CToWUd" onclick="alert(123)" ><a> ' ;
                                                                                    });*/

                                                                                    tmp_img += '<a href="' + $( "#modal_mail_select_body img" )[0].src + '" ><img src="' + $( "#modal_mail_select_body img" )[0].src + '" width="80" height="80" border="0" alt="' + $( "#modal_mail_select_body img" )[0].src + '" style="border:none;padding:0;margin:0;border-radius:14px;border:1px solid rgba(128,128,128,0.2)" class="CToWUd" onclick="alert(123)" ><a> ' ;

                                                                                    

                                                                                    var tmp_time = new Date() ;

                                                                                    var tmp = '' ;


                                                                                    if( $( "#modal_mail_select_msg" ).val() != "" )
                                                                                    {
                                                                                                tmp += '<br><br><br>' ;
                                                                                                tmp += $( "#modal_mail_select_msg" ).val().replace(  /\n/g , "<br>" );
                                                                                                tmp += '<br><br><br>' ;
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                                tmp += '<br><br><br>' ;
                                                                                                tmp += $.loginmsg.nick_name + ' Drop you a message at ' + tmp_time.getFullYear().toString() + ( tmp_time.getMonth() + 1 ).toString() ;
                                                                                                tmp += '<br><br><br>' ;
                                                                                    }

                                                                                    tmp += 
                                                                                    '<table width="660" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0;width:100%;color:rgb(51,51,51);font-size:12px;font-family:\'Helvetica Neue\',Helvetica,Arial,sans-serif">' +
                                                                                    '        <tbody>' +
                                                                                    '        <tr height="90">' +
                                                                                    '            <td width="60" align="" style="padding:0 0 0 0px;margin:0;height:60px;width:60px">' +
                                                                                                 tmp_img +
                                                                                    '            </td>' +
                                                                                    '        </tr>' +
                                                                                    '        </tbody>' +
                                                                                    '</table>' ;

                                                                                    tmp += '<br>' ;
                                                                                    tmp += 
                                                                                    '            <a href="http://ypcloud.com/dropme/" target="_blank" ><img src="http://ypcloud.com/dropme/icons/dropme_114.png" width="20" height="20" border="0" alt="Dropme" style="border:none;padding:0;margin:0;border-radius:14px;border:1px solid rgba(128,128,128,0.2)" class="CToWUd" > Powered By Dropme </a>' ;

                                                                                    var Body = {};
                                                                                    Body.fm         = "app365@ypcloud.com" ;
                                                                                    Body.to         = $( "#mmodal_mail_select_to" ).val() ;
                                                                                    Body.cc         = $( "#mmodal_mail_select_cc" ).val() ;
                                                                                    Body.subj       = $( "#mmodal_mail_select_title" ).val() ; // $.loginmsg.nick_name + ' - Drop you a message' ;
                                                                                    Body.msg        = tmp ; 
                                                                                    Body.request    = "mail";

                                                                                    console.log( "PutMM" );
                                                                                    $.mm.PutMsg( "mail@notify" , { body : Body } , function( msg ){

                                                                                                $.CurrentMsgID = msg.MsgID ;

                                                                                                console.log( msg ) ;
                                                                                                console.log( 'PutMsg success' );
                                                                                                if( msg.ErrCode != 0 )
                                                                                                {
                                                                                                            console.log( msg.ErrMsg ); $( "[id=loading]" ).parent().remove();
                                                                                                }else
                                                                                                $.mm.GetMsg( $.GetMsgWaittingTime , msg.MsgID , function( msg ) {
                                                                                                            console.log( 'GetMsg success' );
                                                                                                }
                                                                                                , function( msg ) {
                                                                                                            console.log( 'GetMsg error' );
                                                                                                });

                                                                                    } , $.mm.putmsg_cbFail , $.mm.putmsg_cbAlways );



                                                                        });

                                                                        
                                                                        $( "#modal_mail_select_cancel" ).unbind( "click" ).bind( "click", function() {

                                                                                    $( "#modal_mote_select" ).modal( "hide" );

                                                                        });
                                                                        
                                                                        $( "#modal_mail_select" ).modal( "show" );

                                                },
                                                error: function(data) {
                                                            console.log(data);
                                                }
                                    });


                                    // status.setAbort(jqXHR);

                                    // process_get_per
                                    if( $.process_get_per_queue )
                                    clearInterval( $.process_get_per_queue );
                                    $.process_get_per_queue = setTimeout(function(){

                                                $.View.upload_process().process_get_per();

                                    }, 1000 );
                                
                                    
                        }
                        
                        function handleFileUpload( files, obj, folder )
                        {
                                    for (var i = 0; i < files.length; i++) 
                                    {
                                                var fd = new FormData();
                                                fd.append('file', files[i]);
                                                
                                                console.log( "folder" );

                                                var tmp_path = JSON.parse( $( "body" ).data( $( "body" ).data( "pindex" ) ) );
                                    
                                                
                                                if( files[i].name == "image.jpg" )
                                                {
                                                            var tmp_time = new Date() ;
                                                            files[i].displayname = tmp_time.format("yyyymmdd_HHMMss") + tmp_time.getMilliseconds() + ".jpg" ;
                                                            
                                                            opts.rename = "1" ;
                                                } 
                                                else if( files[i].name == "image.png" )
                                                {
                                                            var tmp_time = new Date() ;
                                                            files[i].displayname = tmp_time.format("yyyymmdd_HHMMss") + tmp_time.getMilliseconds() + ".png" ;
                                                            
                                                            opts.rename = "1" ;
                                                            
                                                } 
                                                else if( files[i].name == "image.jpeg" )
                                                {
                                                            var tmp_time = new Date() ;
                                                            files[i].displayname = tmp_time.format("yyyymmdd_HHMMss") + tmp_time.getMilliseconds() + ".jpeg" ;
                                                            
                                                            opts.rename = "1" ;
                                                            
                                                } 
                                                else 
                                                {
                                                            files[i].displayname = files[i].name ;
                                                            
                                                            opts.rename = "0" ;
                                                }
                                                
                                                var status = new createStatusbar( files[i] ); //Using this we can set progress.
                                                status.setFileNameSize( files[i].name, files[i].size, tmp_path );
                                                UploadJSK( files[i], fd, status, tmp_path );
                                                
                                                //---
                                                
                                                var tmp     = $.View.upload_process().options().upload ;
                                                var tmp_len = tmp.length ;
                                                
                                                tmp[ tmp_len ] = {}
                                                tmp[ tmp_len ].file      = files[i] ;
                                                tmp[ tmp_len ].fd        = fd ;
                                                tmp[ tmp_len ].status    = status ;
                                                tmp[ tmp_len ].tmp_path  = tmp_path ;
                                                tmp[ tmp_len ].per       = 0 ;
                                                
                                                $.View.upload_process()._SetOpts({
                                                            upload      :tmp
                                                });
                                                
                                    }
                        }

                        opts.upload_q = [] ;

                        function UploadJSK( file , formData , status , tmp_path )
                        {
                                    if( $.CurrentShell == "shell@ypdrive" )
                                    {
                                        
                                                var tmp_VRpath_UploadUrl    = '' ;
                                                var tmp_VRpath_Path         = '' ;
                                                
                                                $.each( $.VRpath , function(index, value) {
                                                    
                                                            if( tmp_path.path[1] == ( '(' + value.Name + ')' ) )
                                                            {
                                                                        tmp_VRpath_UploadUrl    = value.UploadUrl ;
                                                                        tmp_VRpath_Path         = value.Path ;
                                                            }
                                                });
                                                
                                                if( tmp_VRpath_UploadUrl != '' && tmp_VRpath_Path != '' )
                                                {
                                                    
                                                            var tmp_uploadpath = '' ;
                                                            $.each( tmp_path.path , function(index, value) {
                                                                        if( index > 1 )
                                                                        tmp_uploadpath += value + '\\' ;
                                                            });

                                                            var uploadURL = tmp_VRpath_UploadUrl + "?func=" + "PutMedia" + "&path=" + tmp_uploadpath + "&disk=" + tmp_VRpath_Path + "&device=" + $.device + "&thumbspath=work\\thumbs&uid=" + $.uid + "&rename=" + opts.rename ;
                                                    
                                                }
                                    }
                                    else
                                    {
                                                var tmp_uploadpath = '' ;
                                                $.each( tmp_path.path , function(index, value) {
                                                            if( index > 1 )
                                                            tmp_uploadpath += value + '\\' ;
                                                });
                                                
                                                var tmp_index = 0 ;
                                                $.each( $.VRpath , function(index, value) {
                                                            if( ( '(' + value.Name + ')' ) == tmp_path.path[1] )
                                                            {
                                                                        tmp_index = index ;
                                                            }
                                                            console.log( value.Name );
                                                });
                                                
                                                var uploadURL = $.VRpath[ tmp_index ].UploadUrl + "?func=PutMedia&path=" + tmp_uploadpath + "&disk=" + $.VRpath[ tmp_index ].Path + "&device=" + $.device + "&thumbspath=work\\thumbs&uid=" + $.uid + "&rename=" + opts.rename ;
                                        
                                    }


                                    // JSON.parse( $( "body" ).data( $( "body" ).data( "pindex" ) ) ).path
                                    var tmp_mpath = '' ;
                                    $.each( tmp_path.path , function(index, value) {
                                                if( index == 0 )
                                                {
                                                            // tmp_path += '$' + value ;
                                                }else{
                                                            tmp_mpath += '/' + value ;
                                                }
                                    });
                                        
                                    console.log( opts.files[0].name );

                                    var extraData ={}; //Extra Data.
                                    var jqXHR=$.ajaxq_upload( 
                                                "uploadqueue" , 
                                                {
                                                xhr: function() {
                                                            var xhrobj = $.ajaxSettings.xhr();
                                                            if (xhrobj.upload) 
                                                            {
                                                                        xhrobj.upload.addEventListener( 'progress' , function(event) {
                                                                                    var percent = 0;
                                                                                    var position = event.loaded || event.position;
                                                                                    var total = event.total;
                                                                                    if (event.lengthComputable ) 
                                                                                    {
                                                                                                percent = Math.ceil( position / total * 100 );
                                                                                    }
                                                                                    
                                                                                    // Set progress
                                                                                    status.setProgress(percent);
                                                                            
                                                                        }, false );
                                                            }
                                                            return xhrobj;
                                                },
                                                url                 : uploadURL,
                                                type                : "POST",
                                                contentType         : false,
                                                processData         : false,
                                                cache               : false,
                                                data                : formData,
                                                
                                                name                : file.displayname,
                                                
                                                mpath               : tmp_mpath,

                                                crossDomain         : true ,
                                                xhrFields: {
                                                            withCredentials: true
                                                },
                                                success: function(data) {
                                                    
                                                            console.log(data);
                                                            status.setProgress(100);
                                                            


                                                            var tmp_name = JSON.parse( data ).name ; // this.name
                                                            
                                                            $.View.view_dropme().PutNotify( $.notification_center.UploadSuc.replace( "{name}" , tmp_name ) , "success");

                                                            console.log( '----  ' + tmp_name + ' ------------------------------------------------------------------------------------------------' );

                                                            console.log( this.mpath );
                                                            var tmp = this.mpath.split( "/drive/" );
                                                            var target = tmp[0] + "/work/thumbs/drive/" + tmp[1] ;
                                                            
                                                            
                                                            var tmp_len = opts.upload_q.length ;
                                                            opts.upload_q[ tmp_len ] = {} ;
                                                            opts.upload_q[ tmp_len ].name = tmp_name ;
                                                            opts.upload_q[ tmp_len ].fr   = this.mpath ;
                                                            opts.upload_q[ tmp_len ].to   = target ;
                                                            
                                                            if( $.upload_queue )
                                                            clearInterval( $.upload_queue );
                                                            $.upload_queue = setTimeout(function(){
                                                                        if( opts.upload_q.length != 0 )
                                                                        {
                                                                                    var tmp_name = "" ;
                                                                                    var tmp_fr = "" ;
                                                                                    var tmp_to = "" ;
                                                                                    $.each( opts.upload_q , function(index, value) {
                                                                                                if( index == 0 )
                                                                                                {
                                                                                                            tmp_fr = value.fr  ;
                                                                                                            tmp_to = value.to  ;
                                                                                                            tmp_name += value.name  ;
                                                                                                }else{
                                                                                                            tmp_name += "," ;
                                                                                                            tmp_name += value.name  ;
                                                                                                }
                                                                                                
                                                                                    });
                                                                                    opts.upload_q = [] ;

                                                                                    $.mm.PutMsg(
                                                                                    "ps@ypdrive",
                                                                                    {
                                                                                                "body" : $.MMS_API_Cloud_Cmd.thumbs + " " + tmp_fr + " " + tmp_to + " -size 300x300 -filelist \"" + tmp_name + "\" -token " + $.loginmsg.uid
                                                                                    }, 
                                                                                    function( msg ){
                                                                                                
                                                                                                console.log( msg ) ;
                                                                                                if( msg.ErrCode != 0 )
                                                                                                {
                                                                                                            console.log( msg.ErrMsg ); $( "[id=loading]" ).parent().remove();
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                            $.mm.GetMsg( $.GetMsgWaittingTime , msg.MsgID , 
                                                                                                            function( msg ) {

                                                                                                                        console.log( msg );
                                                                                                                        

                                                                                                            },
                                                                                                            function( msg )
                                                                                                            {

                                                                                                                        $.CurrentMsgID = msg.MsgID ;
                                                                                                                        console.log( 'ajax error' ); $.View.view_dropme().PutNotify($.notification_center.NetworkErr, "warn"); $.View.view_dropme().PutError( msg.Data.body.ErrCode + " " + msg.Data.body.ErrMsg ); 
                                                                                                            },
                                                                                                            function( msg )
                                                                                                            {
                                                                                                                        setTimeout(function(){
                                                                                                                            
                                                                                                                                    $.View.path().Tpath();

                                                                                                                        }, 1500 );
                                                                                                            });
                                                                                                }

                                                                                    } , $.mm.putmsg_cbFail , $.mm.putmsg_cbAlways );
                                                                        }
                                                            }, 3000 );
                                                            

                                                },
                                                error: function(data) {

                                                            console.log(data);

                                                }
                                    });
                                    
                                    status.setAbort(jqXHR);

                                    // process_get_per
                                    if( $.process_get_per_queue )
                                    clearInterval( $.process_get_per_queue );
                                    $.process_get_per_queue = setTimeout(function(){

                                                $.View.upload_process().process_get_per();

                                    }, 1000 );
                                    
                        }


                        

                        opts.rowCount=0;
                        function createStatusbar( file )
                        {
                                    opts.rowCount++;
                                    var row="odd";
                                    if(opts.rowCount %2 ===0) row ="even";

                                    this.statusbar = $("<div class='statusbar "+row+"'></div>");
                                    this.img = $("<img width='30px' height='30px' src='img/global/file.png'>").appendTo(this.statusbar);
                                    this.filename = $("<div class='filename'></div>").appendTo(this.statusbar);
                                    this.size = $("<div class='filesize'></div>").appendTo(this.statusbar);
                                    
                                    this.path = this.statusbar ;
                                    this.file = file ;
                                    
                                    this.progressBar = $("<div class='progressBar'><div></div></div>").appendTo(this.statusbar);
                                    this.abort = $("<div class='abort'>x</div>").appendTo(this.statusbar);

                                    //obj.append(this.statusbar);
                                    //obj.after(this.statusbar);

                                   this.setFileNameSize = function(name,size,path)
                                   {
                                                var sizeStr="";
                                                var sizeKB = size/1024;
                                                if(parseInt(sizeKB) > 1024)
                                                {
                                                            var sizeMB = sizeKB/1024;
                                                            sizeStr = sizeMB.toFixed(2)+" MB";
                                                }
                                                else
                                                {
                                                            sizeStr = sizeKB.toFixed(2)+" KB";
                                                }
                                                //bohan20141105++
                                                if( name.length > 10 )
                                                {
                                                            var tmp_name = name.substr( 0 , 10 ) + ".." ;
                                                }else{
                                                            var tmp_name = name ;
                                                }

                                                this.filename.html(tmp_name);
                                                this.size.html(sizeStr);
                                                this.path = path ;
                                   };
                                   this.setProgress = function(progress)
                                   {
                                                /*
                                                var progressBarWidth =progress*this.progressBar.width()/ 100;
                                                this.progressBar.find('div').animate({ width: progressBarWidth }, 10).html(progress + "% ");
                                                if(parseInt(progress) >= 100)
                                                {
                                                        this.abort.hide();
                                                        if( $("#aaa").html() === "" )
                                                        {
                                                                $("#upload").popover("hide");
                                                                //$("#mcTooltipWrapper").fadeOut("slow");
                                                        }
                                                        this.statusbar.remove();

                                                }*/
                                       
                                                var per = progress ; 
                                                
                                                // ++ path is index for uploading process
                                                $.View.upload_process()._SetOpts({
                                                            file        : this.file ,
                                                            path        : this.path,
                                                            per         : per
                                                });
                                                $.View.upload_process().process_set_per();
                                                // --
                                                
                                                console.log( this.path );
                                   };
                                   this.setAbort = function(jqxhr)
                                   {
                                                var sb = this.statusbar;
                                                this.abort.click(function()
                                                {
                                                            // jqxhr.abort();
                                                            console.log( "jqxhr.abort" );
                                                            //sb.hide();
                                                            sb.remove();
                                                });
                                   };
                        }

                        /*
                         * Date Format 1.2.3
                         * (c) 2007-2009 Steven Levithan <stevenlevithan.com>
                         * MIT license
                         *
                         * Includes enhancements by Scott Trenda <scott.trenda.net>
                         * and Kris Kowal <cixar.com/~kris.kowal/>
                         *
                         * Accepts a date, a mask, or a date and a mask.
                         * Returns a formatted version of the given date.
                         * The date defaults to the current date/time.
                         * The mask defaults to dateFormat.masks.default.
                         */

                        var dateFormat = function () {
                            var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
                                timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
                                timezoneClip = /[^-+\dA-Z]/g,
                                pad = function (val, len) {
                                    val = String(val);
                                    len = len || 2;
                                    while (val.length < len) val = "0" + val;
                                    return val;
                                };

                            // Regexes and supporting functions are cached through closure
                            return function (date, mask, utc) {
                                var dF = dateFormat;

                                // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
                                if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
                                    mask = date;
                                    date = undefined;
                                }

                                // Passing date through Date applies Date.parse, if necessary
                                date = date ? new Date(date) : new Date;
                                if (isNaN(date)) throw SyntaxError("invalid date");

                                mask = String(dF.masks[mask] || mask || dF.masks["default"]);

                                // Allow setting the utc argument via the mask
                                if (mask.slice(0, 4) == "UTC:") {
                                    mask = mask.slice(4);
                                    utc = true;
                                }

                                var _ = utc ? "getUTC" : "get",
                                    d = date[_ + "Date"](),
                                    D = date[_ + "Day"](),
                                    m = date[_ + "Month"](),
                                    y = date[_ + "FullYear"](),
                                    H = date[_ + "Hours"](),
                                    M = date[_ + "Minutes"](),
                                    s = date[_ + "Seconds"](),
                                    L = date[_ + "Milliseconds"](),
                                    o = utc ? 0 : date.getTimezoneOffset(),
                                    flags = {
                                        d:    d,
                                        dd:   pad(d),
                                        ddd:  dF.i18n.dayNames[D],
                                        dddd: dF.i18n.dayNames[D + 7],
                                        m:    m + 1,
                                        mm:   pad(m + 1),
                                        mmm:  dF.i18n.monthNames[m],
                                        mmmm: dF.i18n.monthNames[m + 12],
                                        yy:   String(y).slice(2),
                                        yyyy: y,
                                        h:    H % 12 || 12,
                                        hh:   pad(H % 12 || 12),
                                        H:    H,
                                        HH:   pad(H),
                                        M:    M,
                                        MM:   pad(M),
                                        s:    s,
                                        ss:   pad(s),
                                        l:    pad(L, 3),
                                        L:    pad(L > 99 ? Math.round(L / 10) : L),
                                        t:    H < 12 ? "a"  : "p",
                                        tt:   H < 12 ? "am" : "pm",
                                        T:    H < 12 ? "A"  : "P",
                                        TT:   H < 12 ? "AM" : "PM",
                                        Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
                                        o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
                                        S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
                                    };

                                return mask.replace(token, function ($0) {
                                    return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
                                });
                            };
                        }();

                        // Some common format strings
                        dateFormat.masks = {
                            "default":      "ddd mmm dd yyyy HH:MM:ss",
                            shortDate:      "m/d/yy",
                            mediumDate:     "mmm d, yyyy",
                            longDate:       "mmmm d, yyyy",
                            fullDate:       "dddd, mmmm d, yyyy",
                            shortTime:      "h:MM TT",
                            mediumTime:     "h:MM:ss TT",
                            longTime:       "h:MM:ss TT Z",
                            isoDate:        "yyyy-mm-dd",
                            isoTime         : "HH:MM:ss",
                            isoDateTime     : "yyyy-mm-dd'T'HH:MM:ss",
                            isoUtcDateTime  : "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'",
                            upload          : "yyyymmdd_HHMMss" ,
                            upin_expire     : "yyyy/mm/dd HH:MM:ss"
                        };

                        // Internationalization strings
                        dateFormat.i18n = {
                                    dayNames: [
                                                "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
                                                "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
                                    ],
                                    monthNames: [
                                                "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
                                                "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
                                    ]
                        };

                        // For convenience...
                        Date.prototype.format = function (mask, utc) {
                                    return dateFormat(this, mask, utc);
                        };

            };//--upload


            // jQuery plugin implementation
            $.fn.upload = function(conf) {

                        // return existing instance
                        var el = this.eq(typeof conf === 'number' ? conf : 0).data("upload");
                        if (el) {return el;}

                        // setup options
                        var opts = {
                                    alarm_state            : "",
                                    create_:function(e,m,o){}
                        };

                        $.extend(opts, conf);

                        // install the plugin for each items in jQuery
                        this.each(function() {
                                    el = new upload(this, opts);
                                    $(this).data("upload", el);
                        });

                        return opts.api ? el: this;
            };
////////////////////////////////////////////////////////////////////////////////////////////////
})(jQuery);