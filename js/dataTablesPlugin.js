var createTable = function(div_id, table_id, row) {
        var th = "";
        var th_foot = "";
        for(var i=0;i<row.length;i++) {
            th += '<th>'+row[i]+'</th>';
            th_foot += '<th style="text-align:right; color: rgb(153, 153, 153);"></th>';
        }
        var table = '<table id="'+table_id.replace("#","")+'" class="display select" width="100%" cellspacing="0">\n\
                        <thead>\n\
                            <tr>\n\
                                <th><input name="select_all" value="1" type="checkbox"></th>'
                                + th +
                            '</tr>\n\
                        </thead>\n\
                        <tfoot>\n\
                            <tr>\n\
                                <th></th>\n\
                                '+th_foot+'\n\
                            </tr>\n\
                        </tfoot>\n\
                    </table>';
        $(div_id).append(table);
};

var createSearchTable = function(div_id, table_id, row) {
        var tr = "";
        
        for( var i=0 ; i<row.length ; i++ )
        {
                if( row[i] != "操作" )
                tr += '<tr id="filter_col'+(i+2)+'" data-column="'+(i+1)+'">'
                        + '<td>' + row[i].replace( /<br>/g , "" ) + '</td>\n\
                        <td align="left">\n\
                            <input type="text" class="column_filter" id="col'+(i+1)+'_filter">\n\
                        </td>\n\
                    <tr>';
        }
        var table = '<table id="'+table_id.replace("#","")+'_search" cellpadding="3" cellspacing="0" border="0" style="width: 50%; margin: 2em auto; " >\n\
                        <tbody>'
                            + tr +
                        '</tbody>\n\
                    </table>';
        $(div_id).html(table);
};

var createDataTable = function(table_id, ajax,  order,  lengthMenu, addtag) {
        console.log( addtag );
        addtag = addtag || [];
        if( lengthMenu )
        {
                    var tmp_lengthMenu = lengthMenu  ;
        }else{
                    var tmp_lengthMenu = [[10, 25, 50, -1], [10, 25, 50, "All"]]  ;
        }
    
        console.log( addtag );
        $(table_id).DataTable({
            "pageLength": 50,
            "ajax": ajax, // 'examples/ajax/data/ids-arrays.txt
            "ajax_callback_tag": addtag,
            'columnDefs': [{
               'targets': 0,
               'searchable': false,
               'orderable': false,
               'width': '1%',
               'className': 'dt-body-center',
               'render': function (data, type, full, meta){
                   return '<input type="checkbox">';
               }
            }],
            "lengthMenu": tmp_lengthMenu ,
            'order': order, // [[4, 'desc']]
            'rowCallback': function(row, data, dataIndex){
               // Get row ID
               var rowId = data[0];
               
               $(table_id).data( "rows_len" , rowId );
               
               // If row ID is in the list of selected row IDs
               if($.inArray(rowId, $(table_id).data("rows_selected")) !== -1){
                        $(row).find('input[type="checkbox"]').prop('checked', true);
                        $(row).addClass('selected');
               }
            },
            'footerCallback': function ( row, data, start, end, display ) {
                
                var api = this.api(), data;
                
                fn_footerCallback( this , api , data );
                
            }
        });
}

var createDataTablePipeline = function(table_id, ajax,  order,  lengthMenu, addtag) {
        console.log( addtag );
        addtag = addtag || [];
        if( lengthMenu )
        {
                    var tmp_lengthMenu = lengthMenu  ;
        }else{
                    var tmp_lengthMenu = [[10, 25, 50, -1], [10, 25, 50, "All"]]  ;
        }
    
        $(table_id).DataTable({
            "pageLength": 50,
            "processing": true,
            "serverSide": true,
            "ajax": $.fn.dataTable.pipeline( {
                url: ajax,
                pages: 1 // number of pages to cache
            } ), // 'examples/ajax/data/ids-arrays.txt
            "ajax_callback_tag": addtag,
            'columnDefs': [{
               'targets': 0,
               'searchable': false,
               'orderable': false,
               'width': '1%',
               'className': 'dt-body-center',
               'render': function (data, type, full, meta){
                   return '<input type="checkbox">';
               }
            }],
            "lengthMenu": tmp_lengthMenu ,
            'order': order, // [[4, 'desc']]
            'rowCallback': function(row, data, dataIndex){
               // Get row ID
               var rowId = data[0];
               
               $(table_id).data( "rows_len" , rowId );
               
               // If row ID is in the list of selected row IDs
               if($.inArray(rowId, $(table_id).data("rows_selected")) !== -1){
                        $(row).find('input[type="checkbox"]').prop('checked', true);
                        $(row).addClass('selected');
               }
               
            },
            'footerCallback': function ( row, data, start, end, display ) {
                
                var api = this.api(), data;
                
                fn_footerCallback( this , api , data );
               
            },
            "drawCallback": function( settings ) {
                if( typeof datatable_callback === "function" )
                    datatable_callback();
            }
        });
}

$.fn.dataTable.pipeline = function ( opts ) {
    // Configuration options
    var conf = $.extend( {
        pages: 1,     // number of pages to cache
        url: '',      // script url
        data: null,   // function or object with parameters to send to the server
                      // matching how `ajax.data` works in DataTables
        method: 'POST' // Ajax HTTP method
    }, opts );
 
    // Private variables for storing the cache
    var cacheLower = -1;
    var cacheUpper = null;
    var cacheLastRequest = null;
    var cacheLastJson = null;
 
    return function ( request, drawCallback, settings ) {
        var ajax          = false;
        var requestStart  = request.start;
        var drawStart     = request.start;
        var requestLength = request.length;
        var requestEnd    = requestStart + requestLength;
         
        if ( settings.clearCache ) {
            // API requested that the cache be cleared
            ajax = true;
            settings.clearCache = false;
        }
        else if ( cacheLower < 0 || requestStart < cacheLower || requestEnd > cacheUpper ) {
            // outside cached data - need to make a request
            ajax = true;
        }
        else if ( JSON.stringify( request.order )   !== JSON.stringify( cacheLastRequest.order ) ||
                  JSON.stringify( request.columns ) !== JSON.stringify( cacheLastRequest.columns ) ||
                  JSON.stringify( request.search )  !== JSON.stringify( cacheLastRequest.search )
        ) {
            // properties changed (ordering, columns, searching)
            ajax = true;
        }
         
        // Store the request for checking next time around
        cacheLastRequest = $.extend( true, {}, request );
 
        if ( ajax ) {
            // Need data from the server
            if ( requestStart < cacheLower ) {
                requestStart = requestStart - (requestLength*(conf.pages-1));
 
                if ( requestStart < 0 ) {
                    requestStart = 0;
                }
            }
             
            cacheLower = requestStart;
            cacheUpper = requestStart + (requestLength * conf.pages);
 
            request.start = requestStart;
            request.length = requestLength*conf.pages;
 
            // Provide the same `data` options as DataTables.
            if ( $.isFunction ( conf.data ) ) {
                // As a function it is executed with the data object as an arg
                // for manipulation. If an object is returned, it is used as the
                // data object to submit
                var d = conf.data( request );
                if ( d ) {
                    $.extend( request, d );
                }
            }
            else if ( $.isPlainObject( conf.data ) ) {
                // As an object, the data given extends the default
                $.extend( request, conf.data );
            }
 
            settings.jqXHR = $.ajax( {
                "type":     conf.method,
                "url":      conf.url,
                "data":     request,
                "dataType": "json",
                "cache":    false,
                "success":  function ( json ) {
                    if( settings.oInit["ajax_callback_tag"][0] !== undefined ){
                        $.each( json.data , function( k , v ){
                            $.each( settings.oInit["ajax_callback_tag"] , function( k2 , v2 ){
                                console.log(json.data[k][v2]);
                                console.log(v[v2]);
                                json.data[k][v2] = "<a>"+v[v2]+"</a>";
                            });
                        });
                    }
                    cacheLastJson = $.extend(true, {}, json);
 
                    if ( cacheLower != drawStart ) {
                        json.data.splice( 0, drawStart-cacheLower );
                    }
                    if ( requestLength >= -1 ) {
                        json.data.splice( requestLength, json.data.length );
                    }
                     
                    drawCallback( json );
                }
            } );
        }
        else {
            json = $.extend( true, {}, cacheLastJson );
            json.draw = request.draw; // Update the echo for each response
            json.data.splice( 0, requestStart-cacheLower );
            json.data.splice( requestLength, json.data.length );
 
            drawCallback(json);
        }
    }
};


var fn_footerCallback = function( foucs , api , table_id ) {
        
        //console.log( foucs , api , table_id );
        console.log( "fn_footerCallback" );
        
        // Remove the formatting to get integer data for summation
        var intVal = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
        };

        var count_num =  '查詢完畢' ;
        
        if( location.pathname.split( "mgm_converted_income_inquiry.php" ).length > 1 )
        {
                    if( foucs.parent().parent().attr( "id" ) == "datatable" )
                    {
                                var count_row = [] ;
                                count_row[0] = {} ;
                                count_row[0].index = parseInt($(foucs).find("thead th").length) - 4;
                                count_row[0].html  = $(  $(foucs).find("thead th")[ count_row[0].index ] ).html() ;
                                count_row[0].a     = 0 ;
                                count_row[0].b     = 0 ;
                                count_row[1] = {} ;
                                count_row[1].index = parseInt($(foucs).find("thead th").length) - 3;
                                count_row[1].html  = $(  $(foucs).find("thead th")[ count_row[1].index ] ).html() ;
                                count_row[1].a     = 0 ;
                                count_row[1].b     = 0 ;
                                count_row[2] = {} ;
                                count_row[2].index = parseInt($(foucs).find("thead th").length) - 2;
                                count_row[2].html  = $(  $(foucs).find("thead th")[ count_row[2].index ] ).html() ;
                                count_row[2].a     = 0 ;
                                count_row[2].b     = 0 ;

                                var ctotal = [] ;
                                var cpageTotal = [] ;

                                var count_html  =  '' ;


                                for(var i=0; i < 3 ; i++) 
                                {

                                            // Total over all pages
                                            if( count_row[i].index )
                                            ctotal[i] = api
                                                .column( count_row[i].index )
                                                .data()
                                                .reduce( function (a, b) {
                                                    var tmp_b = b.split( "/" ) ;
                                                    count_row[i].a += parseInt( tmp_b[0] ) ;
                                                    count_row[i].b += parseInt( tmp_b[1] ) ;
                                                        // console.log( parseInt( tmp_b[0] ) , parseInt( tmp_b[1] ) );
                                                    return intVal(a) + intVal(b);
                                                }, 0 );

                                            // Total over foucs page
                                            if( count_row[i].index )
                                            cpageTotal[i] = api
                                                .column( count_row[i].index , { page: 'current'} )
                                                .data()
                                                .reduce( function (a, b) {
                                                    return intVal(a) + intVal(b);
                                                }, 0 );


                                            // ----------------------------

                                            var num = new Number(cpageTotal[i]);
                                            cpageTotal[i] = num.toFixed(2) ;
                                            var num = new Number(ctotal[i]);
                                            ctotal[i] = num.toFixed(2) ;

                                            // count_html += ' ' + count_row[i].html.split( "/" )[0] + ' 總計, ' + count_row[i].a + count_row[i].html.split( "/" )[1] + '總計 ' + count_row[i].b + '<br>' ;
//                                            count_html += ' ' + count_row[i].html + ' ' + count_row[i].a + ' / ' + count_row[i].b + '<br>' ;
                                            
                                            foucs.find( "tfoot th:eq("+count_row[i].index+")" ).html( count_row[i].html + ' ' + count_row[i].a + ' / ' + count_row[i].b );
                                            
                                }
                                
                    }
                    else if( foucs.parent().parent().attr( "id" ) == "datatable2" )
                    {
                                var count_row = [] ;
                                count_row[0] = {} ;
                                count_row[0].index = parseInt($(foucs).find("thead th").length) - 4;
                                count_row[0].html  = $(  $(foucs).find("thead th")[ count_row[0].index ] ).html() ;
                                count_row[1] = {} ;
                                count_row[1].index = parseInt($(foucs).find("thead th").length) - 3;
                                count_row[1].html  = $(  $(foucs).find("thead th")[ count_row[1].index ] ).html() ;
                                count_row[2] = {} ;
                                count_row[2].index = parseInt($(foucs).find("thead th").length) - 2;
                                count_row[2].html  = $(  $(foucs).find("thead th")[ count_row[2].index ] ).html() ;
                                count_row[3] = {} ;
                                count_row[3].index = parseInt($(foucs).find("thead th").length) - 1;
                                count_row[3].html  = $(  $(foucs).find("thead th")[ count_row[1].index ] ).html() ;

                                var ctotal = [] ;
                                var cpageTotal = [] ;

                                var count_html =  foucs.data( "rows_len" ) + ' 個作者<br>' ;


                                for(var i=0; i < 4 ; i++) 
                                {

                                            // Total over all pages
                                            if( count_row[i].index )
                                            ctotal[i] = api
                                                .column( count_row[i].index )
                                                .data()
                                                .reduce( function (a, b) {
                                                    return intVal(a) + intVal(b);
                                                }, 0 );

                                            // Total over foucs page
                                            if( count_row[i].index )
                                            cpageTotal[i] = api
                                                .column( count_row[i].index , { page: 'current'} )
                                                .data()
                                                .reduce( function (a, b) {
                                                    return intVal(a) + intVal(b);
                                                }, 0 );


                                            // ----------------------------

                                            var num = new Number(cpageTotal[i]);
                                            cpageTotal[i] = num.toFixed(2) ;
                                            var num = new Number(ctotal[i]);
                                            ctotal[i] = num.toFixed(2) ;

                                            if( i == 3 )
                                            count_html += ' ' + '合計' + ctotal[i] + '<br>' ;
                                            else
                                            count_html += ' ' + count_row[i].html + ' 本頁面' + cpageTotal[i] + ' 總計' + ctotal[i] + '<br>' ;
//                                            foucs.find( "tfoot th:eq("+count_row[i].index+")" ).html( count_row[i].html + ' ' + count_row[i].a + ' / ' + count_row[i].b );
                                            
                                }


                                // var count_html = '本頁面 G 幣 ' + pageTotal + ' <br>( 總計 G 幣 ' + total + ' ) ' ;
                                // var count_html = '本頁面已驗證數 ' + cpageTotal + ' <br>( 總計已驗證數 ' + ctotal + ' ) <br>本頁面未驗證數 $'+pageTotal +' <br>( 總計未驗證數 $'+ total +' )' ;

                        
                    }
                    
        }
        else if( location.pathname.split( "mgm_membership_income_statistics.php" ).length > 1 )
        {
                if( foucs.attr("id") === "example" ){
                        var count_rowN = [] ;
                        var count_rowN_output = [] ;
                    
                        count_rowN[0] = parseInt($(foucs).find("thead th").length) - 1;
                        count_rowN[1] = parseInt($(foucs).find("thead th").length) - 2;
                        count_rowN[2] = parseInt($(foucs).find("thead th").length) - 3;
                        count_rowN[3] = parseInt($(foucs).find("thead th").length) - 4;
                        count_rowN[4] = parseInt($(foucs).find("thead th").length) - 5;
                        count_rowN[5] = parseInt($(foucs).find("thead th").length) - 6;
                        count_rowN[6] = parseInt($(foucs).find("thead th").length) - 7;
                        count_rowN[7] = parseInt($(foucs).find("thead th").length) - 8;
                        count_rowN[8] = parseInt($(foucs).find("thead th").length) - 9;
                        
                        
                        $.each( count_rowN , function( index , value ){
                            
                                    if( value == 11 )
                                    {
                                                // Total over all pages
                                                if( value )
                                                var total = api
                                                    .column( value )
                                                    .data()
                                                    .reduce( function (a, b) {
                                                        return intVal(a) + intVal( parseInt( b.split( "<a>" )[1].split( "</a>" )[0] ) );
                                                    }, 0 );

                                                // Total over foucs page
                                                if( value )
                                                var pageTotal = api
                                                    .column( value , { page: 'current'} )
                                                    .data()
                                                    .reduce( function (a, b) {
                                                        return intVal(a) + intVal( parseInt( b.split( "<a>" )[1].split( "</a>" )[0] ) );
                                                    }, 0 );


                                        
                                    }
                                    else{
                                                // Total over all pages
                                                if( value )
                                                var total = api
                                                    .column( value )
                                                    .data()
                                                    .reduce( function (a, b) {
                                                        return intVal(a) + intVal(b);
                                                    }, 0 );

                                                // Total over foucs page
                                                if( value )
                                                var pageTotal = api
                                                    .column( value , { page: 'current'} )
                                                    .data()
                                                    .reduce( function (a, b) {
                                                        return intVal(a) + intVal(b);
                                                    }, 0 );

                                    }

                                    var num = new Number(pageTotal);
                                    pageTotal = num.toFixed(2) ;

                                    count_rowN_output[ index ] = pageTotal ;

                        });
                        
                        var count_html = '';
                        
                        $.each( count_rowN , function( index , value ){
                                    foucs.find( "tfoot th:eq("+ value +")" ).html( count_rowN_output[ index ] );
                        });
                        
                }
        }
        
        else if( location.pathname.split( "mgm_operations_overview.php" ).length > 1 )
        {
                if( foucs.attr("id") === "example" ){
                        var count_row1 = parseInt($(foucs).find("thead th").length) - 2;
                        var count_row2 = parseInt($(foucs).find("thead th").length) - 3;
                        
                        // Total over all pages
                        if( count_row1 )
                        var total = api
                            .column( count_row1 )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );

                        // Total over foucs page
                        if( count_row1 )
                        var pageTotal = api
                            .column( count_row1, { page: 'current'} )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );


                        // Total over all pages
                        if( count_row2 )
                        var ctotal = api
                            .column( count_row2 )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );

                        // Total over foucs page
                        if( count_row2 )
                        var cpageTotal = api
                            .column( count_row2, { page: 'current'} )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );

                        var num = new Number(cpageTotal);
                        cpageTotal = num.toFixed(2) ;
                        var num = new Number(ctotal);
                        ctotal = num.toFixed(2) ;
                        var num = new Number(pageTotal);
                        pageTotal = num.toFixed(2) ;
                        var num = new Number(total);
                        total = num.toFixed(2) ;

    //                    var count_html = '本頁面 G 幣 ' + pageTotal + ' <br>( 總計 G 幣 ' + total + ' ) ' ;
                        var count_html = '';
                        foucs.find( "tfoot th:eq("+count_row1+")" ).html( '金額小計 ' + pageTotal );
                }
        }
        
        else if( location.pathname.split( "mgm_accessories_download_list.php" ).length > 1 )
        {
                    var count_row1 = parseInt($(foucs).find("thead th").length) - 2;
                    var count_row2 = parseInt($(foucs).find("thead th").length) - 3;
                    
                    // Total over all pages
                    if( count_row1 )
                    var total = api
                        .column( count_row1 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over foucs page
                    if( count_row1 )
                    var pageTotal = api
                        .column( count_row1, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );


                    // Total over all pages
                    if( count_row2 )
                    var ctotal = api
                        .column( count_row2 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over foucs page
                    if( count_row2 )
                    var cpageTotal = api
                        .column( count_row2, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    var num = new Number(cpageTotal);
                    cpageTotal = num.toFixed(2) ;
                    var num = new Number(ctotal);
                    ctotal = num.toFixed(2) ;
                    var num = new Number(pageTotal);
                    pageTotal = num.toFixed(2) ;
                    var num = new Number(total);
                    total = num.toFixed(2) ;

//                    var count_html = '本頁面 G 幣 ' + pageTotal + ' <br>( 總計 G 幣 ' + total + ' ) ' ;
                    var count_html = '';
                    // var count_html = '本頁面已驗證數 ' + cpageTotal + ' <br>( 總計已驗證數 ' + ctotal + ' ) <br>本頁面未驗證數 $'+pageTotal +' <br>( 總計未驗證數 $'+ total +' )' ;
                    foucs.find( "tfoot th:eq("+count_row1+")" ).html( '本頁面 G 幣 ' + pageTotal );
                    
        }
        
        else if( location.pathname.split( "mgm_articles_list_manage.php" ).length > 1 )
        {
                    var count_row1 = parseInt($(foucs).find("thead th").length) - 4;
                    var count_row2 = parseInt($(foucs).find("thead th").length) - 6;
                    
                    // Total over all pages
                    if( count_row1 )
                    var total = api
                        .column( count_row1 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over foucs page
                    if( count_row1 )
                    var pageTotal = api
                        .column( count_row1, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );


                    // Total over all pages
                    if( count_row2 )
                    var ctotal = api
                        .column( count_row2 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over foucs page
                    if( count_row2 )
                    var cpageTotal = api
                        .column( count_row2, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    var num = new Number(cpageTotal);
                    cpageTotal = num.toFixed(2) ;
                    var num = new Number(ctotal);
                    ctotal = num.toFixed(2) ;
                    var num = new Number(pageTotal);
                    pageTotal = num.toFixed(2) ;
                    var num = new Number(total);
                    total = num.toFixed(2) ;

//                    var count_html = '本頁面點閱數 ' + cpageTotal + ' <br>( 總計點閱數 ' + ctotal + ' ) <br>幫推收益金額 $'+pageTotal +' <br>( 總計金額 $'+ total +' )' ;
                    var count_html = '';
                    foucs.find( "tfoot th:eq("+count_row1+")" ).html( '幫推收益金額 $' + pageTotal );
                    foucs.find( "tfoot th:eq("+count_row2+")" ).html( '本頁面點閱數 ' + cpageTotal );
        }
        else if( location.pathname.split( "mgm_statistics_member_growUp.php" ).length > 1 )
        {
                    var count_row1 = parseInt($(foucs).find("thead th").length) - 2;
                    var count_row2 = parseInt($(foucs).find("thead th").length) - 3;
                    
                    // Total over all pages
                    if( count_row1 )
                    var total = api
                        .column( count_row1 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over foucs page
                    if( count_row1 )
                    var pageTotal = api
                        .column( count_row1, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );


                    // Total over all pages
                    if( count_row2 )
                    var ctotal = api
                        .column( count_row2 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over foucs page
                    if( count_row2 )
                    var cpageTotal = api
                        .column( count_row2, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    var num = new Number(cpageTotal);
                    cpageTotal = num ;
                    var num = new Number(ctotal);
                    ctotal = num.toFixed(2) ;
                    var num = new Number(pageTotal);
                    pageTotal = num ;
                    var num = new Number(total);
                    total = num.toFixed(2) ;

//                    var count_html = '本頁面已驗證數 ' + cpageTotal + ' <br>( 總計已驗證數 ' + ctotal + ' ) <br>本頁面未驗證數 $'+pageTotal +' <br>( 總計未驗證數 $'+ total +' )' ;
                    var count_html = '';
                    foucs.find( "tfoot th:eq("+count_row1+")" ).html( '本頁面未驗證數 ' + pageTotal );
                    foucs.find( "tfoot th:eq("+count_row2+")" ).html( '本頁面已驗證數 ' + cpageTotal );
        }
        
        else if( location.pathname.split( "mgm_statistics_network.php" ).length > 1 )
        {
                    var count_row1 = parseInt($(foucs).find("thead th").length) - 1;
                    var count_row2 = parseInt($(foucs).find("thead th").length) - 2;

                    // Total over all pages
                    if( count_row1 )
                    var total = api
                        .column( count_row1 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over foucs page
                    if( count_row1 )
                    var pageTotal = api
                        .column( count_row1, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );


                    // Total over all pages
                    if( count_row2 )
                    var ctotal = api
                        .column( count_row2 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over foucs page
                    if( count_row2 )
                    var cpageTotal = api
                        .column( count_row2, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    var num = new Number(cpageTotal);
                    cpageTotal = num ;
                    var num = new Number(ctotal);
                    ctotal = num.toFixed(2) ;
                    var num = new Number(pageTotal);
                    pageTotal = num ;
                    var num = new Number(total);
                    total = num.toFixed(2) ;
                    
//                    var count_html = '本頁面文章流量 ' + cpageTotal + ' <br>( 總計文章流量 ' + ctotal + ' ) <br>本頁面附件流量 $'+pageTotal +' <br>( 總計附件流量 $'+ total +' )' ;
                    var count_html = '';
                    foucs.find( "tfoot th:eq("+count_row1+")" ).html( '本頁面附件流量 ' + pageTotal );
                    foucs.find( "tfoot th:eq("+count_row2+")" ).html( '本頁面文章流量 ' + cpageTotal );
        }
        
        else if( location.pathname.split( "mgm_statistics_support.php" ).length > 1 )
        {
                if( foucs.attr("id") === "example2" ){
                        var count_row1 = parseInt($(foucs).find("thead th").length) - 2;
                        var count_row2 = parseInt($(foucs).find("thead th").length) - 3;

                        // Total over all pages
                        if( count_row1 )
                        var total = api
                            .column( count_row1 )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );

                        // Total over foucs page
                        if( count_row1 )
                        var pageTotal = api
                            .column( count_row1, { page: 'current'} )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );


                        // Total over all pages
                        if( count_row2 )
                        var ctotal = api
                            .column( count_row2 )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );

                        // Total over foucs page
                        if( count_row2 )
                        var cpageTotal = api
                            .column( count_row2, { page: 'current'} )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );

                        var num = new Number(cpageTotal);
                        cpageTotal = num ;
                        var num = new Number(ctotal);
                        ctotal = num.toFixed(2) ;
                        var num = new Number(pageTotal);
                        pageTotal = num.toFixed(2) ;
                        var num = new Number(total);
                        total = num.toFixed(2) ;

    //                    var count_html = '本頁面總計筆數 ' + cpageTotal + ' <br>( 總計筆數 ' + ctotal + ' ) <br>本頁面總計金額 $'+pageTotal +' <br>( 總計金額 $'+ total +' )' ;
                        var count_html = '';
                        foucs.find( "tfoot th:eq("+count_row1+")" ).html( '本頁面總計金額 $' + pageTotal );
                        foucs.find( "tfoot th:eq("+count_row2+")" ).html( '本頁面總計筆數 ' + cpageTotal );
                }
                else if( foucs.attr("id") === "example" ){
                        var count_row1 = parseInt($(foucs).find("thead th").length) - 2;

                        // Total over all pages
                        if( count_row1 )
                        var total = api
                            .column( count_row1 )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );

                        // Total over foucs page
                        if( count_row1 )
                        var pageTotal = api
                            .column( count_row1, { page: 'current'} )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );


                        // Total over all pages
                        if( count_row2 )
                        var ctotal = api
                            .column( count_row2 )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );

                        // Total over foucs page
                        if( count_row2 )
                        var cpageTotal = api
                            .column( count_row2, { page: 'current'} )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );

                        var num = new Number(cpageTotal);
                        cpageTotal = num.toFixed(2) ;
                        var num = new Number(ctotal);
                        ctotal = num.toFixed(2) ;
                        var num = new Number(pageTotal);
                        pageTotal = num ;
                        var num = new Number(total);
                        total = num.toFixed(2) ;

//                        var count_html = '本頁面訂單筆數 '+pageTotal +' <br>( 總計網址 $'+ total +' )' ;var count_html = '';
                        foucs.find( "tfoot th:eq("+count_row1+")" ).html( '本頁面訂單筆數 ' + pageTotal );
                }
                    
        }
        else if( location.pathname.split( "mgm_bonus_statistics.php" ).length > 1 )
        {
                var focus_id = foucs.attr("id");
                if( focus_id == "example" )
                {
                        foucs.find( "tfoot th:eq(0)" ).html( '合計: ' );
                        if( $( "#SD_bonusKind_checkbox" ).is( ":checked" ) ){
                            AppendTotal( foucs , api , 3 , 0 );
                        }
                        else{
                            AppendTotal( foucs , api , 2 , 0 );
                        }
                        AppendTotal( foucs , api , 1 , 0 );
                        var count_html = '';     
                }
                else if( focus_id == "example2" )
                {
                        foucs.find( "tfoot th:eq(0)" ).html( '合計: ' );
                        AppendTotal( foucs , api , 3 , 0 );
                        AppendTotal( foucs , api , 2 , 0 );
                        AppendTotal( foucs , api , 1 , 0 );
                        var count_html = '';     
                }
                    
        }
        else if( location.pathname.split( "mgm_interest_statistics.php" ).length > 1 )
        {
                var focus_id = foucs.attr("id");
                if( focus_id == "example" )
                {
                        foucs.find( "tfoot th:eq(0)" ).html( '合計: ' );
                        if( $( "#SD_interestKind_checkbox" ).is( ":checked" ) ){
                            AppendTotal( foucs , api , 3 , 0 );
                        }
                        else{
                            AppendTotal( foucs , api , 2 , 0 );
                        }
                        AppendTotal( foucs , api , 1 , 0 );
                        var count_html = '';     
                }
                else if( focus_id == "example2" )
                {
                        foucs.find( "tfoot th:eq(0)" ).html( '合計: ' );
                        AppendTotal( foucs , api , 3 , 0 );
                        AppendTotal( foucs , api , 2 , 0 );
                        AppendTotal( foucs , api , 1 , 0 );
                        var count_html = '';     
                }
                    
        }
        else
        {
            
                    var count_html = "" ;
        
        }

        $("#datatable_result").html( count_num );
        
        // Update footer
        if( count_html == "" )
        {
//                    $( "#datatable" ).find( "tfoot" ).remove();
        }else{
                    $(foucs).find("tfoot th:last").css( "color" , "#999" ).html( count_html );
                    // $( "#datatable_result" ).html( count_html );
        }
        
        
}

var AppendTotal = function( foucs , api , row , point )
{
        var intVal = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
        };
        var addintVal = function ( a , b ) {
            return a.search(/[(]/) !== -1 ?
                "("+(intVal(a.substr(1,a.length-2))+intVal(b.substr(1,b.length-2)))+")" : 
                intVal(a)+intVal(b);
        };
        
        var count_row1 = parseInt($(foucs).find("thead th").length) - row;
        // Total over foucs page
        if( count_row1 )
        var pageTotal = api
            .column( count_row1, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
                if( typeof b !== "number" && b.search("<a>") !== -1 ){
                    b=b.substr(3,b.length-7);
                }
                if( typeof b === "number" ){
                    return intVal(a) + intVal(b);
                }
                else if( b.search("/") !== -1 ){
                    if( a == 0 ){
                        return b;
                    }
                    else{
                        var tmp_a = a.split( "/" ) ;
                        var tmp_b = b.split( "/" ) ;
                        return ( intVal(tmp_a[0])+intVal(tmp_b[0]) ) + " / " + ( intVal(tmp_a[1])+intVal(tmp_b[1]) );
                    }
                }
                else if( b.search(/[(]/) !== -1 ){
                    if( a == 0 ){
                        return b;
                    }
                    else{
                        var tmp_a = a.split( "<br>" ) ;
                        var tmp_b = b.split( "<br>" ) ;
                        if( tmp_a.length === 2 ){
                            return ( addintVal(tmp_a[0],tmp_b[0]) ) + "<br>" + ( addintVal(tmp_a[1],tmp_b[1]) );
                        }
                        else if( tmp_a.length === 3 ){
                            return ( addintVal(tmp_a[0],tmp_b[0]) ) + "<br>" + ( addintVal(tmp_a[1],tmp_b[1]) ) + "<br>" + ( addintVal(tmp_a[2],tmp_b[2]) );
                        }
                    }
                }
                else{
                    return intVal(a) + intVal(b);
                }
            }, 0 );
        
        if( typeof pageTotal === "number" ){
            var num = new Number(pageTotal);
            pageTotal = num.toFixed(point);
        }
        foucs.find( "tfoot th:eq("+count_row1+")" ).html( pageTotal );
}


var fn_footerCallback_1 = function( right1 , right2 , table_id ) 
{
    
}


var addEvent = function(div_id, table_id) {
    var dataTable = $(table_id).DataTable();
    
    var updateDataTableSelectAllCtrl = function(table){
        var $table             = table.table().node();
        var $chkbox_all        = $('tbody input[type="checkbox"]', $table);
        var $chkbox_checked    = $('tbody input[type="checkbox"]:checked', $table);
        var chkbox_select_all    = $('thead input[name="select_all"]', $table).get(0);

        // If none of the checkboxes are checked
        if($chkbox_checked.length === 0){
           chkbox_select_all.checked = false;
           if('indeterminate' in chkbox_select_all){
              chkbox_select_all.indeterminate = false;
           }

        // If all of the checkboxes are checked
        } else if ($chkbox_checked.length === $chkbox_all.length){
           chkbox_select_all.checked = true;
           if('indeterminate' in chkbox_select_all){
              chkbox_select_all.indeterminate = false;
           }

        // If some of the checkboxes are checked
        } else {
           chkbox_select_all.checked = true;
           if('indeterminate' in chkbox_select_all){
              chkbox_select_all.indeterminate = true;
           }
        }
    }

    var filterGlobal = function() {
            $(table_id).DataTable().search( 
                    $(div_id).find('#global_filter').val(),
                    $(div_id).find('#global_regex').prop('checked'), 
                    $(div_id).find('#global_smart').prop('checked')
            ).draw();
    }

    var filterColumn = function(i) {
        console.log(table_id);
            $(table_id).DataTable().column( i ).search( 
                    $(table_id+"_search").find('#col'+i+'_filter').val(),
                    $(table_id+"_search").find('#col'+i+'_regex').prop('checked'), 
                    $(table_id+"_search").find('#col'+i+'_smart').prop('checked')
            ).draw();
    }
    
    $(table_id+"_search").find('input.global_filter').on( 'keyup click', function () {
            filterGlobal();
    });
    
    $(table_id+"_search").find('input.column_filter').on( 'keyup click', function () {
            filterColumn( $(this).parents('tr').attr('data-column') );
            // filterColumn( ( parseInt( $(this).parents('tr').attr('data-column') ) - 1 ).toString() );
    });

    // Handle click on table cells with checkboxes
    $(div_id).on('click', 'tbody td, thead th:first-child', function(e){
       $(this).parent().find('input[type="checkbox"]').trigger('click');
    });

    // Handle click on "Select all" control
    $(div_id).find('thead input[name="select_all"]', dataTable.table().container()).on('click', function(e){
        if(this.checked){
            $(div_id).find('tbody input[type="checkbox"]:not(:checked)').trigger('click');
        } else {
            $(div_id).find('tbody input[type="checkbox"]:checked').trigger('click');
        }
        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle table draw event
    dataTable.on('draw', function(){
        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(dataTable);
    });

    // Handle form submission event 
    $('#frm-example').unbind('click').bind('click', function(e){
        console.log( $(table_id).data("rows_selected") );
    });

    // Handle click on checkbox
    $(div_id).find('tbody').on('click', 'input[type="checkbox"]', function(e){
       var $row = $(this).closest('tr');

       // Get row data
       var data = dataTable.row($row).data();

       // Get row ID
       var rowId = data[0];

       // Determine whether row ID is in the list of selected row IDs 
       var index = $.inArray(rowId, $(table_id).data("rows_selected"));

       // If checkbox is checked and row ID is not in list of selected row IDs
       if(this.checked && index === -1){
          $(table_id).data("rows_selected").push(rowId);

       // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
       } else if (!this.checked && index !== -1){
          $(table_id).data("rows_selected").splice(index, 1);
       }

       if(this.checked){
          $row.addClass('selected');
       } else {
          $row.removeClass('selected');
       }

       // Update state of "Select all" control
       updateDataTableSelectAllCtrl(dataTable);

       // Prevent click event from propagating to parent
       e.stopPropagation();
    });    
}

var plug_in_dataTable_reload = function() {
        jQuery.fn.dataTableExt.oApi.fnReloadAjax = function ( oSettings, sNewSource, fnCallback, bStandingRedraw )
        {
            // DataTables 1.10 compatibility - if 1.10 then `versionCheck` exists.
            // 1.10's API has ajax reloading built in, so we use those abilities
            // directly.
            if ( jQuery.fn.dataTable.versionCheck ) {
                var api = new jQuery.fn.dataTable.Api( oSettings );

                if ( sNewSource ) {
                    api.ajax.url( sNewSource ).load( fnCallback, !bStandingRedraw );
                }
                else {
                    api.ajax.reload( fnCallback, !bStandingRedraw );
                }
                return;
            }

            if ( sNewSource !== undefined && sNewSource !== null ) {
                oSettings.sAjaxSource = sNewSource;
            }

            // Server-side processing should just call fnDraw
            if ( oSettings.oFeatures.bServerSide ) {
                this.fnDraw();
                return;
            }

            this.oApi._fnProcessingDisplay( oSettings, true );
            var that = this;
            var iStart = oSettings._iDisplayStart;
            var aData = [];

            this.oApi._fnServerParams( oSettings, aData );

            oSettings.fnServerData.call( oSettings.oInstance, oSettings.sAjaxSource, aData, function(json) {
                /* Clear the old information from the table */
                that.oApi._fnClearTable( oSettings );

                /* Got the data - add it to the table */
                var aData =  (oSettings.sAjaxDataProp !== "") ?
                    that.oApi._fnGetObjectDataFn( oSettings.sAjaxDataProp )( json ) : json;

                for ( var i=0 ; i<aData.length ; i++ )
                {
                    that.oApi._fnAddData( oSettings, aData[i] );
                }

                oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();

                that.fnDraw();

                if ( bStandingRedraw === true )
                {
                    oSettings._iDisplayStart = iStart;
                    that.oApi._fnCalculateEnd( oSettings );
                    that.fnDraw( false );
                }

                that.oApi._fnProcessingDisplay( oSettings, false );

                /* Callback user function - for event handlers etc */
                if ( typeof fnCallback == 'function' && fnCallback !== null )
                {
                    fnCallback( oSettings );
                }
            }, oSettings );
        };
}
