/*
 * edit 2016/4/8 jack & abin
 */
function filterGlobal () {
        $('#example').DataTable().search( 
                $('#global_filter').val(),
                $('#global_regex').prop('checked'), 
                $('#global_smart').prop('checked')
        ).draw();
}

function filterColumn ( i ) {
        $('#example').DataTable().column( i ).search( 
                $('#col'+i+'_filter').val(),
                $('#col'+i+'_regex').prop('checked'), 
                $('#col'+i+'_smart').prop('checked')
        ).draw();
}

function updateDataTableSelectAllCtrl(table){
   var $table             = table.table().node();
   var $chkbox_all        = $('tbody input[type="checkbox"]', $table);
   var $chkbox_checked    = $('tbody input[type="checkbox"]:checked', $table);
   var chkbox_select_all  = $('thead input[name="select_all"]', $table).get(0);

   // If none of the checkboxes are checked
   if($chkbox_checked.length === 0){
      chkbox_select_all.checked = false;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = false;
      }

   // If all of the checkboxes are checked
   } else if ($chkbox_checked.length === $chkbox_all.length){
      chkbox_select_all.checked = true;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = false;
      }

   // If some of the checkboxes are checked
   } else {
      chkbox_select_all.checked = true;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = true;
      }
   }
}

/* 產生dataTable的基本樣式
 * @param jqObject target append目標
 * @param array column 欄位名稱
 */
var initDataTable = function(target, column) {
    var th = "";
    for(var i=0;i<column.length;i++) {
        th += '<th>'+column[i]+'</th>';
    }
    var table = '<table id="example" class="display select" width="100%" cellspacing="0">\n\
                    <thead>\n\
                        <tr>\n\
                            <th><input name="select_all" value="1" type="checkbox"></th>'
                            + th +
                        '</tr>\n\
                    </thead>\n\
                </table>';
    target.append(table);
}
/* 產生dataTable的搜尋功能
 * @param jqObject target append目標
 * @param array column 欄位名稱
 */
var initSearchTable = function(target, column) {
    var tr = "";
    for(var i=0;i<column.length-1;i++) {
        tr += '<tr id="filter_col'+(i+2)+'" data-column="'+(i+1)+'">'
                +'<td>'+column[i]+'</td>\n\
                <td align="left">\n\
			<input type="text" class="column_filter" id="col'+(i+1)+'_filter">\n\
		</td>\n\
            <tr>';
    }
    // 20160409 AL
    var table = '<table id="search_table" cellpadding="3" cellspacing="0" border="0" style="width: 50%; margin: 2em auto; display: none;" >\n\
                    <tbody>'
                        + tr +
                    '</tbody>\n\
                </table>';
    target.append(table);
}
