<?php
/**
 * This is an example of Activity Class using PDO
 */
namespace controllers\User;
use models\CommonFunction;
use models\User\User;
use lib\Ui\DataTable;

class userController {
	
	private $app;

	function __construct($app) {
		$this->app = $app;
	}
	
	public function getAll() {
		$comFun = new CommonFunction();
		return json_encode($comFun->getTableData("userinfo"));
	}
	
	public function getAllToDataTable() {
		$comFun = new CommonFunction();
		return json_encode(array(
				"data" => $comFun->getTableDataToDataTable("userinfo")
		));
	}
	
	public function belongsToRole($role) {
                $data = array();
                $request = $this->app->request();
                $token = $request->post('token');
        
		$Member = new Member ();
		$members = $Member->getMemberByToken($token);
                if(!$members["success"]){
                    return false;
                }
                if($role!==$members["data"]["userinfo_InExUser"]){
                    return false;
                }
//		return json_encode($members);
                return true;
	}
	
	public function getByToken() {
        $data = array();
        $request = $this->app->request();
        $token = $request->get('token');
        
		$User = new User ();
		$users = $User->getUserByToken($token);
		$this->app->contentType('application/json');
		return json_encode($users);
	}
	
	public function add() {
                $request = $this->app->request();
                $data["userinfo_UserName"] = $request->post('userinfo_UserName');
                $data["userinfo_Password"] = $request->post('userinfo_Password');
                $data["userinfo_Email"] = $request->post('userinfo_Email');
		
		$User = new User();
		return json_encode($User->insertUser($data));
	}
	
	public function login() {
                $request = $this->app->request();
                $data["userinfo_UserName"] = $request->get('userinfo_UserName');
                $data["userinfo_Password"] = $request->get('userinfo_Password');
		
		$User = new User();
		return json_encode($User->UserLogin($data));
	}
	public function getUserToDataTable() {
		$User = new User ();
		$users = $User->getUsersToDataTable();
		$json = array(
			"data" => $users
		);

		return json_encode($json);
	}
	public function buildEmployee($User_data) {
                $request = $this->app->request();
                $data["employeeinfo_FirstName"] = $request->post('employeeinfo_FirstName');
                $data["employeeinfo_MiddleName"] = $request->post('employeeinfo_MiddleName');
                $data["employeeinfo_LastName"] = $request->post('employeeinfo_LastName');
		$User = new User();
		return json_encode($User->buildEmployee($data,$User_data));
	}
        
	public function getEmployeeToDataTable() {
		$User = new User ();
		$Users = $User->getEmployeeToDataTable();
		$json = array(
			"data" => $Users
		);

		return json_encode($json);
	}
        
        
        
        
	public function put() {
		return "user PUT route";
	}

	public function delete() {
		return "user DELETE route";
	}
}
?>