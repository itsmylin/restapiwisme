<?php
namespace controllers\WorkOrder;
use models\CommonFunction;

class workorderController {
	
	private $app;

	function __construct($app) {
		$this->app = $app;
	}
	
	public function getAll() {
		$comFun = new CommonFunction();
		return json_encode($comFun->getTableData("workorder"));
	}
	
	public function getAllToDataTable() {
		$comFun = new CommonFunction();
		return json_encode(array(
				"data" => $comFun->getTableDataToDataTable("workorder")
		));
	}
}
?>