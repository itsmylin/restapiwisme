﻿<?php

use models\Jobsite\Jobsite;

$app->get('/jobsite', function () use ($app) {
	echo "jobsite";
});

$app->get('/jobsite/getById', function () use ($app) {
	echo "jobsite getById";
});

$app->post('/jobsite/add', function () use ($app) {
	echo "jobsite add";
});

$app->put('/jobsite', function () {
	echo 'jobsite PUT route';
});

$app->delete('/jobsite', function () {
	echo 'jobsite DELETE route';
});