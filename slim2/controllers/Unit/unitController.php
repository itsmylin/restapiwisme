﻿<?php

use models\Unit\Unit;

$app->get('/unit', function () use ($app) {
	echo "unit";
});

$app->get('/unit/getById', function () use ($app) {
	echo "unit getById";
});

$app->post('/unit/add', function () use ($app) {
	echo "unit add";
});

$app->put('/unit', function () {
	echo 'unit PUT route';
});

$app->delete('/unit', function () {
	echo 'unit DELETE route';
});