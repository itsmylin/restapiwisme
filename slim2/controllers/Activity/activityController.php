<?php
/**
 * This is an example of Activity Class using PDO
 */
namespace controllers\Activity;
use models\Activity\Activity;

class activityController {
	
	private $app;

	function __construct($app) {
		$this->app = $app;
	}
	
	public function getAll() {
		return "activity";
	}
	
	public function getById() {
		return "activity getById";
	}
	
	public function add() {
		return "activity add";
	}

	public function put() {
		return "activity PUT route";
	}

	public function delete() {
		return "activity DELETE route";
	}
}
?>