﻿<?php

use models\Customer\Customer;

$app->get('/customer', function () use ($app) {
	echo "customer";
});

$app->get('/customer/getById', function () use ($app) {
	echo "customer getById";
});

$app->post('/customer/add', function () use ($app) {
	echo "customer add";
});

$app->put('/customer', function () {
	echo 'customer PUT route';
});

$app->delete('/customer', function () {
	echo 'customer DELETE route';
});