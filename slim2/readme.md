SlimMVC
=======

SlimMVC is the easiest and flexible way to create your PHP application using a MVC pattern.
SlimMVC use the PHP microframework [Slim Framework](http://www.slimframework.com/) and use the best practices collected in the slim community.

Getting Started
---------------
1. Get or download the project
2. Install it using Composer

composer安裝指令
---------------
1. composer install (安裝lib)
2. composer dump-autoload -o (載入自定義的class)

此架構參考: https://github.com/revuls/SlimMVC

目錄結構
---------------
* lib/ (存放函式庫)
* models/ (對table操作的class)
* controllers/RESTfulAPI)

* vendor/(php composer依照composer.json的設定，去下載相依的php函式庫)
* composer.json(所需的相依檔案)
* config.php(相關config的設定)

* PS: composer.json 有新增auto load目錄，需cmd指令下: composer dump-autoload -o

相關學習網站
---------------
* Namespaces 與 Class Autoloading: http://ithelp.ithome.com.tw/articles/10134247
* 范围解析操作符"::" : http://php.net/manual/zh/language.oop5.paamayim-nekudotayim.php
* PHP 區別 public private protected: http://kikinote.net/article/1651.html
* self和$this的差異: http://miggo.pixnet.net/blog/post/30799978-%5Bphp%E8%A7%80%E5%BF%B5%5Dself%E5%92%8C$this%E7%9A%84%E5%B7%AE%E7%95%B0
* 匿名函數 / Closure / Callable: http://ithelp.ithome.com.tw/articles/10132747
* Callbacks / Callables: http://www.php.net/manual/en/language.types.callable.php