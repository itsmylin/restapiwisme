<?php
require __DIR__.'/vendor/autoload.php';
require __DIR__.'/config.php';

//Slim 一開始的宣告
$app = new \Slim\Slim(array(
    'debug' => true
));

require __DIR__.'/routers.php';
//require __DIR__.'/controllers/indexController.php';
//載入此floder下的所有檔案
//$controllers = glob(__DIR__.'/controllers/*/*Controller.php');
//foreach ($controllers as $controller) {
//    require $controller;
//}

$app->run();
