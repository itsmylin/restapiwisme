<?php
/**
 * This is an example of Customer Class using PDO
 */
namespace models\Customer;
use lib\Core;
use PDO;

class Customer {
	protected $core;
	function __construct() {
		$this->core = Core::getInstance();
	}
	
	public function insertCustomer($data) {
            
	}

	public function getCustomerByToken($token) {
        
	}
        
	/**
	 *
	 * @retirm [object, object...] 
	 */
	public function getCustomers() {
		$r = array();		
		$sql = "SELECT * FROM customer";
		$stmt = $this->core->dbh->prepare($sql);
		//$stmt->bindParam(':id', $this->id, PDO::PARAM_INT);
		if ($stmt->execute()) {
			$r = $stmt->fetchAll(PDO::FETCH_ASSOC);		   	
		} else {
			$r = 0;
		}		
		return $r;
	}
	// Update the data of an customer
	public function updateCustomer($data) {
		
	}
	// Delete customer
	public function deleteCustomer($id) {
		
	}
}

?>