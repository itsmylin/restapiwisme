<?php
/**
 * This is an example of Project Class using PDO
 */
namespace models;
use lib\Core;
use PDO;

class CommonFunction {
	protected $core;
	function __construct() {
		$this->core = Core::getInstance();
	}
	
	/**
	 * 取得指定資料表資料
	 * @param string $table 資料表名稱
	 * @retirm [object, object...] 資料表資料
	 */
	public function getTableData($table) {
		$r = array();		
		$sql = "SELECT * FROM $table";
		$stmt = $this->core->dbh->prepare($sql);
		if ($stmt->execute()) {
			$r = $stmt->fetchAll(PDO::FETCH_ASSOC);
		} else {
			$r = 0;
		}		
		return $r;
	}
	
	/**
	 * 取得指定資料表資料並轉成dataTable格式
	 * @param string $table 資料表名稱
	 * @retirm [[rowData], [rowData]...] 資料表資料
	 */
	public function getTableDataToDataTable($table) {
		$r = array();		
		$sql = "SELECT * FROM $table";
		$stmt = $this->core->dbh->prepare($sql);
		if ($stmt->execute()) {
			$r = $stmt->fetchAll(PDO::FETCH_NUM);
		} else {
			$r = 0;
		}		
		return $r;
	}
}

?>