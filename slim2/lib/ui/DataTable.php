<?php
namespace lib\Ui;

class DataTable {

    function __construct() {
    }

    /**
     * 將PDO fetchAll的資料轉JQuery DataTable格式
     * @param array $data [rowData, rowData]
     */
    public static function arrayToDataTable(Array $data = array()) {
		for($i=0;$i<count($data);$i++) {
			$toArray = array();
			foreach ($data[$i] as $key => $value) {
				$toArray[] = $value;
			}
			$data[$i] = $toArray;
		}
		$tableData = array(
			"data" => $data
		);
		return $tableData;
	}
}
