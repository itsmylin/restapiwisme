<?php

//autoload名稱定義

namespace lib;

//auto load自動載入
use lib\Config;
use PDO;

class Core {

    public $dbh; // handle of the db connexion
    private static $instance;

    /**
     * 建構子(DB設定)
     */
    private function __construct() {
        // building data source name from config
        $dsn = 'mysql:host=' . Config::read('db.host') .
                ';dbname=' . Config::read('db.basename') .
                ';port=' . Config::read('db.port') .
                ';connect_timeout=15';
        // getting DB user from config                
        $user = Config::read('db.user');
        // getting DB password from config                
        $password = Config::read('db.password');
        $this->dbh = new PDO($dsn, $user, $password);
        $this->dbh->exec("set names utf8");
    }

    /**
     * 賦予靜態val此物件值，類似 new Core();
     * Singleton, reference: https://en.wikipedia.org/wiki/Singleton_pattern
     */
    public static function getInstance() {
        if (!isset(self::$instance)) {
            $object = __CLASS__;
            self::$instance = new $object;
        }
        return self::$instance;
    }

    /**
     * SELECT $objectsFormat FROM $table WHERE $conditionArray AND $conditionString
     * 
     * @paarm $table Table Name
     * @param $conditionArray Condition Dictionary, (key, value) pairs 
     * @param $conditionString Condition String, default ""
     * @param $objectsFormat Select Objects String, default "*"
     * 
     * @return False or Data
     */
    public function select($table, $conditionArray = array(), $conditionString = "", $objectsFormat = '*') {
        $condition = "";
        // Loop throw condition array
        foreach ($conditionArray as $key => $value) {
            if (gettype($value) === "array") {
                $condition .= $key . "" . $value["type"];
                $value = $value["value"];
            } else {
                $condition .= $key . "=";
            }
            if (gettype($value) == "NULL") {
                $condition .= "'NULL' AND ";
            } else if (gettype($value) == "integer") {
                $condition .= $value . " AND ";
            } else if ($value == "") {
                $condition .= "'' AND ";
            } else {
                $condition .= "'" . $value . "' AND ";
            }
        }
        // Remove extra AND
        if (strlen($condition)) {
            $condition = substr($condition, 0, -4);
        }
        // Append ConditionString
        if (strlen($conditionString)) {
            if (strlen($condition)) {
                $condition .= " AND " . $conditionString;
            } else {
                $condition = $conditionString;
            }
        }
        // Construct SQL
        $sql = "SELECT $objectsFormat FROM $table";
        if (strlen($condition)) {
            $sql .= "WHERE " . $condition;
        }
        return $this->execute($sql);
    }

    /**
     * Execute SQL Query
     * 
     * @param type $sql
     * @return false or Data
     */
    private function execute($sql) {
        $stmt = $this->dbh->prepare($sql);
        if ($stmt->execute()) {
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $f_data = array_filter($data);
            if (!empty($f_data)) {
                return $data;
            }
        }
        return FALSE;
    }

    public function selectMysql($table, $Condition_arr = array(), $Condition2 = "", $SELECT = '*') {
//            $return = array();
//            $i = 0;
        $Condition = "";
//        print_r(print$Condition_arr);
        if (!empty($Condition_arr)) {
            $Condition = "WHERE ";
            foreach ($Condition_arr as $key => $value) {
                if (gettype($value) === "array") {
                    $Condition .= $key . "" . $value["type"];
                    $value = $value["value"];
                } else {
                    $Condition .= $key . "=";
                }
                if (gettype($value) == "NULL") {
                    $Condition .= "'NULL' AND ";
                } else if (gettype($value) == "integer") {
                    $Condition .= $value . " AND ";
                } else if ($value == "") {
                    $Condition .= "'' AND ";
                } else {
                    $Condition .= "'" . $value . "' AND ";
                }
            }
            $Condition = substr($Condition, 0, -4);
        }
        $sql = "SELECT $SELECT FROM $table $Condition $Condition2";
        return $this->execute($sql);
//        $stmt = $this->dbh->prepare($sql);
//        if ($stmt->execute()) {
//            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
//            $f_data = array_filter($data);
//            if (empty($f_data)) {
////                        $callback["success"]=false;
////                        $callback["msg"]="search empty";
//                return FALSE;
//            }
//            return $data;
//        } else {
////                    $callback["success"]=false;
////                    $callback["msg"]="cmd error";
//            return FALSE;
//        }
    }

}
