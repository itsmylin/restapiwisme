<?php
        /*
        * @file json_mgm_video.php
        * @brief TABLE: page

        * detail 

        * @author arod ( howareu520@gmail.com )
        * @date 2016-07-28 */

        include '../../php/config.php';
        include '../../php/global.php';
        
        $func = $_REQUEST["func"];

        switch ($func) {
            case "fn_read_regex":
                $echo = fn_read_regex();
                break;
            case "fn_read_singleLog":
                $echo = fn_read_singleLog();
                break;
            case "fn_read_category_mainCate"://主類別下拉選單
                $echo = fn_read_category_mainCate();
                break;
            case "fn_read_category_subCate"://子類別下拉選單
                $echo = fn_read_category_subCate();
                break;
            case "fn_create_update_video":
                $echo = fn_create_update_video();
                break;
            case "fn_delete":
                $echo = fn_delete();
                break;
            case "fn_update_page_block_blockade"://上下架
                $echo = fn_update_page_block_blockade();
                break;
            case "fn_notification"://推播
                $echo = fn_notification();
                break;
        }

    function fn_read_regex(){
        
        try{    
                $callback = array();
                $cart = array();
                
                $operation_html_block = empty($_REQUEST[ "operation_html_block" ]) ? "" : $_REQUEST["operation_html_block"];
                $operation_html_blockade = empty($_REQUEST[ "operation_html_blockade" ]) ? "" : $_REQUEST["operation_html_blockade"];
                $operation_html_none = empty($_REQUEST[ "operation_html_none" ]) ? "" : $_REQUEST["operation_html_none"];
                
                /*
                if( !check_empty( array("token" ) ) ) {
                        $callback['msg'] = "輸入資料不完整";
                        $callback['success'] = false;
                        echo json_encode($callback);
                        return;
                }
                
                $token = md5( $_REQUEST[ "token" ] );*/
                
                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");
                // Check connection
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        return $callback;
                }
                
                /*
                $account = get_sql($con, TABLE_ACCOUNT, "WHERE a_token LIKE '%\\\"$token\\\"%'");
                if( !$account ) {
                        $callback['msg'] = "Login fail";
                        $callback['success'] = false;
                        mysqli_close($con);
                        echo json_encode($callback);
                        return;
                }
                if( $account[0]['a_admin'] !== "true" ){
                        $callback['msg'] = "you dont have admin";
                        $callback['success'] = false;
                        mysqli_close($con);
                        echo json_encode($callback);
                        return;
                }*/
                
                //搜尋： 開始日期, 結束日期, 主分類, 子分類, 標題, 狀態(上下架)
                $condition = "";
                if( !empty($_REQUEST[ "startTime" ]) ){//搜尋：開始日期
                        $startTime = date( 'Y-m-d H:i:s', $_REQUEST[ "startTime" ]/1000);
                        $condition .= ($condition == "")? "WHERE p.p_date >= '".$startTime."'" : " AND p.p_date >= '".$startTime."' " ;
                }
                if( !empty($_REQUEST[ "endTime" ]) ){//搜尋：結束日期
                        $endTime = date( 'Y-m-d H:i:s', ($_REQUEST[ "endTime" ]/1000)+86400);
                        $condition .= ($condition == "")? "WHERE p.p_date <= '".$endTime."'" : " AND p.p_date <= '".$endTime."' " ;
                }
                if( !empty($_REQUEST[ "mainCate" ]) ){//搜尋：主分類
                        $condition .= ($condition == "")? "WHERE p.p_main_category_id = ".$_REQUEST[ "mainCate" ] : " AND p.p_main_category_id = ".$_REQUEST[ "mainCate" ]." ";
                }
                if( !empty($_REQUEST[ "subCate" ]) ){//搜尋：子分類
                        $condition .= ($condition == "")? "WHERE p.p_sub_category_id = ".$_REQUEST[ "subCate" ] : " AND p.p_sub_category_id = ".$_REQUEST[ "subCate" ]." ";
                }
                if( !empty($_REQUEST[ "title" ]) ){//搜尋：標題
                        $condition .= ($condition == "")? "WHERE p.p_title like '%".$_REQUEST[ "title" ]."%'" : " AND p.p_title like '%".$_REQUEST[ "title" ]."%' ";
                }
                if( !empty($_REQUEST[ "display" ]) ){//搜尋：狀態(上下架)
                        $condition .= ($condition == "")? "WHERE p.p_display = '".$_REQUEST[ "display" ]."'" : " AND p.p_display = '".$_REQUEST[ "display" ]."' " ;
                }
                                
                //get_sql_MYSQLI_NUM( $con , $table , $Condition = "" , $SELECT = '*' );
                //$cart = get_sql_MYSQLI_NUM( $con , "page as p LEFT JOIN category as c_main on c_main.cate_id=p.p_main_category_id LEFT JOIN category as c_sub on c_sub.cate_id=p.p_sub_category_id" , "ORDER BY p.date DESC" , "p.page_id, p.p_title, c_main.cate_name, c_sub.cate_name, p.p_click_num, p.p_date" );
                
                $res = mysqli_query($con, "SELECT p.page_id, p.p_title, c_main.cate_name `cate_main`, c_sub.cate_name `cate_sub`, p.p_click_num, p.p_share_num, p.p_date, p.p_display, pn.count, pn.lastest "
                                        . "FROM page as p "
                                        . "LEFT JOIN category as c_main on c_main.cate_id=p.p_main_category_id "
                                        . "LEFT JOIN category as c_sub on c_sub.cate_id=p.p_sub_category_id "
                                        . "LEFT JOIN (SELECT page_id, COUNT(*) `count`, MAX(pn_timestamp) `lastest` FROM page_notification GROUP BY page_id) as pn on pn.page_id=p.page_id "
                                        . $condition
                                        . "ORDER BY p.p_date DESC");

                if (mysqli_num_rows($res) > 0) {
                        while($row = mysqli_fetch_array($res)) {
                            
                                if($row['p_display'] == "block") { $operation_html = $operation_html_block; }
                                else if($row['p_display'] == "blockade") { $operation_html = $operation_html_blockade; }
                                else if($row['p_display'] == "none") { $operation_html = $operation_html_none; }
                                
                                $pn_datetime = '未曾推播';
                                if ($row['lastest']){
                                        $pn_datetime = date( 'Y-m-d H:i:s' , $row['lastest'] );
                                }
                                
                                $cart[] = array(
                                    "0" => $row['page_id'],
                                    "1" => $row['p_display']=="block"? '<a target="_blank" href="../?p='.$row['page_id'].'">'.$row['p_title'].'</a>' : $row['p_title'] ,
                                    "2" => $row['cate_main'],
                                    "3" => $row['cate_sub'],
                                    "4" => $row['p_click_num'],
                                    "5" => $row['p_share_num'], 
                                    "6" => $row['p_date'],
                                    "7" => $pn_datetime,
                                    "8" => $row['count']? $row['count'] : 0 ,
                                    "9" => stripslashes($operation_html)
                                );
                        }

                        $callback['data'] =  $cart;

                } else {
                        $callback['data'] =  array();
                }
                mysqli_close($con);
                
        }
        catch (Exception $e)
        {
                echo "false";
        }
        
        echo json_encode($callback);
    }

    function fn_read_singleLog(){
        try{
                $callback = array();
                $cart = array();
                
                if( !check_empty( array( "page_id" ) ) ){// "token" ,
                        $callback['msg'] = "輸入資料不完整";
                        $callback['success'] = false;
                        echo json_encode($callback);
                        return;
                }
                
                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");

                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        echo json_encode($callback);
                        return;
                }

//              $token = md5( $_REQUEST[ "token" ] );
                
                $page_id = $_REQUEST[ 'page_id' ];

//                $account = get_sql($con, "account" , "WHERE a_token LIKE '%\\\"$token\\\"%'");
//                if( !$account ) {
//                        $callback['msg'] = "Login fail";
//                        $callback['success'] = false;
//                        mysqli_close($con);
//                        echo json_encode($callback);
//                        return;
//                }
//                if( $account[0]['a_admin'] !== "true" ){
//                        $callback['msg'] = "you dont have admin";
//                        $callback['success'] = false;
//                        mysqli_close($con);
//                        echo json_encode($callback);
//                        return;
//                }

                $res = mysqli_query($con, "SELECT p.page_id, p.p_id, p.p_icon, p.p_title, p.p_click_num, p.p_share_num, p.p_main_category_id, p.p_sub_category_id, p.p_pre_chn, p.p_pre_eng, p.p_back_chn, p.p_back_eng, p.p_date "
                                        . "FROM page as p "
                                        . "WHERE p.page_id=$page_id");

                while($row = mysqli_fetch_array($res)) {

                        if($row['p_icon']){
                            $p_icon = "./data/img/". $row["p_icon"];
                        } else {
                            $p_icon = "";
                        }

                        $cart = array( "page_id" => $row["page_id"] ,
                                       "p_id" => $row["p_id"] ,
                                       "p_title" => $row["p_title"] ,
                                       "p_click_num" => $row["p_click_num"] ,
                                       "p_share_num" => $row["p_share_num"] ,
                                       "p_main_category_id" => $row["p_main_category_id"] ,
                                       "p_sub_category_id" => $row["p_sub_category_id"] ,
                                       "p_pre_chn" => $row["p_pre_chn"] ,
                                       "p_pre_eng" => $row["p_pre_eng"] ,
                                       "p_back_chn" => $row["p_back_chn"] ,
                                       "p_back_eng" => $row["p_back_eng"] ,
                                       "p_date" => $row["p_date"]
                        );
                }

                $callback['data'] =  $cart;
                $callback['success'] = true;

                mysqli_close($con);

        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        echo json_encode($callback);
    
    }

    function fn_read_category_mainCate(){
        
        try{    
                $callback = array();
                $cart = array();
                                
                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");
                // Check connection
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        return $callback;
                }
                                
                $mainCate = get_sql_MYSQLI_NUM( $con , "category" , "WHERE cate_parent=0 ORDER BY cate_order ASC" , "cate_id, cate_name" );
                if ( $mainCate ){
                        $callback['data'] = $mainCate;
                        $callback['success'] =  true;
                } else {
                        $callback['data'] = array();
                        $callback['success'] =  false;
                }

                mysqli_close($con);
                
        }
        catch (Exception $e)
        {
                echo "false";
        }
        
        echo json_encode($callback);
    }

    function fn_read_category_subCate(){
        
        try{    
                $callback = array();
                $cart = array();
                                
                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");
                // Check connection
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        return $callback;
                }
                
                if( !empty($_REQUEST[ "mainCate" ]) ){
                        $category = $_REQUEST[ "mainCate" ];
                        $subCate = get_sql_MYSQLI_NUM( $con , "category" , "WHERE cate_parent=$category ORDER BY cate_order ASC" , "cate_id, cate_name" );
//                        print_r($subCate);
                        if ( $subCate ){
                                $callback['data'] = $subCate;
                                $callback['success'] =  true;
                        } else {
                                $callback['data'] = array();
                                $callback['success'] =  true;
                        }
                } else {
                        $callback['data'] = array();
                        $callback['success'] =  true;
                }
                
                mysqli_close($con);
                
        }
        catch (Exception $e)
        {
                echo "false";
        }
        
        echo json_encode($callback);
    }

    function fn_create_update_video(){
        try{
                $callback = array();
                $page_id = "";
                                            
                if( !check_empty( array( "p_id" , "p_title" , "mainCate" , "subCate" ) ) ){// "token" ,
                        $callback['msg'] = "輸入資料不完整";
                        $callback['success'] = false;
                        echo json_encode($callback);
                        return;
                }
                
                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");
                
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        echo json_encode($callback);
                        return;
                }
                
                if( !empty($_REQUEST[ "page_id" ]) ){//搜尋：狀態(上下架)
                        $page_id = mysqli_real_escape_string($con,$_REQUEST['page_id']);
                }
                
                $p_id = mysqli_real_escape_string($con,$_REQUEST['p_id']);//youtube影片
                $p_title = mysqli_real_escape_string($con,$_REQUEST['p_title']);
                $mainCate = mysqli_real_escape_string($con,$_REQUEST['mainCate']);
                $subCate = mysqli_real_escape_string($con,$_REQUEST['subCate']);
                $p_pre_chn = mysqli_real_escape_string($con,$_REQUEST['p_pre_chn']);
                $p_pre_eng = mysqli_real_escape_string($con,$_REQUEST['p_pre_eng']);
                $p_back_chn = mysqli_real_escape_string($con,$_REQUEST['p_back_chn']);
                $p_back_eng = mysqli_real_escape_string($con,$_REQUEST['p_back_eng']);
                
//                $token = md5( $_REQUEST[ "token" ] );
                                                
//                $account = get_sql($con, "account" , "WHERE a_token LIKE '%\\\"$token\\\"%'");
//                if( !$account ) {
//                        $callback['msg'] = "Login fail";
//                        $callback['success'] = false;
//                        mysqli_close($con);
//                        echo json_encode($callback);
//                        return;
//                }
//                if( $account[0]['a_admin'] !== "true" ){
//                        $callback['msg'] = "you dont have admin";
//                        $callback['success'] = false;
//                        mysqli_close($con);
//                        echo json_encode($callback);
//                        return;
//                }
                    
                if( $page_id ) {
                    
                        $sql = "UPDATE page SET 
                                            p_id='$p_id',
                                            p_title='$p_title',
                                            p_main_category_id='$mainCate',
                                            p_sub_category_id='$subCate',
                                            p_pre_chn='$p_pre_chn' ,
                                            p_pre_eng='$p_pre_eng' ,
                                            p_back_chn='$p_back_chn' ,
                                            p_back_eng='$p_back_eng'  " 
                            . "WHERE page_id=$page_id";
                        
                        if( mysqli_query($con, $sql) ) {
                                $callback['success'] = true;
                        }
                        else {
                                $callback['msg'] = "UPDATE fail";
                                $callback['success'] = false;
                        }
                } else {
                    
                        $insert_array = array( "p_id" => $p_id ,
                                               "p_title" => $p_title ,
                                               "p_main_category_id" => $mainCate ,
                                               "p_sub_category_id" => $subCate ,
                                               "p_pre_chn" => $p_pre_chn ,
                                               "p_pre_eng" => $p_pre_eng ,
                                               "p_back_chn" => $p_back_chn ,
                                               "p_back_eng" => $p_back_eng ,
                                               "p_date" => date('Y-m-d H:i:s')
                                            );

                        if( insert_sql($con, "page", $insert_array) ) {
                                $callback['success'] = true;
                        }
                        else {
                                $callback['success'] = false;
                                $callback['msg'] = "video insert fail";
                        }
                        
                }
                
                        mysqli_close($con);
                     
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        echo json_encode($callback);
    
    }
    
    function fn_delete(){
        $callback = array();
        try{
                if( !check_empty( array( "page_id" ) ) ){// "token" ,
                        $callback['msg'] = "輸入資料不完整";
                        $callback['success'] = false;
                        echo json_encode($callback);
                        return;
                }

//                    $token = md5( $_REQUEST[ "token" ] );
                $page_id = $_REQUEST[ "page_id" ];

                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");
                // Check connection
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        echo json_encode($callback);
                        return;
                }

//                $account = get_sql($con, TABLE_ACCOUNT, "WHERE a_token LIKE '%\\\"$token\\\"%'");
//                if( !$account ) {
//                        $callback['msg'] = "Login fail";
//                        $callback['success'] = false;
//                        mysqli_close($con);
//                        echo json_encode($callback);
//                        return;
//                }
//                if( $account[0]['a_admin'] !== "true" ){
//                        $callback['msg'] = "you dont have admin";
//                        $callback['success'] = false;
//                        mysqli_close($con);
//                        echo json_encode($callback);
//                        return;
//                }

                $page = get_sql($con, "page" , "WHERE page_id=$page_id");
                if( !$page ){
                        $callback['msg'] = "page is not exist";
                        $callback['success'] = false;
                        mysqli_close($con);
                        echo json_encode($callback);
                        return;
                }

                if(mysqli_query($con, "DELETE FROM page WHERE page_id=$page_id") ){
                        $callback['success'] = true;
                }
                else{
                        $callback['msg'] = "修改失敗";
                        $callback['success'] = false;
                }

                mysqli_close($con);

        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        echo json_encode($callback);

    }
    
    function fn_update_page_block_blockade(){
        $callback = array();
        try{
                if( !check_empty( array( "page_id" ) ) ){// "token" ,
                        $callback['msg'] = "輸入資料不完整";
                        $callback['success'] = false;
                        echo json_encode($callback);
                        return;
                }

//                    $token = md5( $_REQUEST[ "token" ] );
                $page_id = $_REQUEST[ "page_id" ]+0;
                $display = $_REQUEST[ "display" ];

                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");
                // Check connection
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        echo json_encode($callback);
                        return;
                }

//                $account = get_sql($con, TABLE_ACCOUNT, "WHERE a_token LIKE '%\\\"$token\\\"%'");
//                if( !$account ) {
//                        $callback['msg'] = "Login fail";
//                        $callback['success'] = false;
//                        mysqli_close($con);
//                        echo json_encode($callback);
//                        return;
//                }
//                if( $account[0]['a_admin'] !== "true" ){
//                        $callback['msg'] = "you dont have admin";
//                        $callback['success'] = false;
//                        mysqli_close($con);
//                        echo json_encode($callback);
//                        return;
//                }

                $page = get_sql($con, "page" , "WHERE page_id=$page_id");
                if( !$page ){
                        $callback['msg'] = "page is not exist";
                        $callback['success'] = false;
                        mysqli_close($con);
                        echo json_encode($callback);
                        return;
                }

                $sql = "UPDATE page SET p_display='$display' WHERE page_id=$page_id";

                if( mysqli_query($con, $sql) ) {
                        $callback['success'] = true;
                }
                else {
                        $callback['msg'] = "UPDATE fail";
                        $callback['success'] = false;
                }
                
                mysqli_close($con);

        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        echo json_encode($callback);

    }
    
    function fn_notification(){
        $callback = array();
        try{
                if( !check_empty( array( "token" , "title" , "message" , "page_id" ) ) ){
                        $callback['msg'] = "輸入資料不完整";
                        $callback['success'] = false;
                        echo json_encode($callback);
                        return;
                }

                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con_noti=mysqli_connect(DB_HOST,DB_USER,DB_PASS,"notification");
                $con->query("SET NAMES utf8");
                // Check connection
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        echo json_encode($callback);
                        return;
                }

                $title = $_REQUEST[ "title" ];
                $message = $_REQUEST[ "message" ];
                $page_id = $_REQUEST[ "page_id" ];
                $token = md5( $_REQUEST[ "token" ] );
                
                $Check_Admin = Check_Admin( $con , $token );
                if( ! $Check_Admin['success'] ){
                        $callback['msg'] = "you dont have admin";
                        $callback['success'] = false;
                        mysqli_close($con);
                        mysqli_close($con_noti);
                        return $callback;
                }
                
                include '../../service/notification/fn_android_sendMsg.php';
                $deviceToken_Android = get_sql($con_noti, "mobile_info" , "WHERE deviceOS='Android' AND app='hf'" , "token" );
                if( $deviceToken_Android ){
                        foreach ($deviceToken_Android as $key => $value) {
                                $android_callback_arr = fn_android_sendMsg( $value['token'] , $title , $message , http_default_path.'?p='.$page_id );
//                                echo json_encode($android_callback_arr);
                        }
                }else{
                        $callback['msg'] = "query deviceToken_Android fail";
                        $callback['success'] = false;
                }
                
                include '../../service/notification/fn_ios_sendMsg.php';
                fn_ios_sendMsg( $message , http_default_path.'?p='.$page_id );
                
                
                $timestamp = strtotime('now');
                $datetime = date( 'Y-m-d H:i:s' , $timestamp );
                
                if( ! insert_sql( $con , "page_notification" , array( "a_id" => $Check_Admin['data'][0]['a_id'], "pn_title" => $title, "pn_message" => $message, "page_id" => $page_id, "pn_datetime" => $datetime, "pn_timestamp" => $timestamp ) ) ){
                        $callback['msg'] = "insert_sql fail";
                        $callback['success'] = false;
                        mysqli_close($con);
                        echo json_encode($callback);
                        return;
                } else {
                        $callback['success'] = true;
                }

                mysqli_close($con);

        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        echo json_encode($callback);

    }
?>
