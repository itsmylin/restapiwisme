<?php	
        /*
        * @file json_mgm_account.php
        * @brief TABLE:account

        * detail 

        * @author arod ( howareu520@gmail.com )
        * @date 2016-08-30 */

        include '../../php/config.php';
        include '../../php/global.php';
        
        $func = $_REQUEST["func"];

        switch ($func) {
            case "fn_read_account_statistics":
                $echo = fn_read_account_statistics();
                break;
            case "fn_read_account_highchart_line":
                $echo = fn_read_account_highchart_line();
                break;
        }
        echo json_encode($echo);
        
    
    function fn_read_account_statistics(){
        $callback = array();
        $callback['data'] = array();
        try{    
                $area = array();
                $sex = array();
                $age = array();
                $job = array();
                
//                if( !check_empty( array("token" ) ) ) {
//                        $callback['msg'] = "輸入資料不完整";
//                        $callback['success'] = false;
//                        return $callback;
//                }
                
//                $token = md5( $_REQUEST[ "token" ] );
                
                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");
                date_default_timezone_set('Asia/Taipei');
                // Check connection
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        return $callback;
                }
                
//                $account = get_sql($con, "account" , "WHERE a_token LIKE '%\\\"$token\\\"%'");
//                if( !$account ) {
//                        $callback['msg'] = "Login fail";
//                        $callback['success'] = false;
//                        mysqli_close($con);
//                        return $callback;
//                }
//                if( $account[0]['a_admin'] !== "true" ){
//                        $callback['msg'] = "you dont have admin";
//                        $callback['success'] = false;
//                        mysqli_close($con);
//                        return $callback;
//                }

                //會員居住城市統計
                $res_area = mysqli_query($con, "SELECT SUM(area.none) `none`, SUM(area.north) `north`, SUM(area.central) `central`, SUM(area.south) `south`, SUM(area.east) `east` " .
                                                "FROM " .
                                                "( " .
                                                        "SELECT SUM( CASE WHEN ac.ac_area='none' THEN 1 ELSE 0 END) `none` , SUM( CASE WHEN ac.ac_area='north' THEN 1 ELSE 0 END) `north`, SUM( CASE WHEN ac.ac_area='central' THEN 1 ELSE 0 END) `central`, SUM( CASE WHEN ac.ac_area='south' THEN 1 ELSE 0 END) `south`, SUM( CASE WHEN ac.ac_area='east' THEN 1 ELSE 0 END) `east` " .
                                                        "FROM account as a LEFT JOIN account_city as ac on ac.ac_id=a.ac_id WHERE a.a_delete != 'blockade' GROUP BY ac.ac_area " .
                                                ") as area");

                if (mysqli_num_rows($res_area) > 0) {

                        while($row = mysqli_fetch_array($res_area)) {
                                $all_num = (int)$row['north']+(int)$row['central']+(int)$row['south']+(int)$row['east']+(int)$row['none'];
                                $north = round( ( (int)$row['north']/$all_num ), 2 )*100;
                                $central = round( ( (int)$row['central']/$all_num ), 2 )*100;
                                $south = round( ( (int)$row['south']/$all_num ), 2 )*100;
                                $east = round( ( (int)$row['east']/$all_num ), 2 )*100;
                                $none = 100 - ($north+$central+$south+$east);
                                        
                                $area = array(
                                        "north" => $north . "%",
                                        "central" => $central . "%",
                                        "south" => $south . "%",
                                        "east" => $east . "%",
                                        "none" => $none . "%"//不公開
                                );
                        }
                        $callback['data']['area'] =  $area;

                } else {
                        echo "area sql error<br/>";
                }

                //會員性別統計
                $res_sex = mysqli_query($con, "SELECT SUM(sex.none) `none`, SUM(sex.male) `male`, SUM(sex.female) `female` " .
                                                "FROM " .
                                                "( " .
                                                        "SELECT SUM( CASE WHEN a_sex='male' THEN 1 ELSE 0 END) `male` , SUM( CASE WHEN a_sex='female' THEN 1 ELSE 0 END) `female`, SUM( CASE WHEN a_sex!='male' AND a_sex!='female' THEN 1 ELSE 0 END) `none` " .
                                                        "FROM account WHERE a_delete != 'blockade' GROUP BY a_sex " .
                                                ") as sex");

                if (mysqli_num_rows($res_sex) > 0) {

                        while($row = mysqli_fetch_array($res_sex)) {
                                $all_num = (int)$row['male']+(int)$row['female']+(int)$row['none'];
                                $male = round( ( (int)$row['male']/$all_num ), 2 )*100;
                                $female = round( ( (int)$row['female']/$all_num ), 2 )*100;
                                $none = 100 - ($male+$female);
                                $sex = array(
                                        "male" => $male . "%",
                                        "female" => $female . "%",
                                        "none" => $none . "%"//不公開
                                );
                        }
                        $callback['data']['sex'] =  $sex;

                } else {
                        echo "sex sql error<br/>";
                }

                //會員年齡統計
                $year = date('Y');
                                $_15 = $year-15;
                $_16 = $year-16;$_20 = $year-20;
                $_21 = $year-21;$_25 = $year-25;
                $_26 = $year-26;$_30 = $year-30;
                $_31 = $year-31;$_35 = $year-35;
                $_36 = $year-36;$_40 = $year-40;
                $_41 = $year-41;$_45 = $year-45;
                $_46 = $year-46;$_50 = $year-50;
                $_51 = $year-51;$_55 = $year-55;
                $_56 = $year-56;$_60 = $year-60;
                $_61 = $year-61;$_65 = $year-65;
                $_66 = $year-66;
                $res_age = mysqli_query($con, "SELECT SUM(age.15) `15`, SUM(age.16_20) `16_20`, SUM(age.21_25) `21_25`, SUM(age.26_30) `26_30`, SUM(age.31_35) `31_35`, SUM(age.36_40) `36_40`, SUM(age.41_45) `41_45`, SUM(age.46_50) `46_50`, SUM(age.51_55) `51_55`, SUM(age.56_60) `56_60`, SUM(age.61_65) `61_65`, SUM(age.66) `66` " .
                                                "FROM " .
                                                "( " .
                                                        "SELECT SUM( CASE WHEN a_age>=$_15 THEN 1 ELSE 0 END) `15` ," .
                                                                "SUM( CASE WHEN a_age<=$_16 AND a_age>=$_20 THEN 1 ELSE 0 END) `16_20`," .
                                                                "SUM( CASE WHEN a_age<=$_21 AND a_age>=$_25 THEN 1 ELSE 0 END) `21_25`," .
                                                                "SUM( CASE WHEN a_age<=$_26 AND a_age>=$_30 THEN 1 ELSE 0 END) `26_30`," .
                                                                "SUM( CASE WHEN a_age<=$_31 AND a_age>=$_35 THEN 1 ELSE 0 END) `31_35`," .
                                                                "SUM( CASE WHEN a_age<=$_36 AND a_age>=$_40 THEN 1 ELSE 0 END) `36_40`," .
                                                                "SUM( CASE WHEN a_age<=$_41 AND a_age>=$_45 THEN 1 ELSE 0 END) `41_45`," .
                                                                "SUM( CASE WHEN a_age<=$_46 AND a_age>=$_50 THEN 1 ELSE 0 END) `46_50`," .
                                                                "SUM( CASE WHEN a_age<=$_51 AND a_age>=$_55 THEN 1 ELSE 0 END) `51_55`," .
                                                                "SUM( CASE WHEN a_age<=$_56 AND a_age>=$_60 THEN 1 ELSE 0 END) `56_60`," .
                                                                "SUM( CASE WHEN a_age<=$_61 AND a_age>=$_65 THEN 1 ELSE 0 END) `61_65`," .
                                                                "SUM( CASE WHEN a_age<=$_66 THEN 1 ELSE 0 END) `66` " .
                                                        "FROM account WHERE a_delete != 'blockade' " .
                                                ") as age");

                if (mysqli_num_rows($res_age) > 0) {

                        while($row = mysqli_fetch_array($res_age)) {
                                $all_num = (int)$row['15']+(int)$row['16_20']+(int)$row['21_25']+(int)$row['26_30']+(int)$row['31_35']+(int)$row['36_40']+(int)$row['41_45']+(int)$row['46_50']+(int)$row['51_55']+(int)$row['56_60']+(int)$row['61_65']+(int)$row['66'];
                                $age = array(
                                        "15" => round( ( (int)$row['15']/$all_num ), 2 )*100 . "%",
                                        "16_20" => round( ( (int)$row['16_20']/$all_num ), 2 )*100 . "%",
                                        "21_25" => round( ( (int)$row['21_25']/$all_num ), 2 )*100 . "%",
                                        "26_30" => round( ( (int)$row['26_30']/$all_num ), 2 )*100 . "%",
                                        "31_35" => round( ( (int)$row['31_35']/$all_num ), 2 )*100 . "%",
                                        "36_40" => round( ( (int)$row['36_40']/$all_num ), 2 )*100 . "%",
                                        "41_45" => round( ( (int)$row['41_45']/$all_num ), 2 )*100 . "%",
                                        "46_50" => round( ( (int)$row['46_50']/$all_num ), 2 )*100 . "%",
                                        "51_55" => round( ( (int)$row['51_55']/$all_num ), 2 )*100 . "%",
                                        "56_60" => round( ( (int)$row['56_60']/$all_num ), 2 )*100 . "%",
                                        "61_65" => round( ( (int)$row['61_65']/$all_num ), 2 )*100 . "%",
                                        "66" => round( ( (int)$row['66']/$all_num ), 2 )*100 . "%"
                                );
                        }
                        $callback['data']['age'] =  $age;

                } else {
                        echo "age sql error<br/>";
                }

                //會員職業統計
                $res_job = mysqli_query($con, "SELECT ".
                                                        "COUNT(DISTINCT( CASE WHEN aj_id=1 THEN aj_id END )) AS none, ".//不公開
                                                        "COUNT(DISTINCT( CASE WHEN aj_id=2 THEN aj_id END )) AS farmer, ".//農林漁牧業
                                                        "COUNT(DISTINCT( CASE WHEN aj_id=3 THEN aj_id END )) AS manufacture, ".//製造業
                                                        "COUNT(DISTINCT( CASE WHEN aj_id=4 THEN aj_id END )) AS build, ".//營造業
                                                        "COUNT(DISTINCT( CASE WHEN aj_id=5 THEN aj_id END )) AS sale, ".//批發及零售業
                                                        "COUNT(DISTINCT( CASE WHEN aj_id=6 THEN aj_id END )) AS transportation, ".//運輸及倉儲業
                                                        "COUNT(DISTINCT( CASE WHEN aj_id=7 THEN aj_id END )) AS hotel, ".//住宿及餐飲業
                                                        "COUNT(DISTINCT( CASE WHEN aj_id=8 THEN aj_id END )) AS information, ".//資訊及通訊傳播業
                                                        "COUNT(DISTINCT( CASE WHEN aj_id=9 THEN aj_id END )) AS finance, ".//金融及保險業
                                                        "COUNT(DISTINCT( CASE WHEN aj_id=10 THEN aj_id END )) AS estate, ".//不動產業
                                                        "COUNT(DISTINCT( CASE WHEN aj_id=11 THEN aj_id END )) AS tech, ".//科學及技術服務業
                                                        "COUNT(DISTINCT( CASE WHEN aj_id=12 THEN aj_id END )) AS public_admin, ".//公共行政及國防
                                                        "COUNT(DISTINCT( CASE WHEN aj_id=13 THEN aj_id END )) AS education, ".//教育服務業
                                                        "COUNT(DISTINCT( CASE WHEN aj_id=14 THEN aj_id END )) AS medical, ".//醫療保健及社會工作服務業
                                                        "COUNT(DISTINCT( CASE WHEN aj_id=15 THEN aj_id END )) AS art, ".//藝術、娛樂及休閒服務業
                                                        "COUNT(DISTINCT( CASE WHEN aj_id=16 THEN aj_id END )) AS others, ". //其他
                                                        "COUNT(*) AS all_sum ".
                                                "FROM account ".
                                                "WHERE a_delete != 'blockade'");
                
                if (mysqli_num_rows($res_job) > 0) {

                        while($row = mysqli_fetch_array($res_job)) {
                                $job = array(
                                        "farmer" => round( ( (int)$row['farmer']/$row['all_sum'] ), 2 )*100 . "%",
                                        "manufacture" => round( ( (int)$row['manufacture']/$row['all_sum'] ), 2 )*100 . "%",
                                        "build" => round( ( (int)$row['build']/$row['all_sum'] ), 2 )*100 . "%",
                                        "sale" => round( ( (int)$row['sale']/$row['all_sum'] ), 2 )*100 . "%",
                                        "transportation" => round( ( (int)$row['transportation']/$row['all_sum'] ), 2 )*100 . "%",
                                        "hotel" => round( ( (int)$row['hotel']/$row['all_sum'] ), 2 )*100 . "%",
                                        "information" => round( ( (int)$row['information']/$row['all_sum'] ), 2 )*100 . "%",
                                        "finance" => round( ( (int)$row['finance']/$row['all_sum'] ), 2 )*100 . "%",
                                        "estate" => round( ( (int)$row['estate']/$row['all_sum'] ), 2 )*100 . "%",
                                        "tech" => round( ( (int)$row['tech']/$row['all_sum'] ), 2 )*100 . "%",
                                        "public_admin" => round( ( (int)$row['public_admin']/$row['all_sum'] ), 2 )*100 . "%",
                                        "education" => round( ( (int)$row['education']/$row['all_sum'] ), 2 )*100 . "%",
                                        "medical" => round( ( (int)$row['medical']/$row['all_sum'] ), 2 )*100 . "%",
                                        "art" => round( ( (int)$row['art']/$row['all_sum'] ), 2 )*100 . "%",
                                        "others" => round( ( (int)$row['others']/$row['all_sum'] ), 2 )*100 . "%",
                                        "none" => round( ( (int)$row['none']/$row['all_sum'] ), 2 )*100 . "%",
                                );
                        }
                        $callback['data']['job'] = $job;

                        $callback['success'] = true;
                } else {
                        echo "job sql error<br/>";
                }
                mysqli_close($con);
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        
        return $callback;
    }
    
    
    function fn_read_account_highchart_line(){
        $callback = array();
        try{    
                $cart = array();
                $month = array(0,0,0,0,0,0,0,0,0,0,0,0);
                
//                if( !check_empty( array("token" ) ) ) {
//                        $callback['msg'] = "輸入資料不完整";
//                        $callback['success'] = false;
//                        return $callback;
//                }
                
//                $token = md5( $_REQUEST[ "token" ] );
                
                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");
                date_default_timezone_set('Asia/Taipei');
                // Check connection
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        return $callback;
                }
                
//                $account = get_sql($con, "account" , "WHERE a_token LIKE '%\\\"$token\\\"%'");
//                if( !$account ) {
//                        $callback['msg'] = "Login fail";
//                        $callback['success'] = false;
//                        mysqli_close($con);
//                        return $callback;
//                }
//                if( $account[0]['a_admin'] !== "true" ){
//                        $callback['msg'] = "you dont have admin";
//                        $callback['success'] = false;
//                        mysqli_close($con);
//                        return $callback;
//                }
                
                //年初年底 時間戳
                $year = date('Y');
                $thisYear_firstDate = date('Y-01-01 00:00:00');//2016-01-01 00:00:00
                $thisYear_lastDate = date( ($year+1).'-01-01 00:00:00' );//2017-01-01 00:00:00
                
                $registrationByMonth = get_sql($con, "account" 
                                                    , "WHERE a_registration_time BETWEEN '".$thisYear_firstDate."' AND '".$thisYear_lastDate."' GROUP BY date_format(a_registration_time,'%Y-%m')"
                                                    , "date_format(a_registration_time,'%Y-%m') as ym, COUNT(*) AS count_registration");
//                                            print_r($registrationByMonth);
//                                            echo $year;
                if($registrationByMonth){
                        foreach ($registrationByMonth as $key => $value) {
                                $i = substr($value['ym'], 5, 2) -1;
                                $month[ $i ] = $value['count_registration']+0;
                        }
                }else{
                        $callback['msg'] = "sql fail";
                        $callback['success'] = false;
                }

                $cart[] = array(
                        "name" => "會員數",
                        "data" => $month
                );
                /*[
                    {
                        name: 'Tokyo',
                        data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
                    }, 
                    {
                        name: 'New York',
                        data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
                    }
                ]*/
                
                $callback['data'] = $cart;
                $callback['success'] = true;
                mysqli_close($con);
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        
        return $callback;
    }
    
    ?>
