<?php
        /*
        * @file json_mgm_index.php
        * @brief TABLE: aphorism fruit

        * detail 

        * @author arod ( howareu520@gmail.com )
        * @date 2016-10-15 */

        include '../../php/config.php';
        include '../../php/global.php';
        
        $func = $_REQUEST["func"];

        switch ($func) {
            case "fn_read_mgm_index_regex":
                $echo = fn_read_mgm_index_regex();
                break;
            case "fn_delete_aphorism"://刪除靜思語
                $echo = fn_delete_aphorism();
                break;
            case "fn_update_aphorism_block_blockade"://上下架
                $echo = fn_update_aphorism_block_blockade();
                break;
            case "fn_update_fruit_img"://修改對應靜思語之水果圖
                $echo = fn_update_fruit_img();
                break;
        }
        echo json_encode($echo);

    function fn_read_mgm_index_regex(){
        
        try{    
                $callback = array();
                $cart = array();
                $fruit_dropdown = "<select>";
                
                $operation_html_block = empty($_REQUEST[ "operation_html_block" ]) ? "" : stripcslashes($_REQUEST["operation_html_block"]);
                $operation_html_blockade = empty($_REQUEST[ "operation_html_blockade" ]) ? "" : stripcslashes($_REQUEST["operation_html_blockade"]);
                
                
                if( !check_empty( array("token" ) ) ) {
                        $callback['msg'] = "輸入資料不完整";
                        $callback['success'] = false;
                        return $callback;
                }
                
                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");
                // Check connection
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        return $callback;
                }
                
                $token = md5( $_REQUEST[ "token" ] );
                
                $Check_Admin = Check_Admin( $con , $token );
                if( ! $Check_Admin['success'] ){
                        $callback['msg'] = "you dont have admin";
                        $callback['success'] = false;
                        mysqli_close($con);
                        return $callback;
                }
                
                $aphorism = get_sql($con, "aphorism as aph LEFT JOIN fruit as f on f.f_id=aph.f_id" , "" , "aph.aph_id, aph.aph_img, aph.aph_datetime, aph.aph_display, f.f_id, f.f_img" );
                
                $fruit = get_sql($con, "fruit" , "" , "f_id, f_img" );
                if ( $fruit ) {
                        foreach ($fruit as $key => $value) {
                                $fruit_dropdown .= '<option value="'.$value["f_id"].'">'.$value["f_img"].'</option>';
                        }
                        $fruit_dropdown .= "</select>";
                }
                
                if ( $aphorism ) {
                        foreach ($aphorism as $key => $value) {
                                $http_f_img = ($value['f_img'] != "")? '<a><div style="cursor: pointer; background-image: url('.http_upload_fruit_file . $value['f_img'].'); height: 100px;width:100px;" class="bg_top"></div></a>' : "";
                                $http_aph_img = ($value['aph_img'] != "")? '<a data-target="#myModalImg" data-toggle="modal"><div id="background-image-resource" style="cursor: pointer; background-image: url('.http_upload_aphorism_file . $value['aph_img'].'); height: 200px;width:100%;" class="bg_top" target-background-image="'.http_upload_aphorism_file . $value['aph_img'].'"></div></a>' : "";
                                $operation_html = ($value['aph_display'] == 'block')? $operation_html_block : $operation_html_blockade ;
                                
                                $cart[] = array( "0" => $aphorism[$key]['aph_id'] ,
                                                "1" => str_replace('value="'.$value["f_id"].'"', 'value="'.$value["f_id"].'"'.' selected' ,$fruit_dropdown) ,
                                                "2" => $http_f_img ,
                                                "3" => $aphorism[$key]['aph_img'] ,
                                                "4" => $http_aph_img ,
                                                "5" => $aphorism[$key]['aph_datetime'] ,
                                                "6" => $operation_html
                                );
                        }
                        
                        $callback['data'] = $cart;
                        
                } else {
                        $callback['msg'] = 'aphorism query fail';
                        $callback['success'] = false;
                }
                mysqli_close($con);
                
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        
        return $callback;
    }

    function fn_read_category_mainCate(){
        
        try{    
                $callback = array();
                $cart = array();
                                
                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");
                // Check connection
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        return $callback;
                }
                                
                $mainCate = get_sql_MYSQLI_NUM( $con , "category" , "WHERE cate_parent=0 ORDER BY cate_order ASC" , "cate_id, cate_name" );
                if ( $mainCate ){
                        $callback['data'] = $mainCate;
                        $callback['success'] =  true;
                } else {
                        $callback['data'] = array();
                        $callback['success'] = false;
                }

                mysqli_close($con);
                
        }
        catch (Exception $e)
        {
                echo "false";
        }
        
        return $callback;
    }
    
    function fn_delete_aphorism(){
        $callback = array();
        try{
                if( !check_empty( array( "token" , "aph_id" ) ) ){
                        $callback['msg'] = "輸入資料不完整";
                        $callback['success'] = false;
                        return $callback;
                }
                $aph_id = $_REQUEST[ "aph_id" ];

                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");
                // Check connection
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        return $callback;
                }
                
                $token = md5( $_REQUEST[ "token" ] );
                
                $Check_Admin = Check_Admin( $con , $token );
                if( ! $Check_Admin['success'] ){
                        $callback['msg'] = "you dont have admin";
                        $callback['success'] = false;
                        mysqli_close($con);
                        return $callback;
                }

                $aphorism = get_sql($con, "aphorism" , "WHERE aph_id=$aph_id");
                if( !$aphorism ){
                        $callback['msg'] = "aphorism is not exist";
                        $callback['success'] = false;
                        mysqli_close($con);
                        return $callback;
                }

                if(mysqli_query($con, "DELETE FROM aphorism WHERE aph_id=$aph_id") ){
                        $callback['success'] = true;
                }
                else{
                        $callback['msg'] = "DELETE fail";
                        $callback['success'] = false;
                }

                mysqli_close($con);

        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;

    }
    
    function fn_update_aphorism_block_blockade(){
        $callback = array();
        try{
                if( !check_empty( array( "token" , "aph_id" ) ) ){
                        $callback['msg'] = "輸入資料不完整";
                        $callback['success'] = false;
                        return $callback;
                }

//                    $token = md5( $_REQUEST[ "token" ] );
                $aph_id = $_REQUEST[ "aph_id" ]+0;
                $display = $_REQUEST[ "display" ];

                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");
                // Check connection
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        return $callback;
                }
                
                $token = md5( $_REQUEST[ "token" ] );
                
                $Check_Admin = Check_Admin( $con , $token );
                if( ! $Check_Admin['success'] ){
                        $callback['msg'] = "you dont have admin";
                        $callback['success'] = false;
                        mysqli_close($con);
                        return $callback;
                }

                $aphorism = get_sql($con, "aphorism" , "WHERE aph_id=$aph_id");
                if( !$aphorism ){
                        $callback['msg'] = "aphorism is not exist";
                        $callback['success'] = false;
                        mysqli_close($con);
                        return $callback;
                }

                $sql = "UPDATE aphorism SET aph_display='$display' WHERE aph_id=$aph_id";

                if( mysqli_query($con, $sql) ) {
                        $callback['success'] = true;
                }
                else {
                        $callback['msg'] = "UPDATE fail";
                        $callback['success'] = false;
                }
                
                mysqli_close($con);

        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;

    }
    
    function fn_update_fruit_img(){
        $callback = array();
        try{
                if( !check_empty( array( "token" , "aph_id" , "f_id" ) ) ){
                        $callback['msg'] = "輸入資料不完整";
                        $callback['success'] = false;
                        return $callback;
                }

                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");
                // Check connection
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        return $callback;
                }
                
                $aph_id = $_REQUEST[ "aph_id" ]+0;
                $f_id = $_REQUEST[ "f_id" ]+0;
                $token = md5( $_REQUEST[ "token" ] );
                
                $Check_Admin = Check_Admin( $con , $token );
                if( ! $Check_Admin['success'] ){
                        $callback['msg'] = "you dont have admin";
                        $callback['success'] = false;
                        mysqli_close($con);
                        return $callback;
                }
                
                $aphorism = update_sql($con, "aphorism" , array( "f_id" => $f_id ) , array( "aph_id" => $aph_id ) );
                if( !$aphorism ){
                        $callback['msg'] = "update_sql fail";
                        $callback['success'] = false;
                        mysqli_close($con);
                        return $callback;
                }
                
                $callback['success'] = true;

                mysqli_close($con);

        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        return $callback;

    }
?>
