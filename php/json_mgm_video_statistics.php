<?php	
        /*
        * @file json_mgm_account.php
        * @brief TABLE:account

        * detail 

        * @author arod ( howareu520@gmail.com )
        * @date 2016-08-30 */

        include '../../php/config.php';
        include '../../php/global.php';
        
        $func = $_REQUEST["func"];

        switch ($func) {
            case "fn_read_video_statistics":
                $echo = fn_read_video_statistics();
                break;
            case "fn_read_video_highchart_line":
                $echo = fn_read_video_highchart_line();
                break;
        }
        echo json_encode($echo);
        
    
    function fn_read_video_statistics(){
        $callback = array();
        try{    
                $main_data = array();
                $sub_data = array();
                
//                if( !check_empty( array("token" ) ) ) {
//                        $callback['msg'] = "輸入資料不完整";
//                        $callback['success'] = false;
//                        return $callback;
//                }
                
//                $token = md5( $_REQUEST[ "token" ] );
                
                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");
                date_default_timezone_set('Asia/Taipei');
                // Check connection
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        return $callback;
                }
                
//                $account = get_sql($con, "account" , "WHERE a_token LIKE '%\\\"$token\\\"%'");
//                if( !$account ) {
//                        $callback['msg'] = "Login fail";
//                        $callback['success'] = false;
//                        mysqli_close($con);
//                        return $callback;
//                }
//                if( $account[0]['a_admin'] !== "true" ){
//                        $callback['msg'] = "you dont have admin";
//                        $callback['success'] = false;
//                        mysqli_close($con);
//                        return $callback;
//                }
                
                /*大分類點閱統計*/
                $main_category = get_sql($con, "category" , "WHERE cate_parent=0" , "cate_id, cate_name");
                
                //月初月底 時間戳
                $thisMonth_firstSec = strtotime( date('Y-m-01') );
                $thisMonth_lastSec = strtotime( date('Y-m-t') )+3600*24;
                
                //週初週底 時間戳
                $thisWeek_firstSec = strtotime( 'last Sunday' );
                $thisWeek_lastSec = strtotime( 'this Saturday' )+3600*24;
                
                //今日 時間戳
                $today_firstSec = strtotime( date('Y-m-d') );
                $today_lastSec = $today_firstSec+3600*24;
                
                foreach ($main_category as $key => $value) {
                        $monthByCate = get_sql($con, "page as p LEFT JOIN page_click as pc on pc.page_id=p.page_id" , "WHERE p.p_main_category_id=".$value['cate_id']." AND pc.pc_timestamp between ". $thisMonth_firstSec ." AND " .$thisMonth_lastSec." GROUP BY p.p_main_category_id" , "COUNT(*) AS cate_id_".$value['cate_id']);
                        $weekByCate = get_sql($con, "page as p LEFT JOIN page_click as pc on pc.page_id=p.page_id" , "WHERE p.p_main_category_id=".$value['cate_id']." AND pc.pc_timestamp between ". $thisWeek_firstSec ." AND " .$thisWeek_lastSec." GROUP BY p.p_main_category_id" , "COUNT(*) AS cate_id_".$value['cate_id']);
                        $dayByCate = get_sql($con, "page as p LEFT JOIN page_click as pc on pc.page_id=p.page_id" , "WHERE p.p_main_category_id=".$value['cate_id']." AND pc.pc_timestamp between ". $today_firstSec ." AND " .$today_lastSec." GROUP BY p.p_main_category_id" , "COUNT(*) AS cate_id_".$value['cate_id']);
                        
                        $m_click = ($monthByCate) ? $monthByCate[0]['cate_id_'.$value['cate_id']] : 0;
                        $w_click = ($weekByCate) ? $weekByCate[0]['cate_id_'.$value['cate_id']] : 0;
                        $d_click = ($dayByCate) ? $dayByCate[0]['cate_id_'.$value['cate_id']] : 0;
                        
                        $main_data[] = array(
                                "cate_name" => $value['cate_name'],
                                "m_click" => $m_click,
                                "w_click" => $w_click,
                                "d_click" => $d_click
                        );
                        // [{"cate_name":"大分類", "m_click":999, "w_click":99}, "d_click":9}, {}..]
                }
                
                /*子分類點閱統計*/
                $sub_category = get_sql($con, "category" , "WHERE cate_parent != 0" , "cate_id, cate_name");
                
                //月初月底 時間戳
                $thisMonth_firstSec = strtotime( date('Y-m-01') );
                $thisMonth_lastSec = strtotime( date('Y-m-t') )+3600*24;
                
                //週初週底 時間戳
                $thisWeek_firstSec = strtotime( 'last Sunday' );
                $thisWeek_lastSec = strtotime( 'this Saturday' )+3600*24;
                
                //今日 時間戳
                $today_firstSec = strtotime( date('Y-m-d') );
                $today_lastSec = $today_firstSec+3600*24;
                
                foreach ($sub_category as $key => $value) {
                        $monthByCate = get_sql($con, "page as p LEFT JOIN page_click as pc on pc.page_id=p.page_id" , "WHERE p.p_sub_category_id=".$value['cate_id']." AND pc.pc_timestamp between ". $thisMonth_firstSec ." AND " .$thisMonth_lastSec." GROUP BY p.p_main_category_id" , "COUNT(*) AS cate_id_".$value['cate_id']);
                        $weekByCate = get_sql($con, "page as p LEFT JOIN page_click as pc on pc.page_id=p.page_id" , "WHERE p.p_sub_category_id=".$value['cate_id']." AND pc.pc_timestamp between ". $thisWeek_firstSec ." AND " .$thisWeek_lastSec." GROUP BY p.p_main_category_id" , "COUNT(*) AS cate_id_".$value['cate_id']);
                        $dayByCate = get_sql($con, "page as p LEFT JOIN page_click as pc on pc.page_id=p.page_id" , "WHERE p.p_sub_category_id=".$value['cate_id']." AND pc.pc_timestamp between ". $today_firstSec ." AND " .$today_lastSec." GROUP BY p.p_main_category_id" , "COUNT(*) AS cate_id_".$value['cate_id']);
                        
                        $m_click = ($monthByCate) ? $monthByCate[0]['cate_id_'.$value['cate_id']] : 0;
                        $w_click = ($weekByCate) ? $weekByCate[0]['cate_id_'.$value['cate_id']] : 0;
                        $d_click = ($dayByCate) ? $dayByCate[0]['cate_id_'.$value['cate_id']] : 0;
                        
                        $sub_data[] = array(
                                "cate_name" => $value['cate_name'],
                                "m_click" => $m_click,
                                "w_click" => $w_click,
                                "d_click" => $d_click
                        );
                        // [{"cate_name":"子分類", "m_click":999, "w_click":99}, "d_click":9}, {}..]
                }
                
                $callback['main_data'] = $main_data;
                $callback['sub_data'] = $sub_data;
                $callback['success'] = true;
                mysqli_close($con);
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        
        return $callback;
    }
    
    function fn_read_video_highchart_line(){
        $callback = array();
        try{    
                $cart = array();
                
//                if( !check_empty( array("token" ) ) ) {
//                        $callback['msg'] = "輸入資料不完整";
//                        $callback['success'] = false;
//                        return $callback;
//                }
                
//                $token = md5( $_REQUEST[ "token" ] );
                
                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");
                date_default_timezone_set('Asia/Taipei');
                // Check connection
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        return $callback;
                }
                
//                $account = get_sql($con, "account" , "WHERE a_token LIKE '%\\\"$token\\\"%'");
//                if( !$account ) {
//                        $callback['msg'] = "Login fail";
//                        $callback['success'] = false;
//                        mysqli_close($con);
//                        return $callback;
//                }
//                if( $account[0]['a_admin'] !== "true" ){
//                        $callback['msg'] = "you dont have admin";
//                        $callback['success'] = false;
//                        mysqli_close($con);
//                        return $callback;
//                }
                
                /*大分類點閱統計*/
                $main_category = get_sql($con, "category" , "WHERE cate_parent=0" , "cate_id, cate_name");
                
                //年初年底 時間戳
                $thisYear_firstSec = strtotime( date('Y-01-01') );//2016-01-01 00:00:00
                $thisYear_lastSec = strtotime( date('Y-01-01',strtotime('+1 year')) );//2017-01-01 00:00:00
                
                $year = date('Y');
                foreach ($main_category as $key => $value) {
                        $clickByMonthByCate = get_sql($con, "page as p JOIN (SELECT page_id, date_format(pc_datetime,'%Y-%m') as ym, COUNT(*) as month_click FROM page_click WHERE pc_timestamp BETWEEN ".$thisYear_firstSec." AND ".$thisYear_lastSec." GROUP BY page_id, date_format(pc_datetime,'%Y-%m') ) as pc on pc.page_id=p.page_id" 
                                                            , "WHERE p.p_main_category_id=".$value['cate_id']." GROUP BY pc.ym" 
                                                            , "pc.ym, SUM(pc.month_click) AS m_click");
                        
                        if($clickByMonthByCate){
                                foreach ($clickByMonthByCate as $key1 => $value1) {
                                        $month = array(0,0,0,0,0,0,0,0,0,0,0,0);
                                        
                                        $i = substr($value1['ym'], 5, 2) -1;
                                        $month[ $i ] = $value1['m_click']+0;
                                }
                        }else{
                                $month = array(0,0,0,0,0,0,0,0,0,0,0,0);
                        }
                        
                        $cart[] = array(
                                "name" => $value['cate_name'],
                                "data" => $month
                        );
                        /*[
                            {
                                name: 'Tokyo',
                                data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
                            }, 
                            {
                                name: 'New York',
                                data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
                            }
                        ]*/
                }
                
                $callback['data'] = $cart;
                $callback['success'] = true;
                mysqli_close($con);
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        
        return $callback;
    }
    
    
    ?>
