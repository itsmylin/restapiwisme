<?php header("X-Frame-Options: DENY");?>
<!DOCTYPE html>
<html lang="en">

<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale = 1.0, user-scalable = 0" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="app-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="apple-touch-fullscreen" content="yes" />
        <link rel='shortcut icon' href='template/images/favicon.ico' type='x-icon'>
        <title>留言管理 | healing_fruits</title>
        <meta name="description" content="What you see what you get Enjoy to Interactive with living objects">

        <link class="icon" href="../icon/logo.png" rel="apple-touch-icon-precomposed" />
        <link class="icon" href="../icon/logo.png" rel="SHORTCUT ICON" />
        
        <link rel="stylesheet" href="css/all_css.css">
        <link rel="stylesheet" href="css/global.css">
        
        <?php include( "js/all_js.php"); ?>
        <script src="js/jquery.pagination.js"></script>
        <script src="js/arod/management_account.jquery.pagination.js"></script>
        <script>
                $.global_date_statistics_column = "vip"; //table:date_statistics > col:vip
        </script>
        <!--script src="js/search.js"></script-->
        <script src="js/batch.js"></script>

        <!-- include HIGHCHARTS-->
        <script src="js/highstock.js"></script>
        <script src="js/mgm_highchart_jack.js"></script>
        <script src="js/mgm_hichart_click_jack.js"></script>
        <!-- for arod edit function (點擊折線圖的點會呼叫的fun)-->

        <!-- datePicker -->
        <script src="js/jquery-ui.js"></script>

        <!-- jquery.dataTables -->
        <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
        <script type="text/javascript" language="javascript" src="js/dataTablesPlugin.js"></script>

        <script>
                
                $(document).ready(function() {

                });


                function init() {
                        
                        $.initDatatable_1();
                        
                        $("#btn_checkbox_block_new_account").unbind('click').bind('click', function() {
                                console.log($('#example').data("rows_selected"));
                                console.log( 'btn_checkbox_block_new_account' );
                                $( "#add_account_modal" ).modal( "show" );
                                
                        });
                        
                        $("#add_account_modal_Yes").unbind('click').bind('click', function() {
                                
                                $(".alert").hide();
                                var bool = true;
                                var msg = "",pos,input_type;
                                $.each( $("#container input.necessary") , function( index , value ){
                                        input_type = $( value ).attr( "type" );
                                        if( input_type === "email" ){
                                                if( !email_event( $( value ) ) ){
                                                        bool = false;
                                                        pos = $( value );
                                                }
                                        }
                                        else if( input_type === "password" ){
                                                if( !password_event( $( value ) ) ){
                                                        bool = false;
                                                        pos = $( value );
                                                        $( value ).val("");
                                                        $( "#account_pwd2" ).val("");
                                                }
                                        }
                                        else{
                                                if( !input_event( $( value ) ) ){
                                                        bool = false;
                                                        pos = $( value );
                                                }
                                        }

                                });

                                if( !check_password_event( $( "#account_pwd2" ) ) ){
                                        bool = false;
                                        pos = $( "#account_pwd2" );
                                        $( "#account_pwd" ).val("");
                                        $( "#account_pwd2" ).val("");
                                }

                                if( !select_event( $( "#account_country" ) ) ){
                                        bool = false;
                                        pos = $( "#account_country" );
                                }
                                
                                if( bool ) {
                                    loading_ajax_show();
                                    var data = {
                                                token:      getCookie( "scs_cookie" ),
                                                a_email:      $( "#account_email" ).val(),
                                                a_password:   md5( $( "#account_pwd" ).val() ),
                                                a_nickname:       $( "#account_nickname" ).val(),
                                                a_country:     $( "#account_country" ).val(),
                                                a_eighteen:     $( "#account_eighteen" ).is( ":checked" ),
                                                a_admin:     $( "#account_admin" ).is( ":checked" )
                                    };
                                    var success_back = function( data ) {

                                            data = JSON.parse( data );
                                            console.log(data);
                                            loading_ajax_hide();
                                            if( data.success ) {
                                                show_remind( "新增成功" );
                                                $.initDatatable_1();
                                                $( "#add_account_modal" ).modal( "hide" );
                                            }
                                            else {
                                                show_remind( data.msg , "error" );
                                                if( data.action === "email" ){
                                                        $( "#account_email" ).parent().find(".alert").hide();
                                                        $( "#account_email" ).parent().find(".alert.error2").show();
                                                        scrollto( $( "#account_email" ) );
                                                }
                                            }

                                    }
                                    var error_back = function( data ) {
                                            console.log(data);
                                    }
                                    $.Ajax( "POST" , "../php/member.php?func=add_by_admin" , data , "" , success_back , error_back);

                                }
                                else {
                                    re_captcha();
                                    show_remind( msg , "error" );
                                    scrollto( pos );
                                }
                                    
                        });
                        
                        $("#btn_checkbox_block_email_account").unbind('click').bind('click', function() {
                                    console.log($('#example').data("rows_selected"));
                                    console.log( 'btn_checkbox_block_email_account' );
                                    
                                    $( "#dialogue_email_account_user" ).html( '注意：確定送出此 ' + $('#example').data("rows_selected").length + ' 位使用者' );
                                    
                                    $("#dialogue_email_account").modal("show");
                                    
                                    $("#dialogue_email_account_main_Yes").unbind('click').bind('click', function() {
                                                loading_ajax_show();
                                                var data = {
                                                        a_id    : JSON.stringify( $('#example').data("rows_selected") ) ,
                                                        title   : $( "#dialogue_email_account_main" ).val() ,
                                                        content : $( "#dialogue_email_account_contnet" ).val() ,
                                                        token:   getCookie( "scs_cookie" )
                                                };
                                                var success_back = function( data ) {
                                                        
                                                        loading_ajax_hide();
                                                        data = JSON.parse( data );
                                                        console.log(data);
                                                        if( data.success ) {
                                                                show_remind("送出成功", "success");
                                                                $( "#dialogue_email_account" ).modal("hide");
                                                        }
                                                        else {
                                                                show_remind( data.msg , "error" );
                                                        }

                                                }
                                                var error_back = function( data ) {
                                                        console.log(data);
                                                }
                                                $.Ajax( "GET" , "php/json_mgm_account.php?func=fn_btn_checkbox_send_account_letter" , data , "" , success_back , error_back);

                                        
                                    });
                                    
                        });
                        
                        $("#btn_checkbox_block_emailall_account").unbind('click').bind('click', function() {
                                    console.log($('#example').data("rows_selected"));
                                    console.log( 'btn_checkbox_block_emailall_account' );
                                    
                                    $( "#dialogue_email_account_user" ).html( '注意：為防止會員名單外洩，將用密件副本傳送，確定送出全部會員' );
                                    
                                    $("#dialogue_email_account").modal("show");
                                    
                                    $("#dialogue_email_account_main_Yes").unbind('click').bind('click', function() {
                                                loading_ajax_show();
                                                var data = {
                                                        title   : $( "#dialogue_email_account_main" ).val() ,
                                                        content : $( "#dialogue_email_account_contnet" ).val() ,
                                                        token:   getCookie( "scs_cookie" )
                                                };
                                                var success_back = function( data ) {
                                                        loading_ajax_hide();
                                                        data = JSON.parse( data );
                                                        console.log(data);
                                                        if( data.success ) {
                                                                $( "#dialogue_email_account" ).modal("hide");
                                                                show_remind("送出成功", "success");
                                                        }
                                                        else {
                                                                show_remind( data.msg , "error" );
                                                        }

                                                }
                                                var error_back = function( data ) {
                                                        console.log(data);
                                                }
                                                $.Ajax( "GET" , "php/json_mgm_account.php?func=fn_btn_all_send_account_letter" , data , "" , success_back , error_back);

                                        
                                    });
                                    
                        });
                        
                        $("#myModalEditAccount").delegate("#myModalEditAccount_Yes", "click", function() {
                                $.a_id = $("#myModalEditAccount_Yes").attr('a_id');
                                fn_update_account_log();
                        });
                }
                
                $.initDatatable_1 = function initDatatable_1() {

                        //destory dataTable
                        $("#example").DataTable().destroy();
                        $("#datatable").html("");

                        var column = ["影片", "會員", "會員留言", "管理者", "管理者留言", "會員留言時間<br>管理者留言時間", "操作"];

                        createSearchTable('#search_Datatable', '#example', column);

                        var ajax = 'php/json_mgm_message.php?func=fn_read_mgm_message_regex&' +
                                'token='+getCookie("scs_cookie")+'&' +
                                'operation_html_block=<div class="center"><a id="block" class="mark msg_box">上架中</a><br><a class="mark msg_box page_delete" id="delete">刪除</a></div>&' +
                                'operation_html_blockade=<div class="center"><a id="block" class="mark msg_box orange_btn">下架中</a><br><a class="mark msg_box page_delete" id="delete">刪除</a></div>';


                        var order = [
                                [1, 'desc']
                        ];
                        
                        createTable('#datatable', '#example', column);
                        createDataTablePipeline('#example', ajax, order, false, [1]);
                        $('#example').data("rows_selected", []);
                        addEvent('#datatable', '#example');
                        
                        //修改會員資料 click id=pencil 
                        $('#example tbody').on('click', ' tr td:nth-child(2) a', function(e) {
                                var $row = $(this).closest('tr');
                                // Get row data
                                var data = $('#example').DataTable().row($row).data();
                                // Get row ID
                                $.a_id = data[0];

                                fn_read_account_log();
                        });
                        //event管理者貼文
                        $('#example tbody').on('click', '[id=po_admin_content]', function(e) {
                                var $row = $(this).closest('tr');
                                // Get row data
                                var row = $('#example').DataTable().row($row).data();
                                // Get row ID
//                                $.pm_id = row[0];

                                var data = {
                                        token: getCookie("scs_cookie"),
                                        pm_id: row[0],
                                        pm_admin_content: $(this).prev().val() //$('#admin_content').val()
                                };
                                var success_back = function(data) {
                                        data = JSON.parse(data);
                                        console.log(data);
                                        if (data.success) {
                                                console.log(this);
                                                show_remind("貼文成功且上架囉");
                                                $.initDatatable_1();
                                        } else {
                                                show_remind( data.msg , "error");
                                        }
                                };
                                var error_back = function(data) {
                                        console.log(data);
                                };
                                $.Ajax("POST", "php/json_mgm_message.php?func=fn_update_message_content", data, "", success_back, error_back);
                        });
                        //event上下架管理者貼文
                        $('#example tbody').on('click', '[id=block]', function(e) {
                                var $row = $(this).closest('tr');
                                // Get row data
                                var row = $('#example').DataTable().row($row).data();
                                // Get row ID
//                                $.pm_id = row[0];

                                var data = {
                                        token: getCookie("scs_cookie"),
                                        pm_id: row[0]
                                };
                                var success_back = function(data) {
                                        data = JSON.parse(data);
                                        console.log(data);
                                        if (data.success) {
                                                console.log(this);
                                                show_remind("更改成功");
                                                $.initDatatable_1();
                                        } else {
                                                show_remind( data.msg , "error");
                                        }
                                };
                                var error_back = function(data) {
                                        console.log(data);
                                };
                                $.Ajax("POST", "php/json_mgm_message.php?func=fn_update_message_block", data, "", success_back, error_back);
                        });
                        //event刪除會員及管理者貼文
                        $('#example tbody').on('click', '[id=delete]', function(e) {
                                var $row = $(this).closest('tr');
                                // Get row data
                                var row = $('#example').DataTable().row($row).data();
                                // Get row ID
//                                $.pm_id = row[0];
                                $("#myModaldelete").modal('show');
                                $("#myModaldelete_yes").unbind('click').bind('click', function() {
                                        var data = {
                                                token: getCookie("scs_cookie"),
                                                pm_id: row[0]
                                        };
                                        var success_back = function(data) {
                                                data = JSON.parse(data);
                                                console.log(data);
                                                if (data.success) {
                                                        show_remind("刪除成功");
                                                        $.initDatatable_1();
                                                } else {
                                                        show_remind( data.msg , "error");
                                                }
                                        };
                                        var error_back = function(data) {
                                                console.log(data);
                                        };
                                        $.Ajax("POST", "php/json_mgm_message.php?func=fn_update_message_delete", data, "", success_back, error_back);
                                });
                        });

                }

                //會員資訊Dialog
                function fn_read_account_log() {

                        var data = {
                                token: getCookie("scs_cookie"),
                                a_id: $.a_id
                        };
                        var success_back = function(data) {

                                console.log(data);
                                var tmp_franchisee = "";
                                var tmp_kind = "";
                                data = JSON.parse(data);
                                console.log(data);
                                if (data.success) {

                                        $("#myModalEditAccount_Yes").attr('a_id', data.data.a_id);

                                        $("#Edit_a_admin").val(data.data.a_admin);
                                        
                                        $("#Edit_a_email").val(data.data.a_email);
                                        $("#Edit_a_nickname").val(data.data.a_nickname);
                                        $("#Edit_a_sex").val(data.data.a_sex);
                                        $("#Edit_a_age").val(data.data.a_age);
                                        $("#Edit_a_city").val(data.data.a_city);
                                        $("#Edit_a_job").val(data.data.a_job);
                                        $("#Edit_a_last_login_time").val(data.data.a_last_login_time);
                                        $("#Edit_a_registration_time").val(data.data.a_registration_time);
                                
                                        $("#myModalEditAccount").modal("show");

                                } else {
                                        $("#myModalEditAccount").modal("hide");
                                }

                        };
                        var error_back = function(data) {
                                console.log(data);
                        };
                        $.Ajax("POST", "php/json_mgm_account.php?func=fn_read_account_log", data, "", success_back, error_back);

                }
                
                //修改會員資料
                function fn_update_account_log() {

                        var data = {
                                token: getCookie("scs_cookie"),
                                a_id: $.a_id,
                                a_admin: $("#Edit_a_admin").val(),

                                a_email: $("#Edit_a_email").val(),
                                a_nickname: $("#Edit_a_nickname").val(),
                                a_sex: $("#Edit_a_sex").val(),
                                a_age: $("#Edit_a_age").val(),
                                a_city: $("#Edit_a_city").val(),
                                a_job: $("#Edit_a_job").val(),
                        };
                        var success_back = function(data) {
                                data = JSON.parse(data);
                                console.log(data);
                                if (data.success) {
                                        show_remind("更改成功");
                                        $.initDatatable_1();
                                        $("#myModalEditAccount").modal('hide');
                                } else {
                                        alert("更新失敗");
                                }

                        };
                        var error_back = function(data) {
                                console.log(data);
                        };
                        $.Ajax("POST", "php/json_mgm_account.php?func=fn_update_account_log", data, "", success_back, error_back);
                }
        </script>
</head>

<body>
        <div id="all">

                <?php include( "html/loading.php"); ?>
                <?php include( "html/header.php"); ?>

                <div class="content">
                        <?php include( "html/sidebar_setting.php"); ?>

                        <div class="main-content container">                        
                                <div class="path">
                                        <a href="#">控制後台</a> > <a href="#">留言管理</a>
                                </div>

                                <div class="list">

                                        <h2>留言管理</h2>

                                        <!-- search box -->
        <!--                                        <div class="dataTable_search_box">
                                                <div id="condition_Datatable">
                                                        <table>
                                                                <tr class="condition_4" data-column="2">
                                                                        <td align="center">
                                                                                開始日期:
                                                                                <input type="text" placeholder="開始時間" class="lbox datepicker" id="condition_Datatable_starttime">
                                                                        </td>
                                                                        <td align="center">
                                                                                結束日期:
                                                                                <input type="text" placeholder="開始時間" class="lbox datepicker" id="condition_Datatable_endtime">
                                                                        </td>
                                                                        <td class="center">主分類:
                                                                                <select style=" width: 50%;" id="SD_mainCate">
                                                                                        <option value=""></option>
                                                                                </select>
                                                                        </td>
                                                                        <td class="center">子分類:
                                                                                <select style=" width: 50%;" id="SD_subCate">
                                                                                        <option value=""></option>
                                                                                </select>
                                                                        </td>
                                                                </tr>
                                                                <tr class="condition_4" data-column="2">
                                                                        <td align="center">
                                                                                標題:<input type="text" id="SD_title" class="column_filter hasDatepicker">
                                                                        </td>
                                                                        <td class="center">狀態:
                                                                                <select style=" width: 50%;" id="SD_display">
                                                                                        <option value="">全部</option>
                                                                                        <option value="block">上架中</option>
                                                                                        <option value="blockade">下架中</option>
                                                                                </select>
                                                                        </td>
                                                                </tr>
                                                        </table>
                                                </div>

                                                <button id="btn_Datatable" class="submit">查詢</button>
                                        </div>-->
                                        <!-- function btn box -->
        <!--                                        <div class="dataTable_function_btn_box">
                                                <div class="float-left">操作結果訊息顯示</div>
                                                <button id="btn_checkbox_block_new_account" onclick="$('#add_account_modal').modal('show');" class="float-right" type="button">+新增會員</button>
                                        </div>-->

                                        <div id="datatable"></div>
                                </div>
                        </div>

                </div>

                <?php include( "html/footer.php"); ?>

        </div>

        <!--add-->
        <div class="modal fade" id="add_account_modal" aria-hidden="false">
                <div class="modal-dialog">
                        <div id="dialog-msg">
                                <dl>
                                        <div class="list">
                                                <h2>新增帳號</h2>
                                                <form role="form" id="container">
                                                        <ul>
                                                                <li>
                                                                        <span>信箱</span>
                                                                        <input type="email" onfocus="clear_event($(this))" onblur="email_event($(this))" class="necessary" placeholder="帳號或 E-mail" id="account_email">
                                                                        <div style="display:none;" class="alert success"></div>
                                                                        <div style="display:none;" class="alert warning">必填欄位</div>
                                                                        <div style="display:none;" class="alert error">請輸入有效的電子郵箱</div>
                                                                        <div style="display:none;" class="alert error error2">電子郵件重複</div>
                                                                </li>
                                                                <li>
                                                                        <span>輸入密碼</span>
                                                                        <input type="password" oninput="$( '#account_pwd2' ).val('');" onfocus="clear_event($(this))" onblur="password_event($(this))" class="necessary" id="account_pwd">
                                                                        <div style="display:none;" class="alert success"></div>
                                                                        <div style="display:none;" class="alert warning">至少八個字元組以上，需要包括小寫字母和數字。</div>
                                                                </li>
                                                                <li>
                                                                        <span>確認密碼</span>
                                                                        <input type="password" onfocus="clear_event($(this))" onblur="check_password_event($(this))" id="account_pwd2">
                                                                        <div style="display:none;" class="alert success"></div>
                                                                        <div style="display:none;" class="alert error">密碼不一致</div>
                                                                </li>
                                                                <li>
                                                                        <span>公開暱稱</span>
                                                                        <input type="text" onfocus="clear_event($(this))" onblur="input_event($(this))" class="necessary" id="account_nickname">
                                                                        <div style="display:none;" class="alert success"></div>
                                                                        <div style="display:none;" class="alert warning">必填欄位</div>
                                                                </li>
                                                                <li class="check">
                                                                        <input type="checkbox" id="account_admin">是否是管理員
                                                                </li>
                                                                <li class="center">
                                                                        <a class="button" href="#" data-dismiss="modal" id="add_account_modal_Yes">確定</a>
                                                                        <a class="button delete" href="#" data-dismiss="modal">取消</a>
                                                                </li>
                                                        </ul>
                                                </form>
                                        </div>
                                </dl>
                        </div>
                </div>
        </div>

        <!--修改會員資料 dialog-->
        <div aria-hidden="false" id="myModalEditAccount" class="modal fade" style="display:none;">
                <div class="modal-dialog">
                        <div id="dialog-msg">
                                <dl>
                                        <div class="list">
                                                <h2>修改會員資料</h2>
                                                <nav class="set">
                                                        <form>
                                                                <ul>
                                                                        <li>
                                                                                <span>管理權限 </span>
                                                                                <select id="Edit_a_admin">
                                                                                    <option value="true">是</option>
                                                                                    <option value="false">否</option>
                                                                                </select>
                                                                        </li>
                                                                        <hr>
                                                                        <li>
                                                                                <span>信箱 </span>
                                                                                <input type="text" class="necessary long" id="Edit_a_email" placeholder="信箱" value="">
                                                                        </li>
                                                                        <li>
                                                                                <span>暱稱 </span>
                                                                                <input type="text" class="necessary long" id="Edit_a_nickname" placeholder="暱稱" value="">
                                                                        </li>
                                                                        <li>
                                                                                <span>性別 </span>
                                                                                <select id="Edit_a_sex">
                                                                                        <option value="male">男</option>
                                                                                        <option value="female">女</option>
                                                                                </select>
                                                                        </li>
                                                                        <li>
                                                                                <span>年齡 </span>
                                                                                <input type="number" class="necessary long" id="Edit_a_age" placeholder="年齡" value="">
                                                                        </li>
                                                                        <li>
                                                                                <span>居住地 </span>
                                                                                <select id="Edit_a_city">
                                                                                        <option value="1">不公開</option>
                                                                                        <option value="2">基隆市</option>
                                                                                        <option value="3">臺北市</option>
                                                                                        <option value="4">新北市</option>
                                                                                        <option value="5">桃園市</option>
                                                                                        <option value="6">新竹市</option>
                                                                                        <option value="7">新竹縣</option>
                                                                                        <option value="8">苗栗縣</option>
                                                                                        <option value="9">臺中市</option>
                                                                                        <option value="10">南投縣</option>
                                                                                        <option value="1">彰化縣</option>
                                                                                        <option value="12">雲林縣</option>
                                                                                        <option value="13">嘉義市</option>
                                                                                        <option value="14">嘉義縣</option>
                                                                                        <option value="15">臺南市</option>
                                                                                        <option value="16">高雄市</option>
                                                                                        <option value="17">屏東縣</option>
                                                                                        <option value="18">宜蘭縣</option>
                                                                                        <option value="19">花蓮縣</option>
                                                                                        <option value="20">臺東縣</option>
                                                                                        <option value="21">澎湖縣</option>
                                                                                        <option value="22">連江縣</option>
                                                                                </select>
                                                                        </li>
                                                                        <li>
                                                                                <span>行業 </span>
                                                                                <select id="Edit_a_job">
                                                                                        <option value="1">不公開</option>
                                                                                        <option value="2">農林漁牧業</option>
                                                                                        <option value="3">製造業</option>
                                                                                        <option value="4">營造業</option>
                                                                                        <option value="5">批發及零售業</option>
                                                                                        <option value="6">運輸及倉儲業</option>
                                                                                        <option value="7">住宿及餐飲業</option>
                                                                                        <option value="8">資訊及通訊傳播業</option>
                                                                                        <option value="9">金融及保險業</option>
                                                                                        <option value="10">不動產業</option>
                                                                                        <option value="11">科學及技術服務業</option>
                                                                                        <option value="12">公共行政及國防</option>
                                                                                        <option value="13">教育服務業</option>
                                                                                        <option value="14">醫療保健及社會工作服務業</option>
                                                                                        <option value="15">藝術、娛樂及休閒服務業</option>
                                                                                        <option value="16">其他</option>
                                                                                </select>
                                                                        </li>
                                                                        <li>
                                                                                <span>上次登入時間 </span>
                                                                                <input type="text" class="necessary long" id="Edit_a_last_login_time" placeholder="上次登入時間" value="" readonly="readonly">
                                                                        </li>
                                                                        <li>
                                                                                <span>註冊時間 </span>
                                                                                <input type="text" class="necessary long" id="Edit_a_registration_time" placeholder="註冊時間" value="" readonly="readonly">
                                                                        </li>
                                                                </ul>
                                                        </form>
                                                </nav>

                                                <form method="get" action=""  class="center">
                                                        <a id="myModalEditAccount_Yes" href="#" class="button">確認</a>
                                                        <a data-dismiss="modal" href="#" class="button delet">取消</a>
                                                </form>
                                        </div>
                                </dl>
                        </div>
                </div>
        </div>
        
        <!--delete-->
        <div class="modal fade" id="myModaldelete" aria-hidden="false">
                <div class="modal-dialog">
                        <div id="dialog-msg">
                                <dl>
                                        <div class="list">
                                                <h2>刪除會員</h2>
                                                <p class="words"><strong>注意：刪除就不能復原了</strong></p>
                                                <form method="get">
                                                        <nav>
                                                                <a data-dismiss="modal" id="myModaldelete_yes" class="button" type="button">確定</a>
                                                                <a data-dismiss="modal" class="button delete" type="button">取消</a>
                                                        </nav>
                                                </form>
                                        </div>
                                </dl>
                        </div>
                </div>
        </div>
        
        <script>
            
        function unconnected_callback() {
                loading_ajax_hide();
                show_remind( "請先登入" , "error"  );
        };
        function connected_callback( member ) {
                if( member.a_admin !== "true" ){
                        loading_ajax_hide();
                        show_remind( "不是管理者，三秒後轉跳到首頁。" , "error" );
                        setTimeout( function(){ location.href = "../index.php" }, 3000);
                }
                else{
                        init();
                }
        };
        
        </script>
</body>
</html>
