<?php 
if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Interop\Container\ContainerInterface as ContainerInterface;

require __DIR__ . '/../vendor/autoload.php';
//
spl_autoload_register(function ($classname) {
    if (is_file( __DIR__ ."/../src/controllers/" . $classname . ".php")) {
        require (  __DIR__ ."/../src/controllers/" . $classname . ".php");    
    }
    if (is_file( __DIR__ ."/../src/middlewares/" . $classname . ".php")) {
        require (  __DIR__ ."/../src/middlewares/" . $classname . ".php");    
    }    
    if (is_file( __DIR__ ."/../src/models/" . $classname . ".php")) {
        require (  __DIR__ ."/../src/models/" . $classname . ".php");    
    }        
    if (is_file( __DIR__ ."/../src/lib/" . $classname . ".php")) {
        require (  __DIR__ ."/../src/lib/" . $classname . ".php");    
    }        
    
});

session_start();

// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);

// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Register middleware
require __DIR__ . '/../src/middleware.php';

// Register routes
require __DIR__ . '/../src/routes.php';

require __DIR__ . '/../src/global.php';

// Run app
$app->run();

// Test
