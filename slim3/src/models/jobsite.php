<?php
use \Interop\Container\ContainerInterface as ContainerInterface;

class Jobsite extends BasicModel {

    protected $ci;
    protected $db;

    function __construct(ContainerInterface $ci) {
        parent::__construct($ci);
        $this->db = $ci->db;
    }

    /**
     * 取得jobsite資料 By Id
     * @param string $id 編號
     * @return object jobsite資料
     */
    public function getById($id) {
        $sql ="select "
                ." CONCAT( COALESCE(customerContact.contactinfo_Name, ''), ' ', COALESCE(customerContact.contactinfo_PhoneNumber, '')) as createContact, "
                ." CONCAT( COALESCE(a.address_PostalCode, ''), ' ', COALESCE(a.address_Address, ''), ' ', COALESCE(a.address_City, ''), ' ', COALESCE(a.address_Latitude, ''), ' ', COALESCE(a.address_Longitude, '')) as address, "
                ." a.address_ID as jobsite_Address_address_ID, a.address_Address as jobsite_Address_address_Address, a.address_City as jobsite_Address_address_City, a.address_Province as jobsite_Address_address_Province, a.address_PostalCode as jobsite_Address_address_PostalCode, "
                ." a.address_Latitude as jobsite_Address_address_Latitude, a.address_Longitude as jobsite_Address_address_Longitude,"
                ." CONCAT( emergency.contactinfo_Name, ' ', IF(emergency.contactinfo_Extension='', '', CONCAT('(',emergency.contactinfo_Extension, ')')), emergency.contactinfo_PhoneNumber) as emergency, "
                ." CONCAT( foreman.contactinfo_Name, ' ', IF(foreman.contactinfo_Extension='', '', CONCAT('(',foreman.contactinfo_Extension, ')')), foreman.contactinfo_PhoneNumber) as foreman, "
                ." CONCAT( supervisor.contactinfo_Name, ' ', IF(supervisor.contactinfo_Extension='', '', CONCAT('(',supervisor.contactinfo_Extension, ')')), supervisor.contactinfo_PhoneNumber) as supervisor, "
                ." CONCAT( superintend.contactinfo_Name, ' ', IF(superintend.contactinfo_Extension='', '', CONCAT('(',superintend.contactinfo_Extension, ')')), superintend.contactinfo_PhoneNumber) as superintend, "
                ." job.*, c.*, "
                ." emergency.contactinfo_ID as Emergency_contactinfo_ID, emergency.contactinfo_Name as Emergency_contactinfo_Name, emergency.contactinfo_Title as Emergency_contactinfo_Title, emergency.contactinfo_PhoneNumber as Emergency_contactinfo_PhoneNumber, emergency.contactinfo_Extension as Emergency_contactinfo_Extension, "
                ." foreman.contactinfo_ID as Foreman_contactinfo_ID, foreman.contactinfo_Name as Foreman_contactinfo_Name, foreman.contactinfo_Title as Foreman_contactinfo_Title, foreman.contactinfo_PhoneNumber as Foreman_contactinfo_PhoneNumber, foreman.contactinfo_Extension as Foreman_contactinfo_Extension, "
                ." supervisor.contactinfo_ID as Supervisor_contactinfo_ID, supervisor.contactinfo_Name as Supervisor_contactinfo_Name, supervisor.contactinfo_Title as Supervisor_contactinfo_Title, supervisor.contactinfo_PhoneNumber as Supervisor_contactinfo_PhoneNumber, supervisor.contactinfo_Extension as Supervisor_contactinfo_Extension, "
                ." superintend.contactinfo_ID as Superintend_contactinfo_ID, superintend.contactinfo_Name as Superintend_contactinfo_Name, superintend.contactinfo_Title as Superintend_contactinfo_Title, superintend.contactinfo_PhoneNumber as Superintend_contactinfo_PhoneNumber, superintend.contactinfo_Extension as Superintend_contactinfo_Extension, "
                ." creater.userinfo_UserName as creater, updater.userinfo_UserName as updater, "
                ." COALESCE(DATE_FORMAT(job.jobsite_StartDateTime, '%Y-%m-%d %H:%i'), '-') as jobsite_StartDateTime, "
                ." DATE_FORMAT(job.jobsite_CreateDateTime, '%Y-%m-%d %H:%i') as jobsite_CreateDateTime, "
                ." DATE_FORMAT(job.jobsite_UpdateDateTime, '%Y-%m-%d %H:%i') as jobsite_UpdateDateTime "
            ." from jobsite as job "
            ." inner join customerinfo as c on job.jobsite_CustomerID = c.customerInfo_ID "
            ." inner join contactinfo as customerContact on c.customerInfo_ContactID = customerContact.contactinfo_ID and customerContact.contactinfo_PrimaryTable = 'customerinfo' "
            ." inner join address as a on job.jobsite_AddressID = a.address_ID "
            ." inner join contactinfo as emergency on job.jobsite_EmergencyContactID = emergency.contactinfo_ID and emergency.contactinfo_PrimaryTable = 'jobsite' "
            ." inner join contactinfo as foreman on job.jobsite_ForemanContactID = foreman.contactinfo_ID and foreman.contactinfo_PrimaryTable = 'jobsite' "
            ." inner join contactinfo as supervisor on job.jobsite_SupervisorContactID = supervisor.contactinfo_ID and supervisor.contactinfo_PrimaryTable = 'jobsite' "
            ." inner join contactinfo as superintend on job.jobsite_SuperintendContactID = superintend.contactinfo_ID and superintend.contactinfo_PrimaryTable = 'jobsite' "
            ." inner join userinfo as creater on job.jobsite_CreateByID = creater.userinfo_ID "
            ." left join userinfo as updater on job.jobsite_UpdateByID = updater.userinfo_ID "
            ." where job.jobsite_ID = '".$id."'";
        $stmt = $this->db->prepare($sql);
        if ($stmt->execute()) {
            return (array)$stmt->fetch(PDO::FETCH_OBJ);
        } else {
            return false;
        }
    }

    /**
     * 建立jobsite資料
     * @param object $jobstieData ex:array("欄位名稱"=> "欄位值")
     * @return object jobstie資料
     */
    public function create($jobstieData, $relateData) {
        $dbh = $this->ci->db;
        $dbh->beginTransaction(); 
        try { 
            $prepare = $this->prepareInsertSQL("jobsite", $jobstieData);
            $stmt = $dbh->prepare($prepare["sql"]);
            $stmt->execute($prepare["val"]);
            $jobsiteId = $dbh->lastInsertId();
            $emergencyCId = $this->createContact("jobsite", $jobsiteId, $relateData["jobsite_Emergency"]);
            $supervisorCId = $this->createContact("jobsite", $jobsiteId, $relateData["jobsite_Supervisor"]);
            $superintendCId = $this->createContact("jobsite", $jobsiteId, $relateData["jobsite_Superintend"]);
            $foremanCId = $this->createContact("jobsite", $jobsiteId, $relateData["jobsite_Foreman"]);
            $addressId = $this->createAddress($relateData["jobsite_Address"]);

            $updateJobsiteData = array(
                "jobsite_JoinID" => "C".$jobstieData["jobsite_CustomerID"]."J$jobsiteId"
                ,"jobsite_EmergencyContactID" => $emergencyCId
                ,"jobsite_SupervisorContactID" => $supervisorCId
                ,"jobsite_SuperintendContactID" => $superintendCId
                ,"jobsite_ForemanContactID" => $foremanCId
                ,"jobsite_AddressID" => $addressId
            );

            $updateSQL = $this->prepareUpdate("jobsite", $updateJobsiteData, "`jobsite_ID`='".$jobsiteId."'");
            $stmt = $dbh->prepare($updateSQL);
            $stmt->execute();
            $dbh->commit(); 

            return $this->getById($jobsiteId);
        } catch(PDOExecption $e) { 
            $dbh->rollback();
            return $e->getMessage();
        }
    }

    /**
     * 修改jobsite資料
     * @param object $jobstieData ex:array("欄位名稱"=> "欄位值")
     * @return object jobstie資料
     */
    public function updateById($jobstieData, $relateData) {
        $dbh = $this->ci->db;
        $dbh->beginTransaction();
        try {
            $jobsiteId = $jobstieData["jobsite_ID"];
            $prepare = $this->prepareUpdate("jobsite", $jobstieData, "`jobsite_ID`='".$jobsiteId."'");
            $stmt = $dbh->prepare($prepare);
            $stmt->execute();
            $this->updateAddress($relateData["jobsite_Address"], "`address_ID`='".$relateData["jobsite_Address"]["address_ID"]."'");
            $this->updateContact($relateData["jobsite_Emergency"], "`contactinfo_ID`='".$relateData["jobsite_Emergency"]["contactinfo_ID"]."'");
            $this->updateContact($relateData["jobsite_Supervisor"], "`contactinfo_ID`='".$relateData["jobsite_Supervisor"]["contactinfo_ID"]."'");
            $this->updateContact($relateData["jobsite_Superintend"], "`contactinfo_ID`='".$relateData["jobsite_Superintend"]["contactinfo_ID"]."'");
            $this->updateContact($relateData["jobsite_Foreman"], "`contactinfo_ID`='".$relateData["jobsite_Foreman"]["contactinfo_ID"]."'");
            $dbh->commit();
            return $this->getById($jobsiteId);
        } catch(PDOExecption $e) { 
            $dbh->rollback();
            return $e->getMessage();
        }
    }

    public function createContact($tableName, $pk, $contactData) {
        $dbh = $this->ci->db;
        $contactData["contactinfo_PrimaryTable"] = $tableName;
        $contactData["contactinfo_PrimaryID"] = $pk;
        $prepare = $this->prepareInsertSQL("contactinfo", $contactData);
        $stmt = $dbh->prepare($prepare["sql"]);
        $stmt->execute($prepare["val"]);
        return $dbh->lastInsertId();
    }

    public function updateContact($contactData, $condition) {
        $dbh = $this->ci->db;
        $updateSQL = $this->prepareUpdate("contactinfo", $contactData, $condition);
        $stmt = $dbh->prepare($updateSQL);
        $stmt->execute();
    }

    public function createAddress($addressData) {
        $dbh = $this->ci->db;
        $prepare = $this->prepareInsertSQL("address", $addressData);
        $stmt = $dbh->prepare($prepare["sql"]);
        $stmt->execute($prepare["val"]);
        return $dbh->lastInsertId();
    }

    public function updateAddress($addressData, $condition) {
        $dbh = $this->ci->db;
        $updateSQL = $this->prepareUpdate("address", $addressData, $condition);
        $stmt = $dbh->prepare($updateSQL);
        $stmt->execute();
    }
}

?>
