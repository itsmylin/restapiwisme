<?php
use \Interop\Container\ContainerInterface as ContainerInterface;

class Punch extends BasicModel {

    protected $ci;
    protected $db;

    function __construct(ContainerInterface $ci) {
        parent::__construct($ci);
        $this->db = $ci->db;
    }

    /**
     * 取得punch資料 By $employee_ID, $date
     * (Operator一天只會有一筆punch資料)
     * @param string $employee_ID 員工id(資料庫流水號)
     * @param string $date 時間(年-月-日)
     * @return array(
            "success" => true/false
            "data" => success:true才有punch資料
            "msg" => success:false才有錯誤訊息
       )
     */
    public function getPunch($employee_ID, $date) {
        $sql = " select * from punch where employeeinfo_ID = '".$employee_ID."' "
              ." and DATE_FORMAT(punch_CreateDateTime, '%Y-%m-%d') = '".$date."'";
        $this->ci->logger->info($sql);
        $stmt = $this->db->prepare($sql);

        if ($stmt->execute()) {
            if($stmt->rowCount() > 0) {
                return array(
                    "success" => true
                    ,"data" => (array)$stmt->fetch(PDO::FETCH_OBJ)
                );
            } else {
                return array(
                    "success" => false
                    ,"msg" => 'empty data'
                );
            }
        } else {
            return array(
                "success" => false
                ,"msg" => $stmt->errorInfo()
            );
        }
    }

    /**
     * 取得punch資料 By $id
     * (Operator一天只會有一筆punch資料)
     * @param string $id punchId
     * @return array(
            "success" => true/false
            "data" => success:true才有punch資料
            "msg" => success:false才有錯誤訊息
       )
     */
    public function getById($id) {
        $sql = " select * from punch where punch_ID = '".$id."' ";
        $stmt = $this->db->prepare($sql);

        if ($stmt->execute()) {
            if($stmt->rowCount() > 0) {
                return array(
                    "success" => true
                    ,"data" => (array)$stmt->fetch(PDO::FETCH_OBJ)
                );
            } else {
                return array(
                    "success" => false
                    ,"msg" => 'empty data'
                );
            }
        } else {
            return array(
                "success" => false
                ,"msg" => $stmt->errorInfo()
            );
        }
    }

    /**
     * 建立punch資料
     * @param string $employee_ID 員工id(資料庫流水號)
     * @param string $date 時間(年-月-日)
     * @return array(
            "success" => true/false
            "data" => success:true才有punch資料
            "msg" => success:false才有錯誤訊息
       )
     */
    public function create($punchData) {
        $dbh = $this->ci->db;
        $dbh->beginTransaction(); 
        try { 
            $prepare = $this->prepareInsertSQL("punch", $punchData);
            $stmt = $dbh->prepare($prepare["sql"]);
            $stmt->execute($prepare["val"]);
            $punchId = $dbh->lastInsertId();
            $dbh->commit(); 

            return $this->getById($punchId);
        } catch(PDOExecption $e) { 
            $dbh->rollback();
            return array(
                "success" => false
                ,"msg" => $e->getMessage()
            );
        }
    }

    /**
     * 修改punch資料
     * @param object $updateDate ex:array("欄位名稱"=> "欄位值")
     * @param object $punchId punch id
     * @return array(
            "success" => true/false
            "data" => success:true才有punch資料
            "msg" => success:false才有錯誤訊息
       )
     */
    public function update($updateDate, $punchId) {
        $dbh = $this->ci->db;
        $dbh->beginTransaction();
        try {
            $prepare = $this->prepareUpdate("punch", $updateDate, "`punch_ID`='".$punchId."'");
            $stmt = $dbh->prepare($prepare);
            $stmt->execute();
            $dbh->commit();

            return $this->getById($punchId);
        } catch(PDOExecption $e) { 
            $dbh->rollback();
            return array(
                "success" => false
                ,"msg" => $e->getMessage()
            );
        }
    }
}

?>
