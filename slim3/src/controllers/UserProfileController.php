<?php
use \Interop\Container\ContainerInterface as ContainerInterface;
use Slim\Http\UploadedFile;

require_once ( __DIR__ . "/../models/console.php");

class UserProfileController extends BasicController {

    public function __construct(ContainerInterface $ci) {
        parent::__construct("Console", $ci);
        $this->ci = $ci;
        $this->db = $ci->db;
        $this->consoleM = new Console($ci);
    }

    public function getByTable($request, $response, $args) {
        $data = $request->getQueryParams();
        $table = explode(".", $data['userinfo_oTableColumn']);
        $d = $this->consoleM->getElseConsole($table[0], "`userinfo_id`='".$data['userinfo_id']."'");
        if(count($d) > 1) {
            $result = array(
                "userinfo_ID" => $data['userinfo_id']
                , "userinfo_value" => end($d)
                , "userinfo_oTableColumn" => $data['userinfo_oTableColumn']
            );
        } else {
            $result = array(
                "userinfo_value" => ""
                , "userinfo_oTableColumn" => $data['userinfo_oTableColumn']
            );
        }
        $r = $d?array("success"=> true, "data"=> $result):array("success"=> false, "result"=> $d);
        return $this->jsonResponse($response, $r);
    }

    public function createUpdateByTable($request, $response, $args) {
        $data = $request->getParsedBody();
        $table = explode(".", $data['userinfo_oTableColumn']);
        $elseConsoleData = array(
                               "userinfo_id" => $data["userinfo_id"]
                               , $table[1] => $data["userinfo_value"]
                           );
        $get = $this->consoleM->getElseConsole($table[0], "`userinfo_id`='".$data['userinfo_id']."'");
        if(count($get) > 1) {
            $d = $this->consoleM->updateElseConsole($table[0], $elseConsoleData, $data["userinfo_id"]);
        } else {
            $d = $this->consoleM->createElseConsole($table[0], $elseConsoleData, $data["userinfo_id"]);
        }
        $r = $d?array("success"=> true):array("success"=> false);
        return $this->jsonResponse($response, $r);
    }
}
