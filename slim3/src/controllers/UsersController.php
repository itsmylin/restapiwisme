<?php
use \Interop\Container\ContainerInterface as ContainerInterface;

define("kUserInfoTableName", "userinfo");

class UsersController extends BasicController {

    private $db;
    private $authController;

    //Constructor
    public function __construct(ContainerInterface $ci) {
        parent::__construct(kUserInfoTableName, $ci);
        $this->db = $ci->db;
        $this->authController = new AuthenticationController($ci);
    }

//    public function collection($request, $response, $args) {
//        $this->ci->logger->info("Get user list");
//        // Testing AuthInfo from Middleware 
////        return $this->jsonResponse($response, $request->getAttribute(kMiddlewareAuthInfoKey));        
//        // Data Processing
//        $data = $this->getTableData();
//        return $this->jsonResponse($response, $data);
//    }

    public function getUserInfo($request, $response, $args) {
        $this->ci->logger->info("Get user Info");
        // Testing AuthInfo from Middleware 
        $data = $request->getAttribute(kMiddlewareAuthInfoKey);
        $data = $data[0];
        $callback = [];
        $callback["success"] = true;
//        $data["userinfo_Email"] .= $data["userinfo_email_auth"]==="1" ? "( Authenticate success )" : "( Authenticate fail )";
        $data["userinfo_email_auth"] = $data["userinfo_email_auth"]==="1" ? "success" : "fail";
//        unset($data["userinfo_email_auth"]);
        unset($data["userinfo_Password"]);
        
        $callback["data"] = $data;
        // Data Processing
//        $data = $this->getTableData();
        return $this->jsonResponse($response, $callback);
    }

    public function toDatatable($request, $response, $args) {
        $this->ci->logger->info("Get user list to Datatable");
        
        $allGetVars = $request->getQueryParams();
        if ( !($this->checkEmpty($allGetVars,array("length","order","search"))) || !($this->checkIsset($allGetVars,array("start"))) )
        {
            return $this->parameterErrorResponse($response);
        }
        $filterGetData = [];
        $filterGetData['length'] = $allGetVars['length'];
        $filterGetData['order'] = $allGetVars['order'];
        $filterGetData['search'] = $allGetVars['search'];
        $filterGetData['start'] = $allGetVars['start'];
        //search keyWord
        if(isset($allGetVars['searchKey'])) {
            //長度為5且為數字 = 只搜尋No
            if(is_numeric($allGetVars['searchKey']) && strlen($allGetVars['searchKey']) == 5) {
                $filterGetData['searchKey'] = " where u.userinfo_ID = '".(int)$allGetVars['searchKey']."'";
            } else {
                $filterGetData['searchKey'] = " where u.userinfo_UserName like '%".$allGetVars['searchKey']."%'"
                                          ." or e.employeeinfo_FirstName like '%".$allGetVars['searchKey']."%' "
                                          ." or e.employeeinfo_MiddleName like '%".$allGetVars['searchKey']."%' "
                                          ." or e.employeeinfo_LastName like '%".$allGetVars['searchKey']."%' "
                                          ." or u.userinfo_Status = '".$allGetVars['searchKey']."' "
                                          ." or u.userinfo_InExUser like '%\"".$allGetVars['searchKey']."\"%' ";
            }
        } else {
            // $filterGetData['search'] = $allGetVars['search'];
            $filterGetData['searchKey'] = "";
        }

        // Data Processing
        $stmt = $this->getTableDataToDataTable($filterGetData
                                        , "u.userinfo_ID, u.userinfo_UserName, CONCAT(e.employeeinfo_FirstName, ' ', if(ISNULL(e.employeeinfo_MiddleName), '', e.employeeinfo_MiddleName), ' ', e.employeeinfo_LastName), u.userinfo_InExUser, u.userinfo_Status, DATE_FORMAT(u.userinfo_lastLoginTime, '%Y-%m-%d %H:%i') as userinfo_lastLoginTime, CONCAT(u.userinfo_ID, ',', u.userinfo_Status) "
                                        ,"userinfo as u left join employeeinfo as e on u.employee_ID = e.employeeinfo_ID");
//        print_r($response);
//        print_r($response);
//        print_r($stmt);
        return $this->jsonResponse($response, $stmt);
    }

    public function getUserByID($request, $response, $args) {
        $this->ci->logger->info("Get user by ID");
        // Data Processing
//        $stmt = $this->ci->db->prepare("SELECT * FROM userinfo WHERE userinfo_id =\"" . $args["id"] . "\"");
//        $stmt->execute();
//        $stmt = $stmt->fetchAll(PDO::FETCH_OBJ);

        $check = DatabaseHelper::selectMysql($this->db, $this->tableName." u"
                . " LEFT JOIN userinfo u2 on u.userinfo_CreateByID=u2.userinfo_ID"
                . " LEFT JOIN userinfo u3 on u.userinfo_UpdateByID=u3.userinfo_ID", array("u.userinfo_id"=>$args["id"]),""
                    ,"u.*,u2.userinfo_UserName 'userinfo_CreateByUserName',u2.userinfo_Email 'userinfo_CreateByEmail'"
                    . ",u3.userinfo_UserName 'userinfo_UpdateByUserName',u3.userinfo_Email 'userinfo_UpdateByEmail'"
                    . ", DATE_FORMAT(u.userinfo_CreateDateTime, '%Y-%m-%d %H:%i') as userinfo_CreateDateTime "
                    . ", DATE_FORMAT(u.userinfo_UpdateDateTime, '%Y-%m-%d %H:%i') as userinfo_UpdateDateTime ");
        if (!$check) {
                return $this->jsonResponse($response, array("success"=>false,"msg"=>"employeeinfo_ID不存在"));
        }
        
        if (!$check) {
                return $this->jsonResponse($response, array("success"=>false,"msg"=>"userinfo_ID OR employeeinfo_ID 不存在"));
        }
        $check[0]["userinfo_CreateByID"] = array( "userinfo_CreateByID" => $check[0]["userinfo_CreateByID"] , "userinfo_CreateByUserName" => $check[0]["userinfo_CreateByUserName"] , "userinfo_CreateByEmail" => $check[0]["userinfo_CreateByEmail"] );
        $check[0]["userinfo_UpdateByID"] = array( "userinfo_UpdateByID" => $check[0]["userinfo_UpdateByID"] , "userinfo_UpdateByUserName" => $check[0]["userinfo_UpdateByUserName"] , "userinfo_UpdateByEmail" => $check[0]["userinfo_UpdateByEmail"] );
        unset($check[0]["userinfo_CreateByUserName"]);
        unset($check[0]["userinfo_CreateByEmail"]);
        unset($check[0]["userinfo_UpdateByUserName"]);
        unset($check[0]["userinfo_UpdateByEmail"]);
        return $this->jsonResponse($response, array("success"=>true,"data"=>$check[0]));
        
    }

    public function getEmployeesSimple($request, $response, $args) {
        $this->ci->logger->info("Get Employees Simple");
        // Data Processing
        $stmt = $this->ci->db->prepare("SELECT employeeinfo_ID as id, ".
                                       "CONCAT(employeeinfo_FirstName, ' ', if(ISNULL(employeeinfo_MiddleName), '', employeeinfo_MiddleName), ' ', employeeinfo_LastName, ' ( ', employeeinfo_PhoneNumber,' ) ') as text FROM employeeinfo");
        $stmt->execute();
        $stmt = $stmt->fetchAll(PDO::FETCH_OBJ);

        return $this->jsonResponse($response, $stmt);
    }
    
    public function createUser($request, $response, $args) {
        $this->ci->logger->info("Create user");
        
        $allPostPutVars = $request->getParsedBody();
        if ( !($this->checkEmpty($allPostPutVars,array("userinfo_UserName","userinfo_Password","userinfo_Email"))) )
        {
            return $this->parameterErrorResponse($response);
        }
        $filterPostData = [];
        $filterPostData['userinfo_UserName'] = $allPostPutVars['userinfo_UserName'];
        $filterPostData['userinfo_Password'] = $allPostPutVars['userinfo_Password'];
        $filterPostData['userinfo_Email'] = $allPostPutVars['userinfo_Email'];
        
        $check = DatabaseHelper::selectMysql($this->db, $this->tableName, array("userinfo_UserName"=>$filterPostData['userinfo_UserName']));
        if ($check) {
                return $this->jsonResponse($response, array("success"=>false,"msg"=>"帳號重複註冊"));
        }
        $check = DatabaseHelper::selectMysql($this->db, $this->tableName, array("userinfo_Email"=>$filterPostData['userinfo_Email']));
        if ($check) {
                return $this->jsonResponse($response, array("success"=>false,"msg"=>"Email重複註冊"));
        }
        
        $token = getRandom(20);
        $filterPostData["userinfo_email_auth"]=$token;
        //權限(Administrator/Operator/Dispatcher/Accounting/Sales)
        //, :userinfo_Password, :u_name, :userinfo_Email, :u_role, :userinfo_CreateDateTime
        $sql = "INSERT INTO " . $this->tableName . " (userinfo_InExUser,userinfo_UserName,userinfo_Password,userinfo_Email,userinfo_Status,userinfo_email_auth,userinfo_CreateDateTime) 
                        VALUES ('Accounting',:userinfo_UserName,:userinfo_Password,:userinfo_Email,'active',:userinfo_email_auth,:userinfo_CreateDateTime)";
//        echo $sql;
//        print_r($filterPostData);
//        $execute = DatabaseHelper::execute($this->db, $sql);
//        $stmt = $this->ci->db->prepare("SELECT * FROM " . $this->table . " WHERE u_id =\"" . $args["id"] . "\"");
//        $stmt->execute();
//        $stmt = $stmt->fetchAll(PDO::FETCH_OBJ);
        $stmt = $this->ci->db->prepare($sql);
        $filterPostData["userinfo_CreateDateTime"]=date('Y-m-d H:i:s');
        if ($stmt->execute($filterPostData)) {
//                $callback["success"]=true;
                // send auth mail
                $callback = $this -> send("authenticate",$token,"http://www.oort.com.tw/csc",'[SCS] Email 地址驗證',$allPostPutVars["userinfo_Email"]);
                
        } else {
                $callback["success"]=false;
                $callback["msg"]="Insert data fail";
        }
        
        return $this->jsonResponse($response, $callback);
        
    }
    
    public function createUserByAdmin($request, $response, $args) {
        $this->ci->logger->info("Create user");
        
        $allPostPutVars = $request->getParsedBody();
        
        $needParameters = array("userinfo_UserName","userinfo_Password","userinfo_Email","employee_ID","userinfo_InExUser");
        $notNeccParameters = array("userinfo_Remarks");
        if ( !($this->checkEmpty($allPostPutVars,$needParameters)) || !($this->checkIsset($allPostPutVars,$notNeccParameters)) )
        {
            return $this->parameterErrorResponse($response);
        }
        
        $filterPostData = [];
        $usersInfo = $request->getAttribute(kMiddlewareAuthInfoKey);
        $filterPostData["userinfo_CreateByID"]=$usersInfo[0]["userinfo_ID"];
        $filterPostData["userinfo_CreateDateTime"]=date('Y-m-d H:i:s');
        foreach ($needParameters as $key => $value) {
            $filterPostData[$value] = $value==="userinfo_InExUser" ? json_encode($allPostPutVars[$value]) : $allPostPutVars[$value];
        }
        foreach ($notNeccParameters as $key => $value) {
            $filterPostData[$value] = $allPostPutVars[$value];
        }
        
        $check = DatabaseHelper::selectMysql($this->db, $this->tableName, array("userinfo_UserName"=>$filterPostData['userinfo_UserName']));
        if ($check) {
                return $this->jsonResponse($response, array("success"=>false,"msg"=>"帳號重複註冊"));
        }
        $check = DatabaseHelper::selectMysql($this->db, $this->tableName, array("userinfo_Email"=>$filterPostData['userinfo_Email']));
        if ($check) {
                return $this->jsonResponse($response, array("success"=>false,"msg"=>"Email重複註冊"));
        }
        
        $userinfo_Password =$filterPostData["userinfo_Password"];
        $token = getRandom(20);
        $filterPostData["userinfo_email_auth"]=$token;
        $filterPostData["userinfo_Password"]=base64_encode(md5($userinfo_Password));
        
        //權限(Administrator/Operator/Dispatcher/Accounting/Sales)
        //, :userinfo_Password, :u_name, :userinfo_Email, :u_role, :userinfo_CreateDateTime
        $sql = "INSERT INTO " . $this->tableName . " (userinfo_InExUser,employee_ID,userinfo_UserName,userinfo_Password,userinfo_Email,userinfo_Status,userinfo_email_auth,userinfo_CreateDateTime,userinfo_CreateByID,userinfo_Remarks) 
                        VALUES (:userinfo_InExUser,:employee_ID,:userinfo_UserName,:userinfo_Password,:userinfo_Email,'active',:userinfo_email_auth,:userinfo_CreateDateTime,:userinfo_CreateByID,:userinfo_Remarks)";
        
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute($filterPostData)) {
                $callback["success"]=true;
                
                $extra = '<p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">您的帳號是：'.$filterPostData['userinfo_UserName'].'，</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>'
                        . '<p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">您的密碼是：'.$userinfo_Password.'，祝您使用愉快！</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>';
                $callback = $this -> send("authenticate",$token,"http://www.oort.com.tw/csc",'[SCS] Email 地址驗證',$allPostPutVars["userinfo_Email"],$extra);
                
        } else {
                $callback["success"]=false;
                $callback["msg"]="Insert data fail";
        }
        
        return $this->jsonResponse($response, $callback);
        
    }

    public function udpateUserByID($request, $response, $args) {
        
    }

    public function deleteUserByID($request, $response, $args) {
        
    }

    /**
     * 登入
     * @param string $id user.id
     * @return  test
     */
    public function userLogin($request, $response, $args) {
        $callback = array();
        $allPostPutVars = $request->getParsedBody();
        if ( empty($allPostPutVars['userinfo_Email']) )
        {
            return $this->parameterErrorResponse($response);
        }
        $filterPostData = [];
        $filterPostData['userinfo_Email'] = $allPostPutVars['userinfo_Email'];
        $auth = $request->getHeaderLine("Authorization");
        $pwd = trim(substr($auth, 5));
        $filterPostData['userinfo_Password'] = $pwd;
        
        $check = DatabaseHelper::select($this->db, $this->tableName, $filterPostData);
        if (!$check) {
            $callback["success"] = false;
            $callback["msg"] = "Email or Password Error";//"帳號或密碼錯誤";
            return $this->jsonResponse($response, $callback);
        }
        
        $user = $check[0];
        
        if( $user["userinfo_Status"] === "inactive" ){
            $callback["success"] = false;
            $callback["msg"] = "your account has been deleted";
            return $this->jsonResponse($response, $callback);
        }
        
        if ( !empty($user["userinfo_ID"]) )
        {
            $token = $this->authController->createToken($user["userinfo_ID"]);
            if ($token === false) {
                return $this->parameterErrorResponse($response);
            }

            $callback['data'] = $token;
            $callback['success'] = true;
            return $this->jsonResponse($response, $callback);            
        }
        
        return $this->parameterErrorResponse($response);
    }
    
    public function getPermissionsByToken($request, $response, $args) {
        $this->ci->logger->info("Get permissions by token");
        // Testing AuthInfo from Middleware 
        $permissions = array();
        $final = array("user"=>"0000","employee"=>"0000","customer"=>"0000","jobsite"=>"0000","workorder"=>"0000","activity"=>"0000","unit"=>"0000");
        $usersInfo = $request->getAttribute(kMiddlewareAuthInfoKey);
        $userinfo_InExUser = json_decode($usersInfo[0]["userinfo_InExUser"],true);
        foreach ($userinfo_InExUser as $key => $value) {
            switch ($value) {
                case "Administrator":
                    $permissions[] = array("user"=>"1110","employee"=>"1110","customer"=>"1110","jobsite"=>"1110","workorder"=>"1110","activity"=>"1110","unit"=>"1110");
                break;
                case "Dispatcher":
                    $permissions[] = array("user"=>"0000","employee"=>"0010","customer"=>"0010","jobsite"=>"1110","workorder"=>"1110","activity"=>"1110","unit"=>"1110");
                break;
                case "Operator":
                    $permissions[] = array("user"=>"0000","employee"=>"0000","customer"=>"0000","jobsite"=>"0000","workorder"=>"0010","activity"=>"1110","unit"=>"0000");
                break;
                case "Accounting":
                    $permissions[] = array("user"=>"0010","employee"=>"1110","customer"=>"1110","jobsite"=>"0000","workorder"=>"1110","activity"=>"1110","unit"=>"1110");
                break;
                case "Sales":
                    $permissions[] = array("user"=>"0000","employee"=>"0010","customer"=>"1110","jobsite"=>"1110","workorder"=>"1110","activity"=>"1110","unit"=>"0000");
                break;
                default:
                    $permissions[] = array("user"=>"0000","employee"=>"0000","customer"=>"0000","jobsite"=>"0000","workorder"=>"0000","activity"=>"0000","unit"=>"0000");
                break;
            }
        }
        
        $callback = array();
        if(count($permissions)===0){
            $callback['data'] = $final;
        }
        else if(count($permissions)===1){
            $callback['data'] = $permissions[0];
        }
        else{
            foreach ($permissions as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    $final_value = $final[$key2];
                    $first_word = (int)substr($final_value, 0, 1) || (int)substr($value2, 0, 1)?1:0;
                    $second_word = (int)substr($final_value, 1, 1) || (int)substr($value2, 1, 1)?1:0;
                    $third_word = (int)substr($final_value, 2, 1) || (int)substr($value2, 2, 1)?1:0;
                    $fourth_word = (int)substr($final_value, 3, 1) || (int)substr($value2, 3, 1)?1:0;
                    $final[$key2]=$first_word.$second_word.$third_word.$fourth_word;
                }
            }
            $callback['data'] = $final;
        }
        
        $callback['success'] = true;
        return $this->jsonResponse($response, $callback);
        
    }
    
    public function checkUserState($request, $response, $args) {
        $this->ci->logger->info("Get check user state");
        // Testing AuthInfo from Middleware 
        $usersInfo = $request->getAttribute(kMiddlewareAuthInfoKey);
        $callback = array();
        $callback['data'] = $usersInfo[0]["userinfo_Status"];
        $callback['success'] = true;
        return $this->jsonResponse($response, $callback);
    }

    public function editMyName($request, $response, $args) {
        $this->ci->logger->info("Edit My Name");
        $callback = array();
        
        $allPostPutVars = $request->getParsedBody();
        if ( !($this->checkEmpty($allPostPutVars,array("userinfo_UserName"))) )
        {
            return $this->parameterErrorResponse($response);
        }
        
        $usersInfo = $request->getAttribute(kMiddlewareAuthInfoKey);
        //未修改
        if($usersInfo[0]['userinfo_UserName']===$allPostPutVars['userinfo_UserName']){
            $callback["success"]=true;
            return $this->jsonResponse($response, $callback);
        }
        
        $check = DatabaseHelper::selectMysql($this->db, $this->tableName, array("userinfo_ID"=>$usersInfo[0]['userinfo_ID']));
        if (!$check) {
                return $this->jsonResponse($response, array("success"=>false,"msg"=>"userinfo_ID不存在"));
        }
        $check = DatabaseHelper::selectMysql($this->db, $this->tableName, array("userinfo_UserName"=>$allPostPutVars['userinfo_UserName']));
        if ($check) {
                return $this->jsonResponse($response, array("success"=>false,"msg"=>"UserName已存在"));
        }
          
        $sql = "UPDATE " . $this->tableName . " SET userinfo_UserName='".$allPostPutVars['userinfo_UserName']."',userinfo_UpdateDateTime='".date('Y-m-d H:i:s')."',userinfo_UpdateByID='".$usersInfo[0]['userinfo_ID']."' WHERE userinfo_ID='".$usersInfo[0]['userinfo_ID']."'";
//        echo $sql;
        $stmt = $this->db->prepare($sql);
        if ($stmt->execute()) {
                $callback["success"]=true;
        } else {
                $callback["success"]=false;
                $callback["msg"]="Update data fail";
        }
        
        return $this->jsonResponse($response, $callback);
        
    }

    public function editMyEmail($request, $response, $args) {
        $this->ci->logger->info("Edit My Email");
        $callback = array();
        
        $allPostPutVars = $request->getParsedBody();
        if ( !($this->checkEmpty($allPostPutVars,array("userinfo_Email","userinfo_Password"))) )
        {
            return $this->parameterErrorResponse($response);
        }
        
        $usersInfo = $request->getAttribute(kMiddlewareAuthInfoKey);
        //密碼驗證
        if($usersInfo[0]['userinfo_Password']!==$allPostPutVars['userinfo_Password']){
            $callback["success"]=false;
            $callback["msg"]="password is error";
            return $this->jsonResponse($response, $callback);
        }
        //未修改
        if($usersInfo[0]['userinfo_Email']===$allPostPutVars['userinfo_Email']){
            $callback["success"]=true;
            return $this->jsonResponse($response, $callback);
        }
        
        $check = DatabaseHelper::selectMysql($this->db, $this->tableName, array("userinfo_ID"=>$usersInfo[0]['userinfo_ID']));
        if (!$check) {
                return $this->jsonResponse($response, array("success"=>false,"msg"=>"userinfo_ID不存在"));
        }
        $check = DatabaseHelper::selectMysql($this->db, $this->tableName, array("userinfo_Email"=>$allPostPutVars['userinfo_Email']));
        if ($check) {
                return $this->jsonResponse($response, array("success"=>false,"msg"=>"UserEmail已存在"));
        }
        
        
        $token = getRandom(20);
        $sql = "UPDATE " . $this->tableName . " SET userinfo_Email='".$allPostPutVars['userinfo_Email']."',userinfo_UpdateDateTime='".date('Y-m-d H:i:s')."',userinfo_UpdateByID='".$usersInfo[0]['userinfo_ID']."',userinfo_email_auth='".$token."' WHERE userinfo_ID='".$usersInfo[0]['userinfo_ID']."'";
//        echo $sql;
        $stmt = $this->db->prepare($sql);
        if ($stmt->execute()) {
                $callback["success"]=true;

                $callback = $this -> send("authenticate",$token,"http://www.oort.com.tw/csc",'[SCS] Email 地址驗證',$allPostPutVars["userinfo_Email"]);
                
        } else {
                $callback["success"]=false;
                $callback["msg"]="Update data fail";
        }
        
        return $this->jsonResponse($response, $callback);
        
    }

    public function editMyPassword($request, $response, $args) {
        $this->ci->logger->info("Edit My Password");
        $callback = array();
        
        $allPostPutVars = $request->getParsedBody();
        if ( !($this->checkEmpty($allPostPutVars,array("userinfo_old_Password","userinfo_new_Password"))) )
        {
            return $this->parameterErrorResponse($response);
        }
        
        $usersInfo = $request->getAttribute(kMiddlewareAuthInfoKey);
        //密碼驗證
        if($usersInfo[0]['userinfo_Password']!==$allPostPutVars['userinfo_old_Password']){
            $callback["success"]=false;
            $callback["msg"]="old password is error";
            return $this->jsonResponse($response, $callback);
        }
        //未修改
        if($allPostPutVars['userinfo_old_Password']===$allPostPutVars['userinfo_new_Password']){
            $callback["success"]=true;
            return $this->jsonResponse($response, $callback);
        }
        
        $check = DatabaseHelper::selectMysql($this->db, $this->tableName, array("userinfo_ID"=>$usersInfo[0]['userinfo_ID']));
        if (!$check) {
                return $this->jsonResponse($response, array("success"=>false,"msg"=>"userinfo_ID不存在"));
        }
          
        $sql = "UPDATE " . $this->tableName . " SET userinfo_Password='".$allPostPutVars['userinfo_new_Password']."',userinfo_UpdateDateTime='".date('Y-m-d H:i:s')."',userinfo_UpdateByID='".$usersInfo[0]['userinfo_ID']."' WHERE userinfo_ID='".$usersInfo[0]['userinfo_ID']."'";
//        echo $sql;
        $stmt = $this->db->prepare($sql);
        if ($stmt->execute()) {
                $callback["success"]=true;
        } else {
                $callback["success"]=false;
                $callback["msg"]="Update data fail";
        }
        
        return $this->jsonResponse($response, $callback);
        
    }
    
    public function editByUserID($request, $response, $args) {
        $this->ci->logger->info("Edit by user ID");
        
        $allPostPutVars = $request->getParsedBody();
        $needParameters = array("userinfo_ID","userinfo_UserName","userinfo_Email","employee_ID","userinfo_InExUser");
        $notNeccParameters = array("userinfo_Remarks","userinfo_Password");
        if ( !($this->checkEmpty($allPostPutVars,$needParameters)) || !($this->checkIsset($allPostPutVars,$notNeccParameters)) )
        {
            return $this->parameterErrorResponse($response);
        }
        
        unset($needParameters[0]);//userinfo_ID
        $filterPostData = [];
        $usersInfo = $request->getAttribute(kMiddlewareAuthInfoKey);
        $filterPostData["userinfo_UpdateByID"]=$usersInfo[0]["userinfo_ID"];
        $filterPostData["userinfo_UpdateDateTime"]=date('Y-m-d H:i:s');
        foreach ($needParameters as $key => $value) {
            $filterPostData[$value] = $value==="userinfo_InExUser"? json_encode($allPostPutVars[$value]) :$allPostPutVars[$value];
        }
        foreach ($notNeccParameters as $key => $value) {
            if($value!=="userinfo_Password"||$allPostPutVars[$value]!=="")
                $filterPostData[$value] = $allPostPutVars[$value];
        }
        
        
        $check = DatabaseHelper::selectMysql($this->db, $this->tableName, array("userinfo_ID"=>$allPostPutVars['userinfo_ID']));
        if (!$check) {
                return $this->jsonResponse($response, array("success"=>false,"msg"=>"userinfo_ID不存在"));
        }
        
        $updateMysql = DatabaseHelper::updateMysql($this->db, $this->tableName, $filterPostData,array("userinfo_ID"=>$allPostPutVars['userinfo_ID']));
        if (!$updateMysql) {
                return $this->jsonResponse($response, array("success"=>false,"msg"=>"儲存失敗"));
        }
        
        $callback = array();
        $callback["success"]=true;
        return $this->jsonResponse($response, $callback);
        
    }

    public function forgotPassword($request, $response, $args) {
        $this->ci->logger->info("forgot Email");
        $callback = array();
        
        $allPostPutVars = $request->getParsedBody();
        if ( !($this->checkEmpty($allPostPutVars,array("userinfo_Email"))) )
        {
            return $this->parameterErrorResponse($response);
        }
        
        if (!filter_var($allPostPutVars["userinfo_Email"], FILTER_VALIDATE_EMAIL)) {
            $callback['msg'] = "email is not valid";
            $callback['success'] = false;
            return $this->jsonResponse($response, $callback);
        }
        
        $rand = getRandom(20);
        $callback = $this -> send("forgot",$rand,"http://www.oort.com.tw/csc",'[SCS] 忘記密碼通知信',$allPostPutVars["userinfo_Email"]);
        
        if($callback["success"]){
            $sql = "UPDATE " . $this->tableName . " SET userinfo_forgot_token='".$rand."' WHERE userinfo_Email='".$allPostPutVars['userinfo_Email']."'";
            $stmt = $this->db->prepare($sql);
            if ($stmt->execute()) {
                    $callback["success"]=true;
            } else {
                    $callback["success"]=false;
                    $callback["msg"]="SQL fail";
            }
        }
        
        return $this->jsonResponse($response, $callback);
        
    }

    public function emailAuthentication($request, $response, $args) {
        $this->ci->logger->info("email Authentication");
        $callback = array();
        
        $allPostPutVars = $request->getParsedBody();
        if ( !($this->checkEmpty($allPostPutVars,array("token"))) )
        {
            return $this->parameterErrorResponse($response);
        }
        
        $check = DatabaseHelper::selectMysql($this->db, $this->tableName, array("userinfo_email_auth"=>$allPostPutVars['token']));
        if (!$check) {
                return $this->jsonResponse($response, array("success"=>false,"msg"=>"此信箱的認證碼已經失效或不存在"));
        }
        
        $sql = "UPDATE " . $this->tableName . " SET userinfo_email_auth='1' WHERE userinfo_ID='".$check[0]['userinfo_ID']."'";
        $stmt = $this->db->prepare($sql);
        if ($stmt->execute()) {
                $callback["success"]=true;
        } else {
                $callback["success"]=false;
                $callback["msg"]="Update data fail";
        }
        
        return $this->jsonResponse($response, $callback);
        
    }

    public function checkForgotToken($request, $response, $args) {
        $this->ci->logger->info("check Forgot Token");
        $callback = array();
        
        $allPostPutVars = $request->getParsedBody();
        if ( !($this->checkEmpty($allPostPutVars,array("token"))) )
        {
            return $this->parameterErrorResponse($response);
        }
        
        $check = DatabaseHelper::selectMysql($this->db, $this->tableName, array("userinfo_forgot_token"=>$allPostPutVars['token']));
        if (!$check) {
                return $this->jsonResponse($response, array("success"=>false,"msg"=>"此認證碼已經失效或不存在"));
        }
        $callback["success"]=true;
        return $this->jsonResponse($response, $callback);
        
    }

    public function editPasswordByForgotToken($request, $response, $args) {
        $this->ci->logger->info("check Forgot Token");
        $callback = array();
        
        $allPostPutVars = $request->getParsedBody();
        if ( !($this->checkEmpty($allPostPutVars,array("token","userinfo_Password"))) )
        {
            return $this->parameterErrorResponse($response);
        }
        
        $check = DatabaseHelper::selectMysql($this->db, $this->tableName, array("userinfo_forgot_token"=>$allPostPutVars['token']));
        if (!$check) {
                return $this->jsonResponse($response, array("success"=>false,"msg"=>"此認證碼已經失效或不存在"));
        }
        
        $sql = "UPDATE " . $this->tableName . " SET userinfo_forgot_token='',userinfo_Password='".$allPostPutVars['userinfo_Password']."' WHERE userinfo_ID='".$check[0]['userinfo_ID']."'";
        $stmt = $this->db->prepare($sql);
        if ($stmt->execute()) {
                $callback["success"]=true;
        } else {
                $callback["success"]=false;
                $callback["msg"]="Update data fail";
        }
        return $this->jsonResponse($response, $callback);
        
    }
    
    
    public function send($mail_type,$rand,$http_default_path,$title,$a_email,$extra_html=""){

            $callback = array();

//            if( !check_empty( array( "mail_type","rand","http_default_path","title","a_email" ) ) ){
//                    $callback['msg'] = "輸入資料不完整";
//                    $callback['success'] = false;
//                    return $callback;
//            }

//            $mail_type = $_REQUEST["mail_type"];
//            $rand = $_REQUEST["rand"];
//            $http_default_path = $_REQUEST["http_default_path"];
//            $title = $_REQUEST["title"];
//            $a_email = $_REQUEST["a_email"];

            if( !in_array($mail_type, array("forgot","authenticate")) ){
                    $callback['msg'] = "輸入資料錯誤";
                    $callback['success'] = false;
                    return $callback;
            }

            if( $mail_type === "forgot" ){
                    $html = '<p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
                     </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">這封信是由</span><span lang="EN-US" style="color:rgb(55,96,146)">AROFLY</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">發送的。</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
                    <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif">------------------------------<wbr>------------------------------<wbr>----------<br>
                    </span><b><span style="font-size:10.5pt;font-family:新細明體,serif">重要！</span></b><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif"><br>
                    ------------------------------<wbr>------------------------------<wbr>----------</span> </p>
                   <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10.5pt;font-family:新細明體,serif">如果您沒有提交密碼重置的請求或不是</span><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif;color:rgb(55,96,146)">AROFLY</span><span style="font-size:10.5pt;font-family:新細明體,serif">的註冊用戶，<wbr>請立即忽略</span><span style="font-size:10.5pt;font-family:Arial,sans-serif"> </span><span style="font-size:10.5pt;font-family:新細明體,serif">並刪除這封郵件。只有在您確認需要重置密碼的情況下，<wbr>才需要繼續閱讀下面的內容。</span><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif"></span> </p>
                   <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif">------------------------------<wbr>------------------------------<wbr>----------<br>
                    </span><b><span style="font-size:10.5pt;font-family:新細明體,serif">密碼重置說明</span></b><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif"><br>
                    ------------------------------<wbr>------------------------------<wbr>----------</span> </p>
                    <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">如果您是</span><span lang="EN-US" style="color:rgb(55,96,146)">AROFLY</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">的新用戶，或在修改您的註冊</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> Email </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">時使用了本地址，<wbr>我們需要對您的地址有效性進行驗證以避免垃圾郵件或地址被濫用。</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
                    <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">您的修改密碼網址為：</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
                    <a target="_blank" href="http://www.oort.com.tw/scs/forgot_password.php?token='.$rand.'">http://www.oort.com.tw/scs/forgot_password.php?token='.$rand.'</a>
                    <br>
                    </span> </p>
                    <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">感謝您的訪問，祝您使用愉快！</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
                    <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">此致</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
                     </span><span lang="EN-US" style="color:rgb(55,96,146)">AROFLY</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">管理團隊</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black">.&nbsp;
                     </span><span style="font-family:新細明體,serif;color:rgb(55,96,146)">'.$http_default_path.'</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
                    <p class="MsoNormal"><span lang="EN-US">&nbsp;</span> </p>
                    <p class="MsoNormal"><span lang="EN-US">&nbsp;</span> </p>';
            }
            else if( $mail_type === "authenticate" ){
                    $html = '<p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
                    </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">這封信是由</span><span lang="EN-US" style="color:rgb(55,96,146)">AROFLY</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">發送的。</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
                   <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">您收到這封郵件，是由於在</span><span style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span lang="EN-US" style="color:rgb(55,96,146)">AROFLY</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">進行了新用戶註冊，或用戶修改</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> Email </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">使用</span><span style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">了這個郵箱地址。如果您並沒有訪問過</span><span style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">，或沒有進行上述操作，請忽略這封郵件。<wbr>您不需要退訂或進行其他進一步的操作。</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
                   <p class="MsoNormal"><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
                    <span style="background-image:initial;background-repeat:initial">------------------------------<wbr>------------------------------<wbr>----------</span>
                       <br> </span><b><span style="font-size:10pt;font-family:新細明體,serif;color:black;background-image:initial;background-repeat:initial">帳號激活說明</span></b><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
                    <span style="background-image:initial;background-repeat:initial">------------------------------<wbr>------------------------------<wbr>----------</span>
                       <br>
                       <br> </span><span lang="EN-US" style="font-family:新細明體,serif"></span> </p>
                   <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">如果您是</span><span lang="EN-US" style="color:rgb(55,96,146)">AROFLY</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">的新用戶，或在修改您的註冊</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> Email </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">時使用了本地址，<wbr>我們需要對您的地址有效性進行驗證以避免垃圾郵件或地址被濫用。</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
                   <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">您的認證網址為：</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
                   <a target="_blank" href="http://www.oort.com.tw/scs/authenticate.php?token='.$rand.'">http://www.oort.com.tw/scs/authenticate.php?token='.$rand.'</a>
                   <br>
                   </span> </p>
                   '.$extra_html.'
                   <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">此致</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
                    </span><span lang="EN-US" style="color:rgb(55,96,146)">AROFLY</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">管理團隊</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black">.&nbsp;
                    </span><span style="font-family:新細明體,serif;color:rgb(55,96,146)">'.$http_default_path.'</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
                   <p class="MsoNormal"><span lang="EN-US">&nbsp;</span> </p>
                   <p class="MsoNormal"><span lang="EN-US">&nbsp;</span> </p>';
            }

            header("Access-Control-Allow-Origin:*");
            header('Access-Control-Allow-Credentials:true');
            header('Access-Control-Allow-Methods:GET, POST, PUT, DELETE, OPTIONS');
            header('Access-Control-Allow-Headers:Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type, X-E4M-With');
            header('Content-Type:text/html; charset=utf-8');
            require '../service/send_mail/gmailsystem/gmail.php';
            mb_internal_encoding('UTF-8');
            $callback = mstart( $a_email , $html , $title );

            return $callback;

    }
    
    /**
     * 修改使用者狀態
     */
    public function switchStatu($request, $response, $args) {      
        //檢查Params格式是否有誤
        $params = $request->getParsedBody();
        $needKey = ["userinfo_ID"];
        for($i=0;$i<count($needKey);$i++) {
            if(!array_key_exists($needKey[$i], $params ) && !empty($params[$needKey[$i]])) {
                return $this->parameterErrorResponse($response);
            }
        }

        //取得使用者狀態
        $sql = "select userinfo_Status from ".$this->tableName." where userinfo_ID = '".$params["userinfo_ID"]."'";
        $stmt = $this->ci->db->prepare($sql);
        $stmt->execute();
        $status = $stmt->fetchAll(PDO::FETCH_ASSOC)[0]["userinfo_Status"];
        //修改狀態
        if($status == "active") {
            $status = "inactive";
        } else {
            $status = "active";
        }
        $sql = "UPDATE ".$this->tableName." SET userinfo_Status='$status' where userinfo_ID = '".$params["userinfo_ID"]."'";
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $callback["success"]=true;
            $callback["status"]=$status;
        } else {
            $callback["success"]=false;
            $callback["msg"]="Update data fail";
        }
        return $this->jsonResponse($response, $callback);        
    }
    
    public function userLogout($request, $response, $args) {
        $this->ci->logger->info("user Logout");
        
        $callback = array();
        $allPostPutVars = $request->getParsedBody();
        
        $usersInfo = $request->getAttribute(kMiddlewareAuthInfoKey);
        $filterPostData["userinfo_UpdateByID"]=$usersInfo[0]["userinfo_ID"];
        
        $sql = "DELETE FROM `authentication` WHERE authentication_token='".$usersInfo[0]["authentication_token"]."'";
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
                $callback["success"]=true;
        } else {
                $callback["success"]=false;
                $callback["msg"]="Insert data fail";
        }
        
        return $this->jsonResponse($response, $callback);
        
    }
    
    public function reSendEmailForAuth($request, $response, $args) {
        $this->ci->logger->info("reSend Email For Auth");
        
        $callback = array();
        $usersInfo = $request->getAttribute(kMiddlewareAuthInfoKey);
        $token = getRandom(20);
        $sql = "UPDATE " . $this->tableName . " SET userinfo_email_auth='".$token."' WHERE userinfo_ID='".$usersInfo[0]['userinfo_ID']."'";
        $stmt = $this->db->prepare($sql);
        if ($stmt->execute()) {
            
                $callback = $this -> send("authenticate",$token,"http://www.oort.com.tw/csc",'[SCS] Email 地址驗證',$usersInfo[0]["userinfo_Email"]);
                if($callback["success"]){
                        $callback["success"]=true;
                        $callback["msg"]="Send EMAIL to ".$usersInfo[0]["userinfo_Email"];
                }
                else {
                        $callback["success"]=false;
                        $callback["msg"]="Send EMAIL fail";
                }
        } else {
                $callback["success"]=false;
                $callback["msg"]="Update data fail";
        }
        return $this->jsonResponse($response, $callback);
        
    }
    
    
    
}
