<?php
use \Interop\Container\ContainerInterface as ContainerInterface;

require_once ( __DIR__ . "/../models/workorder.php");
require_once ( __DIR__ . "/../models/jobsite.php");

class WorkOrderController extends BasicController
{
    private $db;
    //jobsite model
    private $jobsiteM;
    //workorder model
    private $workorderM;

    //Constructor
    public function __construct(ContainerInterface $ci) {
        parent::__construct("workorder", $ci);
        $this->workorderM = new WorkOrder($ci);
        $this->jobsiteM = new Jobsite($ci);
    } 
    
    public function create($request, $response, $args) {
        $data = $request->getParsedBody();
        //取得登入者的資料
        $creater = $this->getLoginUser($request);
        $jobsite = $this->jobsiteM->getById($data["jobsite_ID"]);

        $workorderColumn = ["workorder_JoinID", "workorder_ID", "customer_ID", "jobsite_ID", "workorder_EstStartDateTime"
                        , "workorder_EstHours", "workorder_TypeOfWork", "workorder_DayOfOperation", "workorder_MaterialSupplies"
                        , "workorder_EquipmentTools", "workorder_InternalNotes", "workorder_OperatorFromEmployeeID"
                        , "workorder_HelperFromEmployeeID", "workorder_UnitFromUnitID", "workorder_OperatorNote", "workorder_Rate"
                        , "workorder_BillingHours", "workorder_PO", "workorder_TicketID", "workorder_PaymentTerm"];
        $workorderData = $this->getNeedKeyByObject($workorderColumn, $data);
        $workorderData['workorder_EstStartDateTime'] = $data['workorder_EstStartDate']." ".date("H:i:s", strtotime($data['workorder_EstStartTime']));
        $workorderData['workorder_JoinID'] = $jobsite["jobsite_JoinID"];
        $workorderData['customer_ID'] = $jobsite["jobsite_CustomerID"];
        $workorderData['workorder_Status'] = "Pending";
        $workorderData['workorder_CreateByID'] = $creater["userinfo_ID"];

        $d = $this->workorderM->create($workorderData);
        $r = $d?array("success"=> true, "result"=> $d):array("success"=> false, "result"=> $d);
        return $this->jsonResponse($response, $r);
    }
    
    public function updateById($request, $response, $args) {
        $data = $request->getParsedBody();
        //取得登入者的資料
        $updater = $this->getLoginUser($request);

        $workorderColumn = ["workorder_JoinID", "customer_ID", "jobsite_ID", "workorder_EstStartDateTime"
                        , "workorder_EstHours", "workorder_TypeOfWork", "workorder_DayOfOperation", "workorder_MaterialSupplies"
                        , "workorder_EquipmentTools", "workorder_InternalNotes", "workorder_OperatorFromEmployeeID"
                        , "workorder_HelperFromEmployeeID", "workorder_UnitFromUnitID", "workorder_OperatorNote"
                        , "workorder_StartTime", "workorder_CompleteTime", "workorder_Feedback"
                        , "workorder_Rate", "workorder_BillingHours", "workorder_PO", "workorder_TicketID", "workorder_PaymentTerm"
                        , "workorder_Status"
                        , "workorder_InvoiceID", "workorder_InvoiceDate", "workorder_Purchaser", "workorder_RequestFromPurchaser"
                        ];

        $workorderData = $this->getNeedKeyByObject($workorderColumn, $data);
        if(isset($data['workorder_EstStartDate']) && isset($data['workorder_EstStartTime'])) {
            $workorderData['workorder_EstStartDateTime'] = $data['workorder_EstStartDate']." ".date("H:i:s", strtotime($data['workorder_EstStartTime']));
        }
        if(isset($data["jobsite_ID"])) {
            $jobsite = $this->jobsiteM->getById($data["jobsite_ID"]);
            $workorderData['workorder_JoinID'] = $jobsite["jobsite_JoinID"]."W".$data["workorder_ID"];
            $workorderData['customer_ID'] = $jobsite["jobsite_CustomerID"];
        }

        //設定狀態
        $workorder = $this->workorderM->getById($data["workorder_ID"]);

        if(isset($workorderData["workorder_StartTime"])) {
            if($workorderData["workorder_StartTime"] == "") {
                $workorderData["workorder_StartTime"] = null;
            } else {
                $workorderData["workorder_StartTime"] = $workorder['Date']." ".date("H:i:s", strtotime($workorderData['workorder_StartTime']));
            }
        }
        if(isset($workorderData["workorder_CompleteTime"])) {
            if($workorderData["workorder_CompleteTime"] == "") {
                $workorderData["workorder_CompleteTime"] = null;
            } else {
                $workorderData["workorder_CompleteTime"] = $workorder['Date']." ".date("H:i:s", strtotime($workorderData['workorder_CompleteTime']));
            }
        }
        if(!isset($workorderData['workorder_Status'])) {
            //Pending -> Dispatched
            if($workorder['workorder_Status'] == "Pending" && isset($workorderData["workorder_OperatorFromEmployeeID"]) && isset($workorderData["workorder_UnitFromUnitID"])) {
                $workorderData['workorder_Status'] = "Dispatched";
            }//Dispatched -> In Progress
            else if($workorder['workorder_Status'] == "Dispatched" && isset($workorderData["workorder_StartTime"])) {
                $workorderData["workorder_StartTime"] = 'now()';
                $workorderData['workorder_Status'] = "In Progress";
            }//Dispatched -> Pending
            else if($workorder['workorder_Status'] == "Dispatched" && (!isset($workorderData["workorder_OperatorFromEmployeeID"]) || !isset($workorderData["workorder_UnitFromUnitID"]))) {
                $workorderData["workorder_StartTime"] = null;
                $workorderData["workorder_CompleteTime"] = null;
                $workorderData['workorder_Status'] = "Pending";
            }//In Progress -> Complete
            else if($workorder['workorder_Status'] == "In Progress" && isset($workorderData["workorder_CompleteTime"])) {
                $workorderData["workorder_CompleteTime"] = 'now()';
                $workorderData['workorder_Status'] = "Complete";
            }//Complete -> Invoiced
            else if($workorder['workorder_Status'] == "Complete" && isset($workorderData["workorder_InvoiceID"]) && isset($workorderData["workorder_InvoiceDate"]) && isset($workorderData["workorder_Purchaser"]) && isset($workorderData["workorder_RequestFromPurchaser"])) {
                $workorderData['workorder_Status'] = "Invoiced";
            }
        }
        $workorderData["workorder_UpdateDateTime"] = 'now()';
        if(count($workorderData) > 0) {
            $d = $this->workorderM->updateById($workorderData, $data["workorder_ID"]);
            $r = $d?array("success"=> true, "result"=> $d):array("success"=> false, "result"=> $d);
            return $this->jsonResponse($response, $r);
        } else {
            $r = array("success"=> true, "msg"=> "nothing to update", "result"=> $this->workorderM->getById($data["workorder_ID"]));
            return $this->jsonResponse($response, $r);
        }
    }

    /**
     * 取得workorder by id
     */
    public function getByID($request, $response, $args) {
        $r = $this->workorderM->getById($args["id"]);
        return $this->jsonResponse($response, $r);
    }

    public function toDatatable($request, $response, $args) {
        $params = $request->getQueryParams();
        //search keyWord
        if(isset($params['searchKey'])) {
            $condition = " where w.workorder_JoinID like '%".$params['searchKey']."%' "
                           ." or c.customerInfo_companyName like '%".$params['searchKey']."%' "
                           ." or j.jobsite_Name like '%".$params['searchKey']."%' "
                           ." or w.workorder_TypeOfWork like '%".$params['searchKey']."%' "
                           ." or u.unit_LicensePlate like '%".$params['searchKey']."%' "
                           ." or w.workorder_EquipmentTools like '%".$params['searchKey']."%' "

                           ." or operator.employeeinfo_FirstName like '%".$params['searchKey']."%' "
                           ." or operator.employeeinfo_LastName like '%".$params['searchKey']."%' "
                           ." or helper.employeeinfo_FirstName like '%".$params['searchKey']."%' "
                           ." or helper.employeeinfo_LastName like '%".$params['searchKey']."%' "
                           ." or w.workorder_Status like '%".$params['searchKey']."%' "
                           ." or w.workorder_TicketID like '%".$params['searchKey']."%' ";
        } else {
            $condition = "";
        }

        if(isset($params['startDate']) && isset($params['endDate'])) {
            if($condition == "") {
                $condition .= " where ";
            } else {
                $condition .= " and ";
            }
            $condition .= " w.workorder_EstStartDateTime >= '".$params['startDate']. " 00:00:00' and w.workorder_EstStartDateTime <= '".$params['endDate']." 23:59:59' ";
        }

        //select資料
        $orderBy = " ORDER BY ".((int)$params["order"][0]["column"]+1)." ".$params["order"][0]["dir"];
        $limit = " LIMIT ".$params["start"].", ".$params["length"];
        $sql = "select  SQL_CALC_FOUND_ROWS "
                    ." CONCAT(w.workorder_ID, ',', w.workorder_JoinID) as No "
                    ." , DATE_FORMAT(w.workorder_EstStartDateTime, '%Y-%m-%d') as Date "
                    ." , DATE_FORMAT(w.workorder_EstStartDateTime, '%h:%i %p') as Time "
                    ." , c.customerInfo_companyName as customer "
                    ." , j.jobsite_Name as jobsite "
                    ." , w.workorder_TypeOfWork as TypeOfWork "
                    ." , u.unit_LicensePlate as Unit "
                    ." , REPLACE(w.workorder_EquipmentTools, ',', '<br>') as equipment "
                    ." , CONCAT(COALESCE(operator.employeeinfo_FirstName, '') , ' ', COALESCE(operator.employeeinfo_LastName, '')) as operators "
                    ." , CONCAT(COALESCE(helper.employeeinfo_FirstName, '') , ' ', COALESCE(helper.employeeinfo_LastName, '')) as helpers "
                    ." , w.workorder_Status as Status "
                    ." , w.workorder_TicketID as Ticket "
                ." from workorder as w "
                ." inner join customerinfo as c on c.customerInfo_ID = w.customer_ID "
                ." inner join jobsite as j on j.jobsite_ID = w.jobsite_ID "
                ." left join unit as u on u.unit_ID = w.workorder_UnitFromUnitID "
                ." left join employeeinfo as operator on operator.employeeinfo_ID = w.workorder_OperatorFromEmployeeID "
                ." left join employeeinfo as helper on helper.employeeinfo_ID = w.workorder_HelperFromEmployeeID "
                .$condition
                .$orderBy.$limit;
        $this->ci->logger->info($sql);
        $r = array();
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $r["data"] = $stmt->fetchAll(PDO::FETCH_NUM);
            $recordsTotal = $this->ci->db->query('SELECT FOUND_ROWS();')->fetch(PDO::FETCH_COLUMN);
            $r["recordsTotal"] = $recordsTotal;
            $r["recordsFiltered"] = $recordsTotal;
        }

        return $this->jsonResponse($response, $r);
    }

    public function dispatching($request, $response, $args) {
        $params = $request->getQueryParams();

        $d = $this->workorderM->dispatching($params);
        $r = $d?array_merge(array("success"=> true), $d):array("success"=> false, "result"=> $d);
        return $this->jsonResponse($response, $r);
    }

    public function dispatchingUnit($request, $response, $args) {
        $params = $request->getQueryParams();

        $d = $this->workorderM->dispatchingUnit($params);
        $r = $d?array_merge(array("success"=> true), $d):array("success"=> false, "result"=> $d);
        return $this->jsonResponse($response, $r);
    }

    public function cancel($request, $response, $args) {
        $data = $request->getParsedBody();
        //取得登入者的資料
        $updater = $this->getLoginUser($request);

        $cancelData["workorder_UpdateByID"] = $updater["userinfo_ID"];
        $cancelData["workorder_UpdateDateTime"] = 'now()';

        $d = $this->workorderM->cancel($cancelData, $data["workorder_ID"]);
        $r = $d?array("success"=> true, "result"=> $d):array("success"=> false, "result"=> $d);
        return $this->jsonResponse($response, $r);
    }
}   
?>