<?php
use \Interop\Container\ContainerInterface as ContainerInterface;

define("kEmployeeInfoTableName", "employeeinfo");

class EmployeesController extends BasicController {

    private $db;
    private $authController;

    //Constructor
    public function __construct(ContainerInterface $ci) {
        parent::__construct(kEmployeeInfoTableName, $ci);
        $this->db = $ci->db;
        $this->authController = new AuthenticationController($ci);
    }

    public function collection($request, $response, $args) {
        $this->ci->logger->info("Get user list");
        // Testing AuthInfo from Middleware 
//        return $this->jsonResponse($response, $request->getAttribute(kMiddlewareAuthInfoKey));        
        // Data Processing
        $data = $this->getTableData();
        return $this->jsonResponse($response, $data);
    }

    public function toDatatable($request, $response, $args) {
        $this->ci->logger->info("Get employee list to Datatable");
        
        $allGetVars = $request->getQueryParams();
        if ( !($this->checkEmpty($allGetVars,array("length","order","search"))) || !($this->checkIsset($allGetVars,array("start"))) )
        {
            return $this->parameterErrorResponse($response);
        }

        //search keyWord
        if(isset($allGetVars['searchKey'])) {
            //長度為5且為數字 = 只搜尋No
            if(is_numeric($allGetVars['searchKey']) && strlen($allGetVars['searchKey']) == 5) {
                $condition = " where employeeinfo_ID = '".(int)$allGetVars['searchKey']."'";
            } else {
                $condition = " where employeeinfo_FirstName like '%".$allGetVars['searchKey']."%'"
                                          ." or employeeinfo_LastName like '%".$allGetVars['searchKey']."%' "
                                          ." or employeeinfo_Position like '%".$allGetVars['searchKey']."%' "
                                          ." or employeeinfo_PhoneNumber like '%".$allGetVars['searchKey']."%' "
                                          ." or employeeinfo_Status = '".$allGetVars['searchKey']."' ";
            }
        } else {
            $condition = "";
        }
        
        //select資料
        $orderBy = " ORDER BY ".((int)$allGetVars['order'][0]["column"]+1)." ".$allGetVars['order'][0]["dir"];
        $limit = " LIMIT ".$allGetVars["start"].", ".$allGetVars["length"];
        $sql = "select SQL_CALC_FOUND_ROWS "
                    ." e.employeeinfo_ID "
                    ." , e.employeeinfo_FirstName "
                    ." , e.employeeinfo_LastName "
                    ." , e.employeeinfo_Position "
                    ." , e.employeeinfo_PhoneNumber "
                    ." , e.employeeinfo_Status "
                    ." , COALESCE(max(p.punch_start_time), '-') as last_punch_in "
                ." from employeeinfo as e "
                ." left join punch as p on p.employeeinfo_ID = e.employeeinfo_ID "
                .$condition
                ." group by e.employeeinfo_ID "
                .$orderBy.$limit;

        $r = array();
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $r["data"] = $stmt->fetchAll(PDO::FETCH_NUM);
            $recordsTotal = $this->ci->db->query('SELECT FOUND_ROWS();')->fetch(PDO::FETCH_COLUMN);
            $r["recordsTotal"] = $recordsTotal;
            $r["recordsFiltered"] = $recordsTotal;
        }

        return $this->jsonResponse($response, $r);
    }

//    public function getUserByID($request, $response, $args) {
//        $this->ci->logger->info("Get user by ID");
//        // Data Processing
//        $stmt = $this->ci->db->prepare("SELECT * FROM " . $this->table . " WHERE u_id =\"" . $args["id"] . "\"");
//        $stmt->execute();
//        $stmt = $stmt->fetchAll(PDO::FETCH_OBJ);
//
//        $response->withHeader('Content-Type', 'application/json');
//        $response->write(json_encode($stmt));
//
//        return $response;
//    }
//
    public function createEmployee($request, $response, $args) {
        $this->ci->logger->info("Create Employee");
        
        $allPostPutVars = $request->getParsedBody();
        $needParameters = array("employeeinfo_FirstName","employeeinfo_LastName","employeeinfo_Gender"
                                ,"employeeinfo_PhoneNumber","employeeinfo_AddressID","employeeinfo_EMail","employeeinfo_HomeNumber"
                                ,"employeeinfo_EmergencyContactName", "employeeinfo_EmergencyContactNumber"
                                ,"employeeinfo_Position","employeeinfo_SIN","employeeinfo_PayRate");
        $notNeccParameters = array("employeeinfo_MiddleName","employeeinfo_Birthday","employeeinfo_WageSalary","employeeinfo_StartDate","employeeinfo_Remarks");
        if ( !($this->checkEmpty($allPostPutVars,$needParameters)) || !($this->checkIsset($allPostPutVars,$notNeccParameters)) )
        {
            return $this->parameterErrorResponse($response);
        }
        $filterPostData = [];
        $usersInfo = $request->getAttribute(kMiddlewareAuthInfoKey);
        $filterPostData["employeeinfo_CreateByID"]=$usersInfo[0]["userinfo_ID"];
        $filterPostData["employeeinfo_CreateDateTime"]=date('Y-m-d H:i:s');
        foreach ($needParameters as $key => $value) {
            if($allPostPutVars[$value] == "") {
                $filterPostData[$value] = null;
            } else {
                $filterPostData[$value] = $allPostPutVars[$value];
            }
        }
        foreach ($notNeccParameters as $key => $value) {
            if($allPostPutVars[$value] == "") {
                $filterPostData[$value] = null;
            } else {
                $filterPostData[$value] = $allPostPutVars[$value];
            }
        }
//        $check = DatabaseHelper::selectMysql($this->db, $this->tableName, array("employeeinfo_UserName"=>$filterPostData['employeeinfo_UserName']));
//        if ($check) {
//                return $this->jsonResponse($response, array("success"=>false,"msg"=>"帳號重複註冊"));
//        }
//        $check = DatabaseHelper::selectMysql($this->db, $this->tableName, array("employeeinfo_Email"=>$filterPostData['employeeinfo_Email']));
//        if ($check) {
//                return $this->jsonResponse($response, array("success"=>false,"msg"=>"Email重複註冊"));
//        }
        
        $sql = "INSERT INTO employeeinfo (employeeinfo_FirstName,employeeinfo_MiddleName,employeeinfo_LastName,employeeinfo_Gender,employeeinfo_Birthday,employeeinfo_PhoneNumber,employeeinfo_AddressID,employeeinfo_EMail,employeeinfo_HomeNumber,employeeinfo_EmergencyContactName,employeeinfo_EmergencyContactNumber,employeeinfo_Position,employeeinfo_SIN,employeeinfo_WageSalary,employeeinfo_PayRate,employeeinfo_StartDate,employeeinfo_Remarks,employeeinfo_Status,employeeinfo_CreateDateTime,employeeinfo_CreateByID)
                        VALUES (:employeeinfo_FirstName,:employeeinfo_MiddleName,:employeeinfo_LastName,:employeeinfo_Gender,:employeeinfo_Birthday,:employeeinfo_PhoneNumber,:employeeinfo_AddressID,:employeeinfo_EMail,:employeeinfo_HomeNumber,:employeeinfo_EmergencyContactName,:employeeinfo_EmergencyContactNumber,:employeeinfo_Position,:employeeinfo_SIN,:employeeinfo_WageSalary,:employeeinfo_PayRate,:employeeinfo_StartDate,:employeeinfo_Remarks,'active',:employeeinfo_CreateDateTime,:employeeinfo_CreateByID)";
//        $execute = DatabaseHelper::execute($this->db, $sql);
//        $stmt = $this->ci->db->prepare("SELECT * FROM " . $this->table . " WHERE u_id =\"" . $args["id"] . "\"");
//        $stmt->execute();
//        $stmt = $stmt->fetchAll(PDO::FETCH_OBJ);
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute($filterPostData)) {
                $employeeinfoId = $this->ci->db->lastInsertId();
                $callback["success"]=true;
                $callback["result"]= $this->m_getEmployeeByID($employeeinfoId);
        } else {
                $callback["success"]=false;
                $callback["msg"]="Insert data fail";
        }
        
        return $this->jsonResponse($response, $callback);
//        $response->withHeader('Content-Type', 'application/json');
//        $response->write(json_encode($callback));
////        $response->withHeader('Content-Type', 'application/json');
////        $response->write(json_encode($allPostPutVars));
//
//        return $response;
    }
    
    public function editByEmployeeID($request, $response, $args) {
        $this->ci->logger->info("Edit By Employee ID");
        
        $allPostPutVars = $request->getParsedBody();
        $needParameters = array("employeeinfo_ID","employeeinfo_FirstName","employeeinfo_LastName","employeeinfo_Gender"
                                ,"employeeinfo_PhoneNumber","employeeinfo_AddressID","employeeinfo_EMail","employeeinfo_HomeNumber"
                                ,"employeeinfo_EmergencyContactName","employeeinfo_EmergencyContactNumber"
                                ,"employeeinfo_Position","employeeinfo_SIN","employeeinfo_PayRate");
        $notNeccParameters = array("employeeinfo_MiddleName","employeeinfo_Birthday","employeeinfo_WageSalary","employeeinfo_StartDate","employeeinfo_Remarks");
        if ( !($this->checkEmpty($allPostPutVars,$needParameters)) || !($this->checkIsset($allPostPutVars,$notNeccParameters)) )
        {
            return $this->parameterErrorResponse($response);
        }
        
        unset($needParameters[0]);//employeeinfo_ID
        $filterPostData = [];
        $usersInfo = $request->getAttribute(kMiddlewareAuthInfoKey);
        $filterPostData["employeeinfo_UpdateByID"]=$usersInfo[0]["userinfo_ID"];
        $filterPostData["employeeinfo_UpdateDateTime"]=date('Y-m-d H:i:s');
        foreach ($needParameters as $key => $value) {
            if($allPostPutVars[$value] == "") {
                $filterPostData[$value] = null;
            } else {
                $filterPostData[$value] = $allPostPutVars[$value];
            }
        }
        foreach ($notNeccParameters as $key => $value) {
            if($allPostPutVars[$value] == "") {
                $filterPostData[$value] = null;
            } else {
                $filterPostData[$value] = $allPostPutVars[$value];
            }
        }
        
        $check = DatabaseHelper::selectMysql($this->db, $this->tableName, array("employeeinfo_ID"=>$allPostPutVars['employeeinfo_ID']));
        if (!$check) {
                return $this->jsonResponse($response, array("success"=>false,"msg"=>"employeeinfo_ID不存在"));
        }
        
        $filterPostData["employeeinfo_UpdateDateTime"]=date('Y-m-d H:i:s');
        $usersInfo = $request->getAttribute(kMiddlewareAuthInfoKey);
        $filterPostData["employeeinfo_UpdateByID"]=$usersInfo[0]["userinfo_ID"];
        $updateMysql = DatabaseHelper::updateMysql($this->db, $this->tableName, $filterPostData,array("employeeinfo_ID"=>$allPostPutVars['employeeinfo_ID']));
        if (!$updateMysql) {
                return $this->jsonResponse($response, array("success"=>false,"msg"=>"儲存失敗"));
        }
        
        $callback = array();
        $callback["success"]=true;
        return $this->jsonResponse($response, $callback);
        
//        return $response;
    }

    public function getEmployeeByID($request, $response, $args) {
        $this->ci->logger->info("Get Employee By ID");
        
        $allGetVars = $request->getQueryParams();
        if ( !($this->checkEmpty($allGetVars,array("employeeinfo_ID"))) )
        {
            return $this->parameterErrorResponse($response);
        }

        $check = $this->m_getEmployeeByID($allGetVars['employeeinfo_ID']);
        return $this->jsonResponse($response, array("success"=>true,"data"=>$check));
    }

    public function m_getEmployeeByID($employeeinfoID) {
        $check = DatabaseHelper::selectMysql($this->db, $this->tableName." e LEFT JOIN userinfo u on e.employeeinfo_CreateByID=u.userinfo_ID", array("e.employeeinfo_ID"=>$employeeinfoID),""
            , " e.*,u.userinfo_UserName,u.userinfo_Email"
             ." ,DATE_FORMAT(e.employeeinfo_CreateDateTime, '%Y-%m-%d %H:%i') as employeeinfo_CreateDateTime "
             ." ,DATE_FORMAT(e.employeeinfo_UpdateDateTime, '%Y-%m-%d %H:%i') as employeeinfo_UpdateDateTime ");
        if (!$check) {
                return $this->jsonResponse($response, array("success"=>false,"msg"=>"employeeinfo_ID不存在"));
        }
        
        $check[0]["employeeinfo_CreateByID"] = array( "employeeinfo_CreateByID" => $check[0]["employeeinfo_CreateByID"] , "userinfo_UserName" => $check[0]["userinfo_UserName"] , "userinfo_Email" => $check[0]["userinfo_Email"] );
        //unset($check[0]["userinfo_UserName"]);
        unset($check[0]["userinfo_Email"]);
        
        return $check[0];
    }

    public function getEmployeeByUserID($request, $response, $args) {
        $this->ci->logger->info("Get Employee By User ID");
        
        $allGetVars = $request->getQueryParams();
        if ( !($this->checkEmpty($allGetVars,array("userinfo_ID"))) )
        {
            return $this->parameterErrorResponse($response);
        }
        
        $check = DatabaseHelper::selectMysql($this->db, "userinfo u JOIN ".$this->tableName." e on u.employee_ID=e.employeeinfo_ID"
                                                            . " LEFT JOIN userinfo u2 on e.employeeinfo_CreateByID=u2.userinfo_ID"
                                                            . " LEFT JOIN userinfo u3 on e.employeeinfo_UpdateByID=u3.userinfo_ID"
                                                    , array("u.userinfo_ID"=>$allGetVars['userinfo_ID']),"","e.*"
                                                            . ",u2.userinfo_UserName 'employeeinfo_CreateByUserName',u2.userinfo_Email 'employeeinfo_CreateByEmail'"
                                                            . ",u3.userinfo_UserName 'employeeinfo_UpdateByUserName',u3.userinfo_Email 'employeeinfo_UpdateByEmail'");
        if (!$check) {
                return $this->jsonResponse($response, array("success"=>false,"msg"=>"userinfo_ID OR employeeinfo_ID 不存在"));
        }
        
        $check[0]["employeeinfo_CreateByID"] = array( "employeeinfo_CreateByID" => $check[0]["employeeinfo_CreateByID"] , "userinfo_UserName" => $check[0]["employeeinfo_CreateByUserName"] , "userinfo_Email" => $check[0]["employeeinfo_CreateByEmail"] );
        $check[0]["employeeinfo_UpdateByID"] = array( "employeeinfo_UpdateByID" => $check[0]["employeeinfo_UpdateByID"] , "userinfo_UserName" => $check[0]["employeeinfo_UpdateByUserName"] , "userinfo_Email" => $check[0]["employeeinfo_UpdateByEmail"] );
        unset($check[0]["employeeinfo_CreateByUserName"]);
        unset($check[0]["employeeinfo_CreateByEmail"]);
        unset($check[0]["employeeinfo_UpdateByUserName"]);
        unset($check[0]["employeeinfo_UpdateByEmail"]);
        
        return $this->jsonResponse($response, array("success"=>true,"data"=>$check[0]));
    }
    
    /**
     * 修改Employee狀態
     */
    public function switchStatu($request, $response, $args) {
        //檢查Params格式是否有誤
        $params = $request->getParsedBody();
        $needKey = ["employeeinfo_ID"];
        for($i=0;$i<count($needKey);$i++) {
            if(!array_key_exists($needKey[$i], $params ) && !empty($params[$needKey[$i]])) {
                return $this->parameterErrorResponse($response);
            }
        }

        //取得Employee狀態
        $check = DatabaseHelper::selectMysql($this->db, $this->tableName, array("employeeinfo_ID"=>$params['employeeinfo_ID']),"","employeeinfo_Status");
        if (!$check) {
                return $this->jsonResponse($response, array("success"=>false,"msg"=>"employeeinfo_ID 不存在"));
        }
        $status = $check[0]["employeeinfo_Status"];
        //修改狀態
        if($status == "active") {
            $status = "inactive";
        } else {
            $status = "active";
        }
        
        $updateMysql = DatabaseHelper::updateMysql($this->db, $this->tableName, array("employeeinfo_Status"=>$status),array("employeeinfo_ID"=>$params['employeeinfo_ID']));
        if (!$updateMysql) {
                return $this->jsonResponse($response, array("success"=>false,"msg"=>"儲存失敗"));
        }
        $callback["success"]=true;
        $callback["status"]=$status;
        return $this->jsonResponse($response, $callback);        
    }
    
//
//    public function udpateUserByID($request, $response, $args) {
//        
//    }
//
//    public function deleteUserByID($request, $response, $args) {
//        
//    }
//
//    /**
//     * 登入
//     * @param string $id user.id
//     * @return  test
//     */
//    public function userLogin($request, $response, $args) {
//        $allPostPutVars = $request->getParsedBody();
//        if ( empty($allPostPutVars['employeeinfo_UserName']) )
//        {
//            return $this->parameterErrorResponse($response);
//        }
//        $filterPostData = [];
//        $filterPostData['employeeinfo_UserName'] = $allPostPutVars['employeeinfo_UserName'];
//        $auth = $request->getHeaderLine("Authorization");
//        $pwd = trim(substr($auth, 5));
//        $filterPostData['employeeinfo_Password'] = $pwd;
//        
//        $check = DatabaseHelper::select($this->db, $this->tableName, $filterPostData);
//        if (!$check) {
//            $callback = array();
//            $callback["success"] = false;
//            $callback["msg"] = "Account or Password Error";//"帳號或密碼錯誤";
//            return $this->jsonResponse($response, $callback);
//        }
//        
//        $user = $check[0];
//        
//        if ( !empty($user["employeeinfo_ID"]) )
//        {
//            $token = $this->authController->createToken($user["employeeinfo_ID"]);
//            if ($token === false) {
//                return $this->parameterErrorResponse($response);
//            }
//
//            $callback = array();
//            $callback['data'] = $token;
//            $callback['success'] = true;
//            return $this->jsonResponse($response, $callback);            
//        }
//        
//        return $this->parameterErrorResponse($response);
//    }
    
    
    
}
